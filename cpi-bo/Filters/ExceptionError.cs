﻿using cpi_bo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace cpi_bo.Filters
{
    public class ExceptionError : FilterAttribute, IExceptionFilter
    {
        public void OnException(ExceptionContext filterContext)
        {
            if (!filterContext.ExceptionHandled)
            {
                //LogOperationModel logger = new LogOperationModel()
                //{
                //    LOG_MODULE = "SISTEMA",
                //    LOG_ACTION = "EXCEPTIONS",
                //    LOG_STATE = "ERROR",
                //    LOG_USER= HttpContext.Current.User.Identity.Name,
                //    LOG_MSG = filterContext.Exception.Message,
                //    LOG_STACKTRACE = filterContext.Exception.StackTrace,
                //    LOG_CONTROLLERNAME = filterContext.RouteData.Values["controller"].ToString(),
                //};
                //LogOperationDBHandle DBlog = new LogOperationDBHandle();
                //DBlog.InsertLogOperation(logger);
                filterContext.ExceptionHandled = true;
                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new { controller = "Error", action = "ExceptionError" }));
                
            }
        }


    } 

}