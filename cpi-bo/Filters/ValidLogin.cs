﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Web.Routing;
using System.Web.Mvc.Filters;

namespace cpi_bo.Filters
{
    public class ValidLogin : ActionFilterAttribute, IAuthenticationFilter
    {
        public void OnAuthentication(AuthenticationContext filterContext)
        {
            if (!filterContext.HttpContext.Request.IsAuthenticated || HttpContext.Current.Session["PermisosUser"] == null)
            {
                FormsAuthentication.SignOut();
                filterContext.Result =
                new RedirectToRouteResult(new RouteValueDictionary {
                    { "action", "Index" },
                    { "controller", "Login" },
                    { "returnUrl", filterContext.HttpContext.Request.RawUrl}
                });
            }
        }

        //public void OnAuthentication(ActionExecutingContext filterContext)
        //{
        //    if (HttpContext.Current.Session["PermisosUser"] == null)
        //    {
        //        FormsAuthentication.SignOut();
        //        filterContext.Result =
        //        new RedirectToRouteResult(new RouteValueDictionary {
        //            { "action", "Index" },
        //            { "controller", "Login" },
        //            { "returnUrl", filterContext.HttpContext.Request.RawUrl}
        //        });

        //        return;
        //    }
        //}

        public void OnAuthenticationChallenge(AuthenticationChallengeContext filterContext)
        {
           
        }
    }
}