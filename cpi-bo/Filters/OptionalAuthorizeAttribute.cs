﻿using cpi_bo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;

namespace cpi_bo.Helpers
{
    public class OptionalAuthorizeAttribute : AuthorizeAttribute
    {
        private readonly string _authorize;

        public OptionalAuthorizeAttribute(string authorize)
        {
            _authorize = authorize;
        }
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            //if (!filterContext.HttpContext.Request.IsAuthenticated || HttpContext.Current.Session["PermisosUser"] == null)
            //{
            //    FormsAuthentication.SignOut();
            //    filterContext.Result =
            //    new RedirectToRouteResult(new RouteValueDictionary {
            //        { "action", "Index" },
            //        { "controller", "Login" },
            //        { "returnUrl", filterContext.HttpContext.Request.RawUrl}
            //    });

            //}
            //else 
            if (!validPermisosUser(_authorize))
            {
                filterContext.Result = new RedirectToRouteResult(new
                RouteValueDictionary(new { controller = "Error", action = "AccessDenied" }));
            }
        }

        public static bool validPermisosUser(string perm_id)
        {
            if (HttpContext.Current.Session["PermisosUser"] != null)
                foreach (var i in (List<PermissionsModel>)HttpContext.Current.Session["PermisosUser"])
                {
                    if (i.PERMISO_ID.Equals(perm_id))
                        return true;
                }

            return false;
        }

    }
}