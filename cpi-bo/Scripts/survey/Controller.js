﻿app.controller('AdminCtrl', function ($scope, $http, surveyService) {
    console.log($scope.id);
    $scope.$watch("projectId", function () {
        $scope.nextFamilyOrganization = true;
        $scope.nextHealth = false;
        $scope.nextEducation = false;
        $scope.nextWorkSituation = false;
        $scope.nextFamilyEconomy = false;
        $scope.nextDwelling = false;
        $scope.nextSocialProblem = false;

        console.log($scope.infanteId);
        $scope.getSurvey = function (id) {
            console.log('id', typeof id)
            $http.get('/Kid/IndexSurvey', { params: { 'infanteId': id } }).then(function (response) {
                console.log('data', response);
                $scope.FamilyOrganization = response.data
                console.log('FamilyOrganization', $scope.FamilyOrganization);
                $scope.flagProgressWait = false;
            }, function error(response) {
                console.log('Error al cargar tabla posiciones - ' + response.data.Message)
            });
        };

        $scope.getSurvey($scope.infanteId);

        $scope.getHealth = function (id) {
            console.log('getHealth entre', id)
            $http.get('/Kid/IndexHealth', { params: { 'infanteId': id } }).then(function (response) {
                console.log('data', response);
                $scope.Health = response.data
                console.log('Health', $scope.Health);
            }, function error(response) {
                console.log('Error al cargar tabla posiciones - ' + response.data.Message)
            });
        };

        $scope.getEducation = function (id) {
            $http.get('/Kid/IndexEducation', { params: { 'infanteId': id } }).then(function (response) {
                $scope.Education = response.data
            }, function error(response) {
                console.log('Error al cargar tabla posiciones - ' + response.data.Message)
            });
        };

        $scope.getWorkSituation = function (id) {
            $http.get('/Kid/IndexWorkSituation', { params: { 'infanteId': id } }).then(function (response) {
                $scope.WorkSituation = response.data
            }, function error(response) {
                console.log('Error al cargar tabla posiciones - ' + response.data.Message)
            });
        };

        $scope.getFamilyEconomy = function (id) {
            $http.get('/Kid/IndexFamilyEconomy', { params: { 'infanteId': id } }).then(function (response) {
                $scope.FamilyEconomy = response.data
            }, function error(response) {
                console.log('Error al cargar tabla posiciones - ' + response.data.Message)
            });
        };
        
        $scope.getDwelling = function (id) {
            $http.get('/Kid/IndexDwelling', { params: { 'infanteId': id } }).then(function (response) {
                $scope.Dwelling = response.data
            }, function error(response) {
                console.log('Error al cargar tabla posiciones - ' + response.data.Message)
            });
        };

        $scope.getSocialProblem = function (id) {
            $http.get('/Kid/IndexSocialProblem', { params: { 'infanteId': id } }).then(function (response) {
                $scope.SocialProblem = response.data
            }, function error(response) {
                console.log('Error al cargar tabla posiciones - ' + response.data.Message)
            });
        };

    console.log('ssssss', $scope.infanteId);

    $scope.saveFamilyOrganization = function (surveyObjet) {
        console.log('surveyObjet', surveyObjet);
        $http.post('/Kid/saveFamilyOrganization', surveyObjet).then(function (response) {
            console.log('test', $scope.nextFamilyOrganization)
            $scope.nextFamilyOrganization = false;
            $scope.nextHealth = true;
            console.log('nextFamilyOrganization', $scope.nextFamilyOrganization)
            console.log('$scope.infanteId', $scope.infanteId);
            $scope.getHealth($scope.infanteId);
        }, function error(response) {
            console.log('error al grabar - ' + response.data.Message)
        });
    };

    $scope.saveHealth = function (surveyObjet) {
        console.log('saveHealth', surveyObjet);
        $http.post('/Kid/saveHealth', surveyObjet).then(function (response) {
            console.log('saveHealth', response.data);
            $scope.nextHealth = false;
            $scope.nextEducation = true;
            $scope.getEducation($scope.infanteId);
        }, function error(response) {
            console.log('error al grabar - ' + response.data.Message)
        });
    };

    $scope.saveEducation = function (surveyObjet) {
        $http.post('/Kid/saveEducation', surveyObjet).then(function (response) {
            $scope.nextEducation = false;
            $scope.nextWorkSituation = true;
            $scope.getWorkSituation($scope.infanteId);
        }, function error(response) {
            console.log('error al grabar - ' + response.data.Message)
        });
    };

    $scope.saveWorkSituation = function (surveyObjet) {
        $http.post('/Kid/saveWorkSituation', surveyObjet).then(function (response) {
            $scope.nextWorkSituation = false;
            $scope.nextFamilyEconomy = true;
            $scope.getFamilyEconomy($scope.infanteId);
        }, function error(response) {
            console.log('error al grabar - ' + response.data.Message)
        });
    };

    $scope.saveFamilyEconomy = function (surveyObjet) {
        $http.post('/Kid/saveFamilyEconomy', surveyObjet).then(function (response) {
            $scope.nextFamilyEconomy = false;
            $scope.nextDwelling = true;
            $scope.getDwelling($scope.infanteId);
        }, function error(response) {
            console.log('error al grabar - ' + response.data.Message)
        });
    };

    $scope.saveDwelling = function (surveyObjet) {
        $http.post('/Kid/saveDwelling', surveyObjet).then(function (response) {
            $scope.nextDwelling = false;
            $scope.nextSocialProblem = true;
            $scope.getSocialProblem($scope.infanteId);
        }, function error(response) {
            console.log('error al grabar - ' + response.data.Message)
        });
    };

    $scope.saveSocialProblem = function (surveyObjet) {
        $http.post('/Kid/saveSocialProblem', surveyObjet).then(function (response) {
            $scope.nextSocialProblem = false;
        }, function error(response) {
            console.log('error al grabar - ' + response.data.Message)
        });
    };

    $scope.validFamilyOrganization = function (objetFamilyOrganization) {
        for (var propiedad in objetFamilyOrganization) {
            console.log('propiedad', propiedad.valueOf);
        }
    }
 
    $scope.getButtonStyle = function (optionValue) {
        console.log('style');
        if (this.canDelete && this.answer == optionValue) {
            return 'btn-danger';
        }
        return 'btn-default';
    };


    $scope.FamilyCompositions = [
        {
            question: 'Madre y padre a cargo del niño/a',
            value: 1
        },
        {
            question: 'Madre a cargo del niño/a',
            value: 2
        },
        {
            question: 'Padre a cargo del niño/a',
            value: 3
        },
        {
            question: 'Otro familiar o adulto responsable a cargo del niño/a con tutela judicial',
            value: 4
        },
        {
            question: 'Otro familiar o adulto responsable a cargo del niño/a sin tutela judicial',
            value: 5
        },
        {
            question: 'Sin familiar responsable de la crianza del niño',
            value: 6
        }
    ];

    $scope.DeprivedOfLiberty = [
        {
            question: 'Si',
            value: 1
        },
        {
            question: 'No',
            value: 2
        }
    ];

    $scope.NumberOfSiblings = [
        {
            question: 'Hijo único',
            value: 1
        },
        {
            question: '2 hijos',
            value: 2
        },
        {
            question: 'Entre 3 y 5',
            value: 3
        },
        {
            question: 'Entre 6 y 10',
            value: 4
        },
        {
            question: 'Más de 10 hermanos',
            value: 5
        }
    ];

    $scope.AdolescentMother = [
        {
            question: 'Si',
            value: 1
        },
        {
            question: 'No',
            value: 2
        }
    ];

    $scope.pregnantWoman = [
        {
            question: 'Si',
            value: 1
        },
        {
            question: 'No',
            value: 2
        }
    ];

    $scope.CondicionesDeSaludFamiliar = [
               {
                   question: 'Sin problemas de salud',
                   value: 1
               },
               {
                   question: 'Problemas de salud cronicos. Escribir detelles',
                   value: 2
               },
               {
                   question: 'Discapacidad con certificados. Escribir detelles',
                   value: 3
               },
               {
                   question: 'Discapacidad sin certificados. Escribir detelles',
                   value: 4
               }
    ];

    $scope.CoberturaDeSaludFamiliar = [
            {
                question: 'Cobertura de salud privado, Obra social o Prepaga',
                value: 1
            },
            {
                question: 'Cobertura publica de salud con medicos de referencia, Hospital, Cesac o Cobertura porteña de salud',
                value: 2
            },
            {
                question: 'Cobertura publica de salud sin seguimiendo de medico de referencia',
                value: 3
            },
            {
                question: 'No asiste a ningun centro medico',
                value: 4
            }
    ];

    $scope.ControlDeSaludDelInfante = [
            {
                question: 'Controles periodicos de acuerdo a la edad',
                value: 1
            },
            {
                question: 'Unico control al momento del nacimiento',
                value: 2
            },
            {
                question: 'Solo se controla en caso de enfermedad',
                value: 3
            },
            {
                question: 'Sin ningun control',
                value: 4
            }
    ];

    $scope.VacunacionDelInfante = [
            {
                question: 'Posee carnet de vacunacion completo',
                value: 1
            },
            {
                question: 'Posee carnet de vacunacion incompleto',
                value: 2
            },
            {
                question: 'Sin carnet de vacunas',
                value: 3
            },
    ];

    $scope.EscolarizacionDeLosInfantes = [
            {
                question: 'No aplica, sus hijos son menores de 4 años o mayores de 18 años',
                value: 1
            },
            {
                question: 'Todos escolarizados',
                value: 2
            },
            {
                question: 'Minoria de hijos en edad escolar pero sin escolarizacion',
                value: 3
            },
            {
                question: 'Mayoria en edad escolar pero sin escolarizacion',
                value: 4
            },
            {
                question: 'Ninguno escolarizado pero en edad escolar',
                value: 5
            },
    ];

    $scope.MadreAdolescente = [
                   {
                       question: 'Secundario completo',
                       value: 1
                   },
                   {
                       question: 'Asiste algun establecimiento educativo',
                       value: 2
                   },
                   {
                       question: 'No asiste a ningun establecimiento educativo',
                       value: 3
                   },
    ];

    $scope.TrabajoEnLaFamilia = [
            {
                question: 'Todos los adultos responsables del ñino tienen trabajo',
                value: 1
            },
            {
                question: 'Algun adulto responsable del ñino tiene trabajo',
                value: 2
            },
            {
                question: 'Ningun adulto responsable del ñino tiene trabajo',
                value: 3
            },
    ];

    $scope.TrabajoMenorDeEdad = [
                  {
                      question: 'No, ninguno menor de edad trabaja',
                      value: 1
                  },
                  {
                      question: 'Si y tiene entre 16 y 18 años',
                      value: 2
                  },
                  {
                      question: 'Si y tiene menos de 16 años',
                      value: 3
                  },
    ];

    $scope.IngresoFamiliar = [
            {
                question: 'Mas de $10454',
                value: 1
            },
            {
                question: 'Entre $6570 y $10454',
                value: 2
            },
            {
                question: 'Entre $3884 y $6570',
                value: 3
            },
            {
                question: 'Menoa de $3884',
                value: 4
            },
    ];

    $scope.BeneficioSocial = [
                 {
                     question: 'No recibe porque considera que no lo necesita',
                     value: 1
                 },
                 {
                     question: 'Uno o mas miembros de la familia tienen planes sociales. Escribir detelles',
                     value: 2
                 },
                 {
                     question: 'Ninguno recibe un plan social y concidera que lo necesita',
                     value: 3
                 },
    ];

    $scope.TipoDeVivienda = [
            {
                question: 'Casa o departamento en zona urbana',
                value: 1
            },
            {
                question: 'Vivienda en asentamiento o villa',
                value: 2
            },
            {
                question: 'Habitacion en inquilinato/conventillo',
                value: 3
            },
            {
                question: 'Habitacion de hotel o pension',
                value: 4
            },
            {
                question: 'Rancho o casita en asentamiento o villa',
                value: 5
            },
            {
                question: 'Casa tomada',
                value: 6
            },
            {
                question: 'Institucion colectiva, Hogar convivencial o parador',
                value: 7
            },
            {
                question: 'En situacion de calle',
                value: 8
            },
    ];

    $scope.DerechoHabitarLaVivienda = [
                  {
                      question: 'Propia, con titulo de propiedad',
                      value: 1
                  },
                  {
                      question: 'Propia, sin titulo de propiedad',
                      value: 2
                  },
                  {
                      question: 'Alquila, con contrato',
                      value: 3
                  },
                  {
                      question: 'Alquila, sin contrato',
                      value: 4
                  },
                  {
                      question: 'Prestada',
                      value: 5
                  },
                  {
                      question: 'No tiene, vive en un hogar, parador o se encuentra en situacion de calle',
                      value: 6
                  },
    ];

    $scope.CantidadDePersonasPorHabitacion = [
                 {
                     question: 'Uno',
                     value: 1
                 },
                 {
                     question: 'Dos',
                     value: 2
                 },
                 {
                     question: 'Entre3 y 4',
                     value: 3
                 },
                 {
                     question: 'Entre 4 y 6',
                     value: 4
                 },
                 {
                     question: 'Entre 6 y 10',
                     value: 5
                 },
                 {
                     question: '10 o mas',
                     value: 6
                 },
    ];

    $scope.ServiciosBasicos = [
                {
                    question: 'Cuenta con tos los servicios',
                    value: 1
                },
                {
                    question: 'Cuenta con algunos servicios, gas natural, agua potable y/o electricidad',
                    value: 2
                },
                {
                    question: 'Cuenta con algunos servicios, garrafa, agua potable y/o electricidad',
                    value: 3
                },
                {
                    question: 'No poseen ningun servicio',
                    value: 4
                },
    ];

    $scope.ProblemaSocial = [
            {
                question: 'No',
                value: 1
            },
            {
                question: 'Si. Escribir detelles',
                value: 2
            },
    ];

    });
})