﻿app.service("surveyService", function ($http) {
    this.getBook = function (bookId) {
        var response = $http({
            method: "post",
            url: "Home/GetBookById",
            params: {
                id: JSON.stringify(bookId)
            }
        });
        return response;
    }
});