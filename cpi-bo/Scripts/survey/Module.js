﻿var app = angular.module('appAdmin', ['ui.bootstrap', 'ui.router']);

app.config(function ($stateProvider) {
    $stateProvider.state('kid.surveyKid', {
        url: '/:id',
        component: 'SurveyKid',
        params: {
            id: null
        },
        resolve: {
            id : (function($stateParams){return $stateParams.id})
        }
    });
  })