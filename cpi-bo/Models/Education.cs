﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace cpi_bo.Models
{
    public class Education
    {
        public int ID { get; set; }

        public int ESCOLARIZACIONDELOSINFANTES { get; set; }

        public int MADREADOLESCENTE { get; set; }

        public int IDINFANTE { get; set; }
    }
}