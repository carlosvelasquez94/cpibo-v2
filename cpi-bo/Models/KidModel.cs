﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace cpi_bo.Models
{
    public class KidModel
    {
        [Required]
        [Display(Name = "Id")]
        public int INFANTE_ID { get; set; }

        [Display(Name = "Nombre")]
        [RegularExpression(@"^[a-zA-Z]+$", ErrorMessage = "Ingrese solo letras"), Required(ErrorMessage = "El nombre no puede estar vacio.")]
        public string INFANTE_NOMBRE { get; set; }

        [Display(Name = "Apellido")]
        [RegularExpression(@"^[a-zA-Z]+$", ErrorMessage = "Ingrese solo letras"), Required(ErrorMessage = "El apellido no puede estar vacio.")]
        public string INFANTE_APELLIDO { get; set; }

        [Display(Name = "Documento")]
        [RegularExpression(@"[0-9]{7,8}(\.[0-9]{0,2})?$", ErrorMessage = "El documento debe contar con al menos 7 carácteres y ser solo numeros"), Required(ErrorMessage = "El documento no puede estar vacio.")]
        public string INFANTE_DOCUMENTO { get; set; }

        [Required]
        [Display(Name = "Tipo de documento")]
        public int INFANTE_TIPODOCUMENTO_ID { get; set; }

        [Required]
        [Display(Name = "Genero")]
        public char INFANTE_GENERO { get; set; }

        [Required]
        [Display(Name = "Nacionalidad")]
        public int INFANTE_NACIONALIDAD_ID { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "La Fecha de nacimiento no puede estar vacio.")]
        [Display(Name = "Fecha de nacimiento")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? INFANTE_FECHANACIMIENTO { get; set; }

        [Required]
        [Display(Name = "Fecha de ingreso")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? INFANTE_FECHAINGRESO { get; set; }

        [Display(Name = "Domicilio")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El domicilio no puede estar vacio.")]
        public string INFANTE_DOMICILIO { get; set; }

        [Required]
        [Display(Name = "Codigo postal")]
        public string INFANTE_LOCALIDAD_CODIGOPOSTAL { get; set; }

        [Required]
        [Display(Name = "Sala")]
        public int INFANTE_SALA_ID { get; set; }
        public string INFANTE_DESCRIPCION { get; set; }

        [Display(Name = "Estado")]
        public string INFANTE_ESTADO_ID { get; set; }

        [Display(Name = "Fecha egreso")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? INFANTE_FECHAEGRESO { get; set; }

        [Display(Name = "Puntaje total")]
        public int INFANTE_PUNTAJETOTAL { get; set; }

        [Display(Name = "Asignar")]
        public bool ASIGNACION { get; set; }

        [Display(Name = "Nacionalidad")]
        public string NACIONALIDAD { get; set; }
    }
}