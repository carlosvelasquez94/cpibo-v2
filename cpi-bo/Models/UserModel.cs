﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace cpi_bo.Models
{
    public class UserModel
    {
        [Required]
        [Display(Name = "Nick")]
        public string USUARIO_NICK { get; set; }

        [RegularExpression(@"^[a-zA-Z]+$", ErrorMessage = "Ingrese solo letras"), Required(ErrorMessage = "El nombre no puede estar vacio.")]
        [Display(Name = "Nombre")]
        public string USUARIO_NOMBRE { get; set; }

        [RegularExpression(@"^[a-zA-Z]+$", ErrorMessage = "Ingrese solo letras"), Required(ErrorMessage = "El apellido no puede estar vacio.")]
        [Display(Name = "Apellido")]
        public string USUARIO_APELLIDO { get; set; }

        [Required]
        [Display(Name = "Clave")]
        public string USUARIO_CLAVE { get; set; }

        [Required]
        [Display(Name = "Perfil")]
        public int USUARIO_PERFIL_ID { get; set; }

        public string USUARIO_DESCRIPCION { get; set; }

        [Required]
        [Display(Name = "Estado")]
        public string USUARIO_ESTADO_ID { get; set; }

        [EmailAddress(ErrorMessage = "Email inválido"), Required(ErrorMessage = "El email no puede estar vacio.")]
        [Display(Name = "Email")]
        public string USUARIO_EMAIL { get; set; }
    }
}