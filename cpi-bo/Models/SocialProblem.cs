﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace cpi_bo.Models
{
    public class SocialProblem
    {
        public int ID { get; set; }

        public int REQUIEREINTERVENCION { get; set; }

        public int IDINFANTE { get; set; }
    }
}