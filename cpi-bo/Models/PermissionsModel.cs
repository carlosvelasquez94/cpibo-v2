﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace cpi_bo.Models
{
    public class PermissionsModel
    {
        [Required]
        [Display(Name = "ID")]
        public string PERMISO_ID { get; set; }

        [Required]
        [Display(Name = "Descripcion")]
        public string PERMISO_DESCRIPCION { get; set; }

        [Required]
        [Display(Name = "Modulo")]
        public string PERMISO_MODULO { get; set; }
    }
}