﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace cpi_bo.Models
{
    public class Health
    {
        public int ID { get; set; }

        public int CONDICIONESDESALUDFAMILIAR { get; set; }

        public int COBERTURADESALUDFAMILIAR { get; set; }

        public int CONTROLDESALUDDELINFANTE { get; set; }

        public int VACUNACIONDELINFANTE { get; set; }

        public int IDINFANTE { get; set; }
    }
}