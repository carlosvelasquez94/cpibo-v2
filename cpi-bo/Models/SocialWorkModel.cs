﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace cpi_bo.Models
{
    public class SocialWorkModel 
    {
        [Required]
        [Display(Name = "ID")]
        public int OBRASOCIAL_ID { get; set; }

        [Required]
        [Display(Name = "Descripcion")]
        public string OBRASOCIAL_DESCRIPCION { get; set; }
    }
}