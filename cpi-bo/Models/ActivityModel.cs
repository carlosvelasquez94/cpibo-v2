﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace cpi_bo.Models
{
    public class ActivityModel
    {
        [Required]
        [Display(Name = "ID")]
        public int EMPLEADOACTIVIDAD_ID { get; set; }

        [Required()]
        [Display(Name = "Actividad")]
        public int EMPLEADOACTIVIDAD_ACTIVIDAD_ID { get; set; }
        public string EMPLEADOACTIVIDAD_ACTIVIDAD_DESCRIPCION { get; set; }

        [Required()]
        [Display(Name = "Empleado")]
        public int EMPLEADOACTIVIDAD_EMPLEADO_ID { get; set; }
        public string EMPLEADOACTIVIDAD_EMPLEADO_DESCRIPCION { get; set; }

        [Required]
        [Display(Name = "Fecha")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime EMPLEADOACTIVIDAD_FECHA { get; set; }

        [Required]
        [RegularExpression(@"^(0[1-9]|1[0-9]|2[0-4]):[0-5][0-9]$", ErrorMessage = "Ingresar la hora en un formato valido. Ej: 12:30")]
        [Display(Name = "Hora")]
        public string EMPLEADOACTIVIDAD_HORA { get; set; }

        [Required]
        [Display(Name = "Descripcion")]
        public string EMPLEADOACTIVIDAD_ANOTACION { get; set; }
    }
}