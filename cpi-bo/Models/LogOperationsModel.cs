﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace cpi_bo.Models
{
    public class LogOperationsModel
    {
        [Display(Name = "Id")]
        public Int16 LOG_ID { get; set; }

        [Display(Name = "Modulo")]
        public string LOG_MODULE { get; set; }

        [Display(Name = "Accion")]
        public string LOG_ACTION { get; set; }

        [Display(Name = "Estado")]
        public string LOG_STATE { get; set; }

        [Display(Name = "Mensaje")]
        public string LOG_MSG { get; set; }

        public string LOG_STACKTRACE { get; set; }

        public string LOG_CONTROLLERNAME { get; set; }

        [Display(Name = "Usuario")]
        public string LOG_USER { get; set; }

        [Display(Name = "Fecha/Hora")]
        public DateTime LOG_DATE { get; set; }
        

    }
}