﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace cpi_bo.Models
{
    public class LoginModel
    {
        [Required]
        [Display(Name = "Usuario")]
        public string USUARIO_NICK { get; set; }

        [Required]
        [Display(Name = "Clave")]
        public string USUARIO_CLAVE { get; set; }
    }
}