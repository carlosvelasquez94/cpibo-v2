﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace cpi_bo.Models
{
    public class ResponsibleModel
    {
        [Required]
        [Display(Name = "Id")]
        public int RESPONSABLE_ID { get; set; }

        [Display(Name = "Nombre")]
        [RegularExpression(@"^[a-zA-Z]+$", ErrorMessage = "Ingrese solo letras"), Required(ErrorMessage = "El nombre no puede estar vacio.")]
        public string RESPONSABLE_NOMBRE { get; set; }

        [Display(Name = "Apellido")]
        [RegularExpression(@"^[a-zA-Z]+$", ErrorMessage = "Ingrese solo letras"), Required(ErrorMessage = "El apellido no puede estar vacio.")]
        public string RESPONSABLE_APELLIDO { get; set; }
       
        [Display(Name = "Documento")]
        [RegularExpression(@"[0-9]{7,8}(\.[0-9]{0,2})?$", ErrorMessage = "El documento debe contar con al menos 7 carácteres y ser solo numeros"), Required(ErrorMessage = "El documento no puede estar vacio.")]
        public string RESPONSABLE_DOCUMENTO { get; set; }

        [Required]
        [Display(Name = "Tipo documento")]
        public int RESPONSABLE_TIPODOCUMENTO_ID { get; set; }

        public string RESPONSABLE_TIPODOCUMENTO_DESCRIPCION { get; set; }

        [Required]
        [Display(Name = "Tipo nacionalidad")]
        public int RESPONSABLE_TIPONACIONALIDAD_ID { get; set; }
        public string RESPONSABLE_TIPONACIONALIDAD_DESCRIPCION { get; set; }
    }
}