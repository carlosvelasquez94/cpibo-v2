﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace cpi_bo.Models
{
    public class MaritalStatusModel 
    {
        [Required]
        [Display(Name = "ID")]
        public int ESTADOCIVIL_ID { get; set; }

        [Required]
        [Display(Name = "Descripcion")]
        public string ESTADOCIVIL_DESCRIPCION { get; set; }
    }
}