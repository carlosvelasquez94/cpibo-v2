﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace cpi_bo.Models
{
    public class FamilyEconomy
    {
        public int ID { get; set; }

        public int UBICACIONINGRESOFAMILIAR { get; set; }

        public int BENEFICIOSOCIAL { get; set; }

        public int IDINFANTE { get; set; }
    }
}