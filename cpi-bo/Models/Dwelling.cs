﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace cpi_bo.Models
{
    public class Dwelling
    {
        public int ID { get; set; }

        public int TIPOVIVIENDA { get; set; }

        public int TIPOPROPIEDAD { get; set; }

        public int CANTIDADPERSONASPORHABITACION { get; set; }

        public int SERVICIOSVIVIENDA { get; set; }

        public int IDINFANTE { get; set; }
    }
}