﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace cpi_bo.Models
{
    public class WorkSituation
    {
        public int ID { get; set; }

        public int TRABAJOENLAFAMILIA { get; set; }

        public int TRABAJOMENORDEEDAD { get; set; }

        public int IDINFANTE { get; set; }
    }
}