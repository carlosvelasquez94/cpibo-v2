﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace cpi_bo.Models
{
    public class FamilyOrganization
    {
        public int ID { get; set; }

        public int FAMILIARACARGO { get; set; }

        public int FAMILIARPRIVADOLIBERTAD { get; set; }

        public int CANTIDADHERMANOS { get; set; }

        public int MADREADOLECENTE { get; set; }

        public int FAMILIAREMBARAZADA { get; set; }

        public int IDINFANTE { get; set; }

    }
}