﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace cpi_bo.Models
{
    public class DocumentTypeModel
    {
        [Required]
        [Display(Name = "ID")]
        public int TIPODOCUMENTO_ID { get; set; }

        [Required]
        [Display(Name = "Descripcion")]
        public string TIPODOCUMENTO_DESCRIPCION { get; set; }
    }
}