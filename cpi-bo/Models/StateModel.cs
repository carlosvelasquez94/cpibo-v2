﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace cpi_bo.Models
{
    public class StateModel
    {
        [Required]
        [Display(Name = "ID")]
        public string ESTADO_ID { get; set; }

        [Required]
        [Display(Name = "Descripcion")]
        public string ESTADO_DESCRIPCION { get; set; }

        [Required]
        [Display(Name = "Tipo")]
        public string ESTADO_TIPO { get; set; }

        [Required]
        [Display(Name = "Clase")]
        public string ESTADO_CLASE { get; set; }
    }
}