﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace cpi_bo.Models
{
    public class ProfileModel
    {
        [Required]
        [Display(Name = "ID")]
        public Int16 perfil_id { get; set; }

        [Required]
        [Display(Name = "Nombre")]
        public string perfil_nombre { get; set; }

        [Required]
        [Display(Name = "Descripcion")]
        public string perfil_descripcion { get; set; }
    }
}