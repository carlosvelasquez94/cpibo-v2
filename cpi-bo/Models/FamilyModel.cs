﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
namespace cpi_bo.Models
{
    public class FamilyModel
    {
        [Required]
        [Display(Name = "Id")]
        public int FAMILIAR_ID { get; set; }

        [Required()]
        [Display(Name = "Estado Civil")]
        public int FAMILIAR_ESTADOCIVIL_ID { get; set; }

        [Required()]
        [Display(Name = "Obra Social")]
        public int FAMILIAR_OBRASOCIAL_ID { get; set; }

        [Required()]
        [Display(Name = "Tipo de documento")]
        public int FAMILIAR_TIPODOCUMENTO_ID { get; set; }

        [Display(Name = "Nombre")]
        [RegularExpression(@"^[a-zA-Z]+$", ErrorMessage = "Ingrese solo letras"), Required(ErrorMessage = "El nombre no puede estar vacio.")]
        public string FAMILIAR_NOMBRE { get; set; }

        [Display(Name = "Apellido")]
        [RegularExpression(@"^[a-zA-Z]+$", ErrorMessage = "Ingrese solo letras"), Required(ErrorMessage = "El apellido no puede estar vacio.")]
        public string FAMILIAR_APELLIDO { get; set; }

        [Display(Name = "Documento")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Documento no puede estar vacio.")]
        public string FAMILIAR_DOCUMENTO { get; set; }

        [Required]
        [Display(Name = "Género")]
        public string FAMILIAR_GENERO { get; set; }

        [Display(Name = "Fecha de Nacimiento")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "La fecha de nacimiento no puede estar vacio.")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? FAMILIAR_FECHANACIMIENTO { get; set; }
    }
}