﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace cpi_bo.Models
{
    public class AuditModel
    {
        [Required]
        [Display(Name = "ID")]
        public int AUDITORIA_ID { get; set; }

        [Required]
        [Display(Name = "Modulo")]
        public string AUDITORIA_MODULO { get; set; }

        [Required]
        [Display(Name = "Accion")]
        public string AUDITORIA_ACCION { get; set; }

        [Required]
        [Display(Name = "Estado")]
        public string AUDITORIA_ESTADO { get; set; }

        [Required]
        [Display(Name = "Mensaje")]
        public string AUDITORIA_MENSAJE { get; set; }

        [Required]
        [Display(Name = "Rastro")]
        public string AUDITORIA_RASTRO { get; set; }

        [Required]
        [Display(Name = "Nombre controlador")]
        public string AUDITORIA_NOMBRECONTROLADOR { get; set; }

        [Required]
        [Display(Name = "Usuario")]
        public string AUDITORIA_USUARIO_NICK { get; set; }

        [Required]
        [Display(Name = "Fecha")]
        [DataType(DataType.DateTime)]
        public DateTime AUDITORIA_FECHAHORA { get; set; }

    }
}