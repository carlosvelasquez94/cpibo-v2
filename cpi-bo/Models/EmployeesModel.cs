﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace cpi_bo.Models
{
    public class EmployeesModel 
    {
        [Required]
        [Display(Name = "Id")]
        public int EMPLEADO_ID { get; set; }

        [Display(Name = "Nombre")]
        [RegularExpression(@"^[a-zA-Z]+$", ErrorMessage = "Ingrese solo letras"), Required(ErrorMessage = "El nombre no puede estar vacio.")]
        public string EMPLEADO_NOMBRE { get; set; }

        [Display(Name = "Apellido")]
        [RegularExpression(@"^[a-zA-Z]+$", ErrorMessage = "Ingrese solo letras"), Required(ErrorMessage = "El apellido no puede estar vacio.")]
        public string EMPLEADO_APELLIDO { get; set; }

        [Display(Name = "Fecha de Nacimiento")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "La fecha de nacimiento no puede estar vacio.")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? EMPLEADO_FECHANACIMIENTO { get; set; }

        [Required()]
        [Display(Name = "Estado Civil")]
        public int EMPLEADO_ESTADOCIVIL { get; set; }

        [Required()]
        [Display(Name = "Cantidad de hijos")]
        public int EMPLEADO_CANTIDADHIJOS { get; set; }

        [Display(Name = "Domicilio")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El domicilio no puede estar vacio.")]
        public string EMPLEADO_DOMICILIO { get; set; }

        [Display(Name = "Email")]
        [RegularExpression(@"^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$", ErrorMessage = "Ingrese un email válido"), Required(ErrorMessage = "El email no puede estar vacio.")]
        public string EMPLEADO_EMAIL { get; set; }

        [Required()]
        [Display(Name = "Localidad")]
        public string EMPLEADO_LOCALIDAD_CODIGOPOSTAL { get; set; }

        [Display(Name = "Documento")]
        [RegularExpression(@"[0-9]{7,8}(\.[0-9]{0,2})?$", ErrorMessage = "El documento debe contar con al menos 7 carácteres y ser solo numeros"), Required(ErrorMessage = "El documento no puede estar vacio.")]
        public string EMPLEADO_DOCUMENTO { get; set; }

        [Display(Name = "CUIL")]
        [RegularExpression(@"[0-9]{10,11}(\.[0-9]{0,2})?$", ErrorMessage = "El CUIL debe contar con 11 carácteres y ser solo numeros"), Required(ErrorMessage = "El CUIL no puede estar vacio.")]
        public string EMPLEADO_CUIL { get; set; }

        [Required()]
        [Display(Name = "Nacionalidad")]
        public int EMPLEADO_NACIONALIDAD_ID { get; set; }

        public string EMPLEADO_DESCRIPCION { get; set; }

        [Display(Name = "Fecha de Ingreso")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "La Fecha de Ingreso no puede estar vacio.")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? EMPLEADO_FECHAINGRESO{ get; set; }

        [Display(Name = "Estado")]
        public string EMPLEADO_ESTADO_ID { get; set; }

        [Required()]
        [Display(Name = "Tipo de documento")]
        public int EMPLEADO_TIPODOCUMENTO_ID { get; set; }

        [Required()]
        [Display(Name = "Número de afiliado")]
        public string EMPLEADO_NUMEROAFILIADO { get; set; }

        [Required()]
        [Display(Name = "Obra Social")]
        public int EMPLEADO_OBRASOCIAL_ID { get; set; }
    }
}