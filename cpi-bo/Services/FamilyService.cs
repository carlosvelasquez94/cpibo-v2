﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using cpi_bo.Models;

namespace cpi_bo.Services
{
    public class FamilyService
    {
        private SqlConnection con;
        AuditModel Audit = new AuditModel();
        AuditService auditservice = new AuditService();
        private void connection()
        {
            string constring = ConfigurationManager.ConnectionStrings["db_conn"].ToString();
            con = new SqlConnection(constring);
        }
        private DataTable sqlQuery(string sQuery)
        {
            connection();
            SqlCommand cmd = new SqlCommand(sQuery, con);
            cmd.CommandType = CommandType.Text;
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            sd.Fill(dt);
            con.Close();

            return dt;
        }

        public List<FamilyModel> GetFamily()
        {
            List<FamilyModel> FamilyList = new List<FamilyModel>();
            DataTable dtFamily = sqlQuery("SELECT * FROM FAMILIARES ORDER BY FAMILIAR_ID ASC;");

            foreach (DataRow dr in dtFamily.Rows)
            {
                FamilyList.Add(
                    new FamilyModel
                    {
                        FAMILIAR_ID = Convert.ToInt16(dr["FAMILIAR_ID"]),
                        FAMILIAR_NOMBRE = Convert.ToString(dr["FAMILIAR_NOMBRE"]),
                        FAMILIAR_APELLIDO = Convert.ToString(dr["FAMILIAR_APELLIDO"]),
                        FAMILIAR_DOCUMENTO = Convert.ToString(dr["FAMILIAR_DOCUMENTO"]),
                        FAMILIAR_GENERO = Convert.ToString(dr["FAMILIAR_GENERO"]),
                        FAMILIAR_FECHANACIMIENTO = Convert.ToDateTime(dr["FAMILIAR_FECHANACIMIENTO"]),
                        FAMILIAR_ESTADOCIVIL_ID = Convert.ToInt16(dr["FAMILIAR_ESTADOCIVIL_ID"]),
                        FAMILIAR_TIPODOCUMENTO_ID = Convert.ToInt16(dr["FAMILIAR_TIPODOCUMENTO_ID"]),
                        FAMILIAR_OBRASOCIAL_ID = Convert.ToInt16(dr["FAMILIAR_OBRASOCIAL_ID"])
                    });
            }
            return FamilyList;
        }

        /**************** ADD NEW FAMILY *********************/

        public bool AddFamily(FamilyModel FamilyView)
        {
            Audit.AUDITORIA_MODULO = "FAMILY";
            Audit.AUDITORIA_ACCION = "CREATE";
            Audit.AUDITORIA_NOMBRECONTROLADOR = "FamilyService.AddFamily";
            try
            {
                connection();
                SqlCommand cmd = new SqlCommand("INSERT INTO FAMILIARES (familiar_nombre, familiar_apellido, familiar_documento, familiar_genero, familiar_fechanacimiento, familiar_estadocivil_id, familiar_tipodocumento_id, familiar_obrasocial_id) VALUES (@familiar_nombre, @familiar_apellido, @familiar_documento, @familiar_genero, @familiar_fechanacimiento, @familiar_estadocivil_id, @familiar_tipodocumento_id, @familiar_obrasocial_id)", con);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.Add("@familiar_nombre", SqlDbType.VarChar).Value = FamilyView.FAMILIAR_NOMBRE.ToUpper();
                cmd.Parameters.Add("@familiar_apellido", SqlDbType.VarChar).Value = FamilyView.FAMILIAR_APELLIDO.ToUpper();
                cmd.Parameters.Add("@familiar_documento", SqlDbType.VarChar).Value = FamilyView.FAMILIAR_DOCUMENTO.ToUpper();
                cmd.Parameters.Add("@familiar_genero", SqlDbType.Char).Value = FamilyView.FAMILIAR_GENERO.ToUpper();
                cmd.Parameters.Add("@familiar_fechanacimiento", SqlDbType.DateTime).Value = FamilyView.FAMILIAR_FECHANACIMIENTO;
                cmd.Parameters.Add("@familiar_estadocivil_id", SqlDbType.Int).Value = FamilyView.FAMILIAR_ESTADOCIVIL_ID;
                cmd.Parameters.Add("@familiar_tipodocumento_id", SqlDbType.Int).Value = FamilyView.FAMILIAR_TIPODOCUMENTO_ID;
                cmd.Parameters.Add("@familiar_obrasocial_id", SqlDbType.Int).Value = FamilyView.FAMILIAR_OBRASOCIAL_ID;
                con.Open();
                int i = cmd.ExecuteNonQuery();
                con.Close();

                // query que se ejecuta
                string tmp = cmd.CommandText.ToString();
                foreach (SqlParameter p in cmd.Parameters)
                {
                    tmp = tmp.Replace(p.ParameterName.ToString(), "'" + p.Value.ToString() + "'");
                }

                Audit.AUDITORIA_RASTRO = tmp;

                if (i >= 1)
                {
                    Audit.AUDITORIA_ESTADO = "OK";
                    Audit.AUDITORIA_MENSAJE = "La familia se inserto OK.";
                    auditservice.AddAudit(Audit);
                    return true;
                }
                else
                {
                    Audit.AUDITORIA_ESTADO = "ERROR";
                    Audit.AUDITORIA_MENSAJE = "La familia no se pudo insertar.";
                    auditservice.AddAudit(Audit);
                    return false;
                }
            }
            catch (Exception ex)
            {
                Audit.AUDITORIA_RASTRO = ex.Message;
                Audit.AUDITORIA_ESTADO = "ERROR";
                Audit.AUDITORIA_MENSAJE = "La familia no se pudo insertar.";
                auditservice.AddAudit(Audit);
                return false;
            }
        }

        /**************** FAMILY EDIT *********************/

        public bool UpdateFamily(FamilyModel FamilyView)
        {
            try
            {
                connection();
                SqlCommand cmd = new SqlCommand("UPDATE FAMILIARES SET familiar_nombre= @familiar_nombre, familiar_apellido = @familiar_apellido, familiar_documento = @familiar_documento, familiar_genero = @familiar_genero, familiar_fechanacimiento = @familiar_fechanacimiento, familiar_estadocivil_id = @familiar_estadocivil_id, familiar_tipodocumento_id = @familiar_tipodocumento_id , familiar_obrasocial_id = @familiar_obrasocial_id WHERE familiar_id = @familiar_id ", con);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.Add("@familiar_id", SqlDbType.Int).Value = FamilyView.FAMILIAR_ID;
                cmd.Parameters.Add("@familiar_nombre", SqlDbType.VarChar).Value = FamilyView.FAMILIAR_NOMBRE.ToUpper();
                cmd.Parameters.Add("@familiar_apellido", SqlDbType.VarChar).Value = FamilyView.FAMILIAR_APELLIDO.ToUpper();
                cmd.Parameters.Add("@familiar_documento", SqlDbType.VarChar).Value = FamilyView.FAMILIAR_DOCUMENTO.ToUpper();
                cmd.Parameters.Add("@familiar_genero", SqlDbType.Char).Value = FamilyView.FAMILIAR_GENERO.ToUpper();
                cmd.Parameters.Add("@familiar_fechanacimiento", SqlDbType.DateTime).Value = FamilyView.FAMILIAR_FECHANACIMIENTO;
                cmd.Parameters.Add("@familiar_estadocivil_id", SqlDbType.Int).Value = FamilyView.FAMILIAR_ESTADOCIVIL_ID;
                cmd.Parameters.Add("@familiar_tipodocumento_id", SqlDbType.Int).Value = FamilyView.FAMILIAR_TIPODOCUMENTO_ID;
                cmd.Parameters.Add("@familiar_obrasocial_id", SqlDbType.Int).Value = FamilyView.FAMILIAR_OBRASOCIAL_ID;
                con.Open();
                int i = cmd.ExecuteNonQuery();
                con.Close();
                string tmp = cmd.CommandText.ToString();
                foreach (SqlParameter p in cmd.Parameters)
                    tmp = tmp.Replace(p.ParameterName.ToString(), "'" + p.Value.ToString() + "'");

                if (i >= 1)
                {
                    auditservice.AddAudit(new AuditModel()
                    {
                        AUDITORIA_MODULO = "FAMILY",
                        AUDITORIA_ACCION = "UPDATE",
                        AUDITORIA_ESTADO = "OK",
                        AUDITORIA_MENSAJE = "La familia se actualizo correctamente",
                        AUDITORIA_NOMBRECONTROLADOR = "FamilyService.UpdateFamily",
                        AUDITORIA_RASTRO = tmp
                    });

                    return true;
                }
                else
                {
                    auditservice.AddAudit(new AuditModel()
                    {
                        AUDITORIA_MODULO = "FAMILY",
                        AUDITORIA_ACCION = "UPDATE",
                        AUDITORIA_ESTADO = "ERROR",
                        AUDITORIA_MENSAJE = "Hubo un error al actualizar la familia",
                        AUDITORIA_NOMBRECONTROLADOR = "FamilyService.UpdateFamily",
                        AUDITORIA_RASTRO = tmp
                    });
                    return false;
                }
            }
            catch (Exception ex)
            {
                auditservice.AddAudit(new AuditModel()
                {
                    AUDITORIA_MODULO = "FAMILY",
                    AUDITORIA_ACCION = "UPDATE",
                    AUDITORIA_ESTADO = "ERROR",
                    AUDITORIA_MENSAJE = "Hubo un error al actualizar la familia",
                    AUDITORIA_NOMBRECONTROLADOR = "FamilyService.UpdateFamily",
                    AUDITORIA_RASTRO = ex.Message
                });
                return false;
            }
        }

        /**************** FAMILY DELETE *********************/
        public bool DeleteFamily(FamilyModel FamilyView)
        {
            try
            {
                string sql = "DELETE FROM FAMILIARES WHERE FAMILIAR_ID = @familiar_id";

                connection();
                SqlCommand cmd = new SqlCommand(sql, con);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.Add("@familiar_id", SqlDbType.Int).Value = FamilyView.FAMILIAR_ID;
                con.Open();
                int i = cmd.ExecuteNonQuery();
                con.Close();
                string tmp = cmd.CommandText.ToString();
                foreach (SqlParameter p in cmd.Parameters)
                    tmp = tmp.Replace(p.ParameterName.ToString(), "'" + p.Value.ToString() + "'");

                if (i >= 1)
                {
                    auditservice.AddAudit(new AuditModel()
                    {
                        AUDITORIA_MODULO = "FAMILY",
                        AUDITORIA_ACCION = "DELETE",
                        AUDITORIA_ESTADO = "OK",
                        AUDITORIA_MENSAJE = "La familia se elimino correctamente",
                        AUDITORIA_NOMBRECONTROLADOR = "FamilyService.DeleteFamily",
                        AUDITORIA_RASTRO = tmp
                    });

                    return true;
                }
                else
                {
                    auditservice.AddAudit(new AuditModel()
                    {
                        AUDITORIA_MODULO = "FAMILY",
                        AUDITORIA_ACCION = "DELETE",
                        AUDITORIA_ESTADO = "ERROR",
                        AUDITORIA_MENSAJE = "Hubo un error al elimino la familia",
                        AUDITORIA_NOMBRECONTROLADOR = "FamilyService.DeleteFamily",
                        AUDITORIA_RASTRO = tmp
                    });
                    return false;
                }
            }
            catch (Exception ex)
            {
                auditservice.AddAudit(new AuditModel()
                {
                    AUDITORIA_MODULO = "FAMILY",
                    AUDITORIA_ACCION = "DELETE",
                    AUDITORIA_ESTADO = "ERROR",
                    AUDITORIA_MENSAJE = "Hubo un error al elimino la familia",
                    AUDITORIA_NOMBRECONTROLADOR = "FamilyService.DeleteFamily",
                    AUDITORIA_RASTRO = ex.Message
                });
                return false;
            }
        }

        public bool DeleteBond(FamilyModel FamilyView)
        {
            try
            {
                string sql = "DELETE FROM VINCULOS WHERE FAMILIAR_ID = @familiar_id";

                connection();
                SqlCommand cmd = new SqlCommand(sql, con);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.Add("@familiar_id", SqlDbType.Int).Value = FamilyView.FAMILIAR_ID;
                con.Open();
                int i = cmd.ExecuteNonQuery();
                con.Close();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public bool ExistEmployees(string dni)
        {
            try
            {
                connection();
                SqlCommand cmd = new SqlCommand("SELECT * FROM FAMILIARES WHERE EMPLEADO_DOCUMENTO = @empleado_documento;", con);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.Add("@empleado_documento", SqlDbType.VarChar).Value = dni;
                SqlDataAdapter sd = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();

                con.Open();
                sd.Fill(dt);
                if (dt.Rows.Count < 1)
                    return true;
                return false;
            }
            catch (Exception e)
            {
                return false;
            }
            finally
            {
                con.Close();
            }
        }

        public bool ExistFamily(string dni)
        {
            try
            {
                connection();
                SqlCommand cmd = new SqlCommand("SELECT * FROM FAMILIARES WHERE FAMILIAR_DOCUMENTO = @familiar_documento;", con);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.Add("@familiar_documento", SqlDbType.VarChar).Value = dni;
                SqlDataAdapter sd = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();

                con.Open();
                sd.Fill(dt);
                if (dt.Rows.Count < 1)
                    return true;
                return false;
            }
            catch (Exception e)
            {
                return false;
            }
            finally
            {
                con.Close();
            }
        }
    }
}