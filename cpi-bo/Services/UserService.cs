﻿using System.Data;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Configuration;
using System.Security.Cryptography;
using System.Net.Mail;
using System.Net;
using cpi_bo.Models;
using System;

namespace cpi_bo.Services
{
    public class UserService
    {
        private SqlConnection con;
        AuditModel Audit = new AuditModel();
        AuditService auditservice = new AuditService();
        private LogOperationService DBlog = new LogOperationService();
        private TripleDESCryptoServiceProvider des = new TripleDESCryptoServiceProvider();
        private MD5CryptoServiceProvider Hash = new MD5CryptoServiceProvider();
        private byte[] byString, bytCifrar, bytDesCifrar;

        private void connection()
        {
            string constring = ConfigurationManager.ConnectionStrings["db_conn"].ToString();
            con = new SqlConnection(constring);
        }

        public List<UserModel> GetUsers()
        {
            connection();
            List<UserModel> userlist = new List<UserModel>();

            SqlCommand cmd = new SqlCommand("SELECT * FROM USUARIOS AS A INNER JOIN PERFILES AS B ON A.usuario_perfil_id = B.perfil_id  ; ", con);
            cmd.CommandType = CommandType.Text;
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            sd.Fill(dt);
            con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                userlist.Add(
                    new UserModel
                    {
                        USUARIO_NICK = (string)dr["USUARIO_NICK"],
                        USUARIO_NOMBRE = Convert.ToString(dr["USUARIO_NOMBRE"]),
                        USUARIO_APELLIDO = Convert.ToString(dr["USUARIO_APELLIDO"]),
                        USUARIO_CLAVE = Convert.ToString(dr["USUARIO_CLAVE"]),
                        USUARIO_PERFIL_ID = Convert.ToInt16(dr["USUARIO_PERFIL_ID"]),
                        USUARIO_DESCRIPCION = Convert.ToString(dr["PERFIL_NOMBRE"]),
                        USUARIO_ESTADO_ID = Convert.ToString(dr["USUARIO_ESTADO_ID"]),
                        USUARIO_EMAIL = Convert.ToString(dr["USUARIO_EMAIL"]),
                    });
            }
            return userlist;
        }

        //// ********** VIEW USERS DETAILS ********************
        public UserModel GetUsersXId(string sUserNick)
        {
            connection();
            con.Open();
            UserModel userView = new UserModel();
            SqlCommand cmd = new SqlCommand("SELECT * FROM USUARIOS WHERE usuario_nick = @NICK_NAME", con);
            cmd.Parameters.Add("@NICK_NAME", SqlDbType.VarChar).Value = sUserNick;

            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                userView.USUARIO_NICK = (string)reader["usuario_nick"];
                userView.USUARIO_NOMBRE = Convert.ToString(reader["usuario_nombre"]);
                userView.USUARIO_APELLIDO = Convert.ToString(reader["usuario_apellido"]);
                userView.USUARIO_CLAVE = Convert.ToString(reader["usuario_clave"]);
                userView.USUARIO_EMAIL = Convert.ToString(reader["usuario_email"]);
                userView.USUARIO_PERFIL_ID = Convert.ToInt16(reader["usuario_perfil_id"]);
                userView.USUARIO_ESTADO_ID = Convert.ToString(reader["usuario_estado_id"]);
            }
            con.Close();
            return userView;
        }

        //// ********** ADD USER ********************
        public bool AddUser(UserModel userView)
        {
            Audit.AUDITORIA_MODULO = "USER";
            Audit.AUDITORIA_ACCION = "CREATE";
            Audit.AUDITORIA_NOMBRECONTROLADOR = "UserService.AddUser";
            try
            {
                string sql = "INSERT INTO USUARIOS (usuario_nick,usuario_nombre,usuario_apellido,usuario_clave,usuario_email,usuario_perfil_id,usuario_estado_id) VALUES (@usuario_nick,@usuario_nombre,@usuario_apellido,@usuario_clave,@usuario_email,@usuario_perfil_id,@usuario_estado_id)";
                connection();
                SqlCommand cmd = new SqlCommand(sql, con);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.Add("@usuario_nick", SqlDbType.VarChar).Value = userView.USUARIO_NICK.ToUpper();
                cmd.Parameters.Add("@usuario_nombre", SqlDbType.VarChar).Value = userView.USUARIO_NOMBRE.ToUpper();
                cmd.Parameters.Add("@usuario_apellido", SqlDbType.VarChar).Value = userView.USUARIO_APELLIDO.ToUpper();
                cmd.Parameters.Add("@usuario_clave", SqlDbType.VarChar).Value = Encriptar(userView.USUARIO_CLAVE);
                cmd.Parameters.Add("@usuario_email", SqlDbType.VarChar).Value = userView.USUARIO_EMAIL;
                cmd.Parameters.Add("@usuario_perfil_id", SqlDbType.VarChar).Value = userView.USUARIO_PERFIL_ID;
                cmd.Parameters.Add("@usuario_estado_id", SqlDbType.VarChar).Value = userView.USUARIO_ESTADO_ID;
                con.Open();
                int i = cmd.ExecuteNonQuery();
                con.Close();
                // query que se ejecuta
                string tmp = cmd.CommandText.ToString();
                foreach (SqlParameter p in cmd.Parameters)
                {
                    tmp = tmp.Replace(p.ParameterName.ToString(), "'" + p.Value.ToString() + "'");
                }

                Audit.AUDITORIA_RASTRO = tmp;

                if (i >= 1)
                {
                    Audit.AUDITORIA_ESTADO = "OK";
                    Audit.AUDITORIA_MENSAJE = "El usuario se inserto OK.";
                    auditservice.AddAudit(Audit);
                    return true;
                }
                else
                {
                    Audit.AUDITORIA_ESTADO = "ERROR";
                    Audit.AUDITORIA_MENSAJE = "El usuario no se pudo insertar.";
                    auditservice.AddAudit(Audit);
                    return false;
                }
            }
            catch (Exception ex)
            {
                Audit.AUDITORIA_RASTRO = ex.Message;
                Audit.AUDITORIA_ESTADO = "ERROR";
                Audit.AUDITORIA_MENSAJE = "El usuario no se pudo insertar.";
                auditservice.AddAudit(Audit);
                return false;
            }
        }

        public bool UpdateUser(UserModel userView)
        {
            try
            {
                string sql = "UPDATE USUARIOS SET usuario_nombre = @usuario_nombre," +
                                             " usuario_apellido = @usuario_apellido ,usuario_email = @usuario_email , " +
                                             " usuario_perfil_id = @usuario_perfil_id where usuario_nick = @usuario_nick";
                connection();
                SqlCommand cmd = new SqlCommand(sql, con);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.Add("@usuario_nick", SqlDbType.VarChar).Value = userView.USUARIO_NICK.ToUpper();
                cmd.Parameters.Add("@usuario_nombre", SqlDbType.VarChar).Value = userView.USUARIO_NOMBRE.ToUpper();
                cmd.Parameters.Add("@usuario_apellido", SqlDbType.VarChar).Value = userView.USUARIO_APELLIDO.ToUpper();
                cmd.Parameters.Add("@usuario_email", SqlDbType.VarChar).Value = userView.USUARIO_EMAIL;
                cmd.Parameters.Add("@usuario_perfil_id", SqlDbType.Int).Value = userView.USUARIO_PERFIL_ID;
                con.Open();
                int i = cmd.ExecuteNonQuery();
                con.Close();
                string tmp = cmd.CommandText.ToString();
                foreach (SqlParameter p in cmd.Parameters)
                    tmp = tmp.Replace(p.ParameterName.ToString(), "'" + p.Value.ToString() + "'");

                if (i >= 1)
                {
                    auditservice.AddAudit(new AuditModel()
                    {
                        AUDITORIA_MODULO = "USER",
                        AUDITORIA_ACCION = "UPDATE",
                        AUDITORIA_ESTADO = "OK",
                        AUDITORIA_MENSAJE = "El usuario se actualizo correctamente",
                        AUDITORIA_NOMBRECONTROLADOR = "UserService.UpdateUser",
                        AUDITORIA_RASTRO = tmp
                    });

                    return true;
                }
                else
                {
                    auditservice.AddAudit(new AuditModel()
                    {
                        AUDITORIA_MODULO = "USER",
                        AUDITORIA_ACCION = "UPDATE",
                        AUDITORIA_ESTADO = "ERROR",
                        AUDITORIA_MENSAJE = "Hubo un error al actualizar el usuario",
                        AUDITORIA_NOMBRECONTROLADOR = "UserService.UpdateUser",
                        AUDITORIA_RASTRO = tmp
                    });
                    return false;
                }
            }
            catch (Exception ex)
            {
                auditservice.AddAudit(new AuditModel()
                {
                    AUDITORIA_MODULO = "USER",
                    AUDITORIA_ACCION = "UPDATE",
                    AUDITORIA_ESTADO = "ERROR",
                    AUDITORIA_MENSAJE = "Hubo un error al actualizar el usuario",
                    AUDITORIA_NOMBRECONTROLADOR = "UserService.UpdateUser",
                    AUDITORIA_RASTRO = ex.Message
                });
                return false;
            }
        }

        public bool ValidateUser(LoginModel userView)
        {
            try
            {
                connection();
                SqlCommand cmd = new SqlCommand("SELECT * FROM usuarios WHERE USUARIO_NICK = @user_nick AND USUARIO_CLAVE = @user_password AND usuario_estado_id != 'BAJA_USER';", con);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.Add("@user_nick", SqlDbType.VarChar).Value = userView.USUARIO_NICK;
                cmd.Parameters.Add("@user_password", SqlDbType.VarChar).Value = Encriptar(userView.USUARIO_CLAVE);
                SqlDataAdapter sd = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();

                con.Open();
                sd.Fill(dt);
                if (dt.Rows.Count < 1)
                {
                    return false;
                }
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
            finally
            {
                con.Close();
            }
        }
        
        private string Encriptar(string EncriptString)
        {
            string strEncriptar = "";

            if (EncriptString != string.Empty)
            {
                try
                {
                    des.Key = Hash.ComputeHash(System.Text.ASCIIEncoding.ASCII.GetBytes(EncriptString));
                    des.Mode = System.Security.Cryptography.CipherMode.ECB;
                    System.Security.Cryptography.ICryptoTransform DESEencrypter = des.CreateEncryptor();
                    byte[] buffer = System.Text.ASCIIEncoding.ASCII.GetBytes(EncriptString);
                    strEncriptar = Convert.ToBase64String(DESEencrypter.TransformFinalBlock(buffer, 0, buffer.Length));

                }
                catch (Exception e)
                {
                    return "";
                }


            }
            return strEncriptar;

        }
        public bool UpdatePassword(UserModel userView)
        {
            try
            {
                string sql = "UPDATE USUARIOS SET usuario_clave = @usuario_clave " +
                                             " where usuario_nick = @usuario_nick ; ";

                connection();
                SqlCommand cmd = new SqlCommand(sql, con);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.Add("@usuario_nick", SqlDbType.VarChar).Value = userView.USUARIO_NICK.ToUpper();
                cmd.Parameters.Add("@usuario_clave", SqlDbType.VarChar).Value = Encriptar(userView.USUARIO_CLAVE);
                con.Open();
                int i = cmd.ExecuteNonQuery();
                con.Close();
                string tmp = cmd.CommandText.ToString();
                foreach (SqlParameter p in cmd.Parameters)
                    tmp = tmp.Replace(p.ParameterName.ToString(), "'" + p.Value.ToString() + "'");

                if (i >= 1)
                {
                    auditservice.AddAudit(new AuditModel()
                    {
                        AUDITORIA_MODULO = "USER",
                        AUDITORIA_ACCION = "UPDATE",
                        AUDITORIA_ESTADO = "OK",
                        AUDITORIA_MENSAJE = "La contraseña del usuario se actualizo correctamente",
                        AUDITORIA_NOMBRECONTROLADOR = "UserService.UpdatePassword",
                        AUDITORIA_RASTRO = tmp
                    });

                    return true;
                }
                else
                {
                    auditservice.AddAudit(new AuditModel()
                    {
                        AUDITORIA_MODULO = "USER",
                        AUDITORIA_ACCION = "UPDATE",
                        AUDITORIA_ESTADO = "ERROR",
                        AUDITORIA_MENSAJE = "Hubo un error al actualizar la contraseña del usuario",
                        AUDITORIA_NOMBRECONTROLADOR = "UserService.UpdatePassword",
                        AUDITORIA_RASTRO = tmp
                    });
                    return false;
                }
            }
            catch (Exception ex)
            {
                auditservice.AddAudit(new AuditModel()
                {
                    AUDITORIA_MODULO = "USER",
                    AUDITORIA_ACCION = "UPDATE",
                    AUDITORIA_ESTADO = "ERROR",
                    AUDITORIA_MENSAJE = "Hubo un error al actualizar la contraseña del usuario",
                    AUDITORIA_NOMBRECONTROLADOR = "UserService.UpdatePassword",
                    AUDITORIA_RASTRO = ex.Message
                });
                return false;
            }
        }
        public bool ExistUser(string nick)
        {
            try
            {
                connection();
                SqlCommand cmd = new SqlCommand("SELECT * FROM usuarios WHERE USUARIO_NICK = @user_nick;", con);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.Add("@user_nick", SqlDbType.VarChar).Value = nick;
                SqlDataAdapter sd = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();

                con.Open();
                sd.Fill(dt);
                if (dt.Rows.Count < 1)
                    return true;               
                return false;
            }
            catch (Exception e)
            {
                return false;
            }
            finally
            {
                con.Close();
            }
        }

        public bool RecoveryPassword(UserModel viewUser)
        {
            try
            {
                var user = GetUsersXId(viewUser.USUARIO_NICK);
                string gmail_ID = "bocpitest@gmail.com";
                string gmail_PWD = "softwork";
                if (user.USUARIO_NICK != "")
                {
                    MailMessage email = new MailMessage();
                    email.From = new MailAddress("bocpitest@gmail.com");
                    email.To.Add(user.USUARIO_EMAIL);
                    email.Subject = "Generación de nueva clave";
                    var newPWD = user.USUARIO_NICK + DateTime.Now.Year;
                    user.USUARIO_CLAVE = newPWD;
                    email.Body = "Hola, tu nueva password es : " + newPWD;
                    email.IsBodyHtml = true;
                    //smtp server details

                    using (SmtpClient smtpc = new SmtpClient("smtp.gmail.com"))
                    {
                        smtpc.Port = 587;
                        smtpc.UseDefaultCredentials = false;
                        smtpc.EnableSsl = true;
                        smtpc.Credentials = new NetworkCredential(gmail_ID, gmail_PWD);
                        smtpc.Send(email);
                    }
                   UpdatePassword(user);
                    return true;
                }
                return false;
            }
            catch (Exception e)
            {
                
                return false;
            }
        }
    }
}