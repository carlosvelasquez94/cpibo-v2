﻿using System.Data;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Configuration;
using cpi_bo.Models;
using System;

namespace cpi_bo.Services
{
    public class ResponsibleService
    {
        private SqlConnection con;
        AuditModel Audit = new AuditModel();
        AuditService auditservice = new AuditService();

        private void connection()
        {
            string constring = ConfigurationManager.ConnectionStrings["db_conn"].ToString();
            con = new SqlConnection(constring);
        }

        public List<ResponsibleModel> GetResponsables()
        {
            connection();
            List<ResponsibleModel> userlist = new List<ResponsibleModel>();

            SqlCommand cmd = new SqlCommand("SELECT * FROM RESPONSABLES AS A INNER JOIN TIPONACIONALIDADES AS B ON A.RESPONSABLE_TIPONACIONALIDAD_ID = B.TIPONACIONALIDAD_ID INNER JOIN TIPODOCUMENTOS AS C ON A.RESPONSABLE_TIPODOCUMENTO_ID = C.TIPODOCUMENTO_ID; ", con);
            cmd.CommandType = CommandType.Text;
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            sd.Fill(dt);
            con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                userlist.Add(
                    new ResponsibleModel
                    {
                        RESPONSABLE_ID = (int)dr["RESPONSABLE_ID"],
                        RESPONSABLE_NOMBRE = Convert.ToString(dr["RESPONSABLE_NOMBRE"]),
                        RESPONSABLE_APELLIDO = Convert.ToString(dr["RESPONSABLE_APELLIDO"]),
                        RESPONSABLE_DOCUMENTO = Convert.ToString(dr["RESPONSABLE_DOCUMENTO"]),
                        RESPONSABLE_TIPODOCUMENTO_ID = Convert.ToInt32(dr["RESPONSABLE_TIPODOCUMENTO_ID"]),
                        RESPONSABLE_TIPODOCUMENTO_DESCRIPCION = Convert.ToString(dr["TIPODOCUMENTO_DESCRIPCION"]),
                        RESPONSABLE_TIPONACIONALIDAD_DESCRIPCION = Convert.ToString(dr["TIPONACIONALIDAD_DESCRIPCION"]),
                    });
            }
            return userlist;
        }

        public bool AddResponsable(ResponsibleModel respView)
        {
            Audit.AUDITORIA_MODULO = "RESPONSIBLE";
            Audit.AUDITORIA_ACCION = "CREATE";
            Audit.AUDITORIA_NOMBRECONTROLADOR = "ResponsibleService.AddResponsable";
            try
            {
                string sql = "INSERT INTO RESPONSABLES (responsable_nombre,responsable_apellido,responsable_documento,responsable_tiponacionalidad_id,responsable_tipodocumento_id) VALUES (@responsable_nombre,@responsable_apellido,@responsable_documento,@responsable_tiponacionalidad_id,@responsable_tipodocumento_id)";
                connection();
                SqlCommand cmd = new SqlCommand(sql, con);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.Add("@responsable_nombre", SqlDbType.VarChar).Value = respView.RESPONSABLE_NOMBRE.ToUpper();
                cmd.Parameters.Add("@responsable_apellido", SqlDbType.VarChar).Value = respView.RESPONSABLE_APELLIDO.ToUpper();
                cmd.Parameters.Add("@responsable_documento", SqlDbType.VarChar).Value = respView.RESPONSABLE_DOCUMENTO.ToUpper();
                cmd.Parameters.Add("@responsable_tiponacionalidad_id", SqlDbType.Int).Value = respView.RESPONSABLE_TIPONACIONALIDAD_ID;
                cmd.Parameters.Add("@responsable_tipodocumento_id", SqlDbType.Int).Value = respView.RESPONSABLE_TIPODOCUMENTO_ID;
                con.Open();
                int i = cmd.ExecuteNonQuery();
                con.Close();
                // query que se ejecuta
                string tmp = cmd.CommandText.ToString();
                foreach (SqlParameter p in cmd.Parameters)
                {
                    tmp = tmp.Replace(p.ParameterName.ToString(), "'" + p.Value.ToString() + "'");
                }

                Audit.AUDITORIA_RASTRO = tmp;

                if (i >= 1)
                {
                    Audit.AUDITORIA_ESTADO = "OK";
                    Audit.AUDITORIA_MENSAJE = "El responsable se inserto OK.";
                    auditservice.AddAudit(Audit);
                    return true;
                }
                else
                {
                    Audit.AUDITORIA_ESTADO = "ERROR";
                    Audit.AUDITORIA_MENSAJE = "El responsable no se pudo insertar.";
                    auditservice.AddAudit(Audit);
                    return false;
                }
            }
            catch (Exception ex)
            {
                Audit.AUDITORIA_RASTRO = ex.Message;
                Audit.AUDITORIA_ESTADO = "ERROR";
                Audit.AUDITORIA_MENSAJE = "El responsable no se pudo insertar.";
                auditservice.AddAudit(Audit);
                return false;
            }
        }

        public bool UpdateResponsable(ResponsibleModel respView)
        {
            try
            {
                string sql = "UPDATE RESPONSABLES SET responsable_nombre = @responsable_nombre," +
                                             " responsable_apellido = @responsable_apellido ,responsable_documento = @responsable_documento , " +
                                             " responsable_tiponacionalidad_id = @responsable_tiponacionalidad_id ,responsable_tipodocumento_id = @responsable_tipodocumento_id where responsable_id = @responsable_id";
                connection();
                SqlCommand cmd = new SqlCommand(sql, con);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.Add("@responsable_id", SqlDbType.Int).Value = respView.RESPONSABLE_ID;
                cmd.Parameters.Add("@responsable_nombre", SqlDbType.VarChar).Value = respView.RESPONSABLE_NOMBRE.ToUpper();
                cmd.Parameters.Add("@responsable_apellido", SqlDbType.VarChar).Value = respView.RESPONSABLE_APELLIDO.ToUpper();
                cmd.Parameters.Add("@responsable_documento", SqlDbType.VarChar).Value = respView.RESPONSABLE_DOCUMENTO.ToUpper();
                cmd.Parameters.Add("@responsable_tiponacionalidad_id", SqlDbType.Int).Value = respView.RESPONSABLE_TIPONACIONALIDAD_ID;
                cmd.Parameters.Add("@responsable_tipodocumento_id", SqlDbType.Int).Value = respView.RESPONSABLE_TIPODOCUMENTO_ID;
                con.Open();
                int i = cmd.ExecuteNonQuery();
                con.Close();
                string tmp = cmd.CommandText.ToString();
                foreach (SqlParameter p in cmd.Parameters)
                    tmp = tmp.Replace(p.ParameterName.ToString(), "'" + p.Value.ToString() + "'");

                if (i >= 1)
                {
                    auditservice.AddAudit(new AuditModel()
                    {
                        AUDITORIA_MODULO = "RESPONSIBLE",
                        AUDITORIA_ACCION = "UPDATE",
                        AUDITORIA_ESTADO = "OK",
                        AUDITORIA_MENSAJE = "El responsable se actualizo correctamente",
                        AUDITORIA_NOMBRECONTROLADOR = "ResponsibleService.UpdateResponsable",
                        AUDITORIA_RASTRO = tmp
                    });

                    return true;
                }
                else
                {
                    auditservice.AddAudit(new AuditModel()
                    {
                        AUDITORIA_MODULO = "RESPONSIBLE",
                        AUDITORIA_ACCION = "UPDATE",
                        AUDITORIA_ESTADO = "ERROR",
                        AUDITORIA_MENSAJE = "Hubo un error al actualizar el responsable",
                        AUDITORIA_NOMBRECONTROLADOR = "ResponsibleService.UpdateResponsable",
                        AUDITORIA_RASTRO = tmp
                    });
                    return false;
                }
            }
            catch (Exception ex)
            {
                auditservice.AddAudit(new AuditModel()
                {
                    AUDITORIA_MODULO = "RESPONSIBLE",
                    AUDITORIA_ACCION = "UPDATE",
                    AUDITORIA_ESTADO = "ERROR",
                    AUDITORIA_MENSAJE = "Hubo un error al actualizar el responsable",
                    AUDITORIA_NOMBRECONTROLADOR = "ResponsibleService.UpdateResponsable",
                    AUDITORIA_RASTRO = ex.Message
                });
                return false;
            }
        }

        public bool ValidateResponsible(ResponsibleModel respView)
        {
            try
            {
                connection();
                SqlCommand cmd = new SqlCommand("select r.RESPONSABLE_ID from RESPONSABLES as r inner join INFANTES_RESPONSABLES as i on r.RESPONSABLE_ID=i.RESPONSABLE_ID where i.RESPONSABLE_ID = @RESPONSABLE_ID", con);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.Add("@RESPONSABLE_ID", SqlDbType.Int).Value = respView.RESPONSABLE_ID;
                SqlDataAdapter sd = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();

                con.Open();
                sd.Fill(dt);
                if (dt.Rows.Count < 1)
                {
                    return true;
                }
                return false;
            }
            catch (Exception e)
            {
                return false;
            }
            finally
            {
                con.Close();
            }
        }

        public bool SetStatusInactive(ResponsibleModel respView)
        {
            try
            {
                string sql = "UPDATE RESPONSABLES SET responsable_estado_id = @responsable_estado_id " +
                                             " where responsable_id = @responsable_id";

                connection();
                SqlCommand cmd = new SqlCommand(sql, con);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.Add("@responsable_id", SqlDbType.Int).Value = respView.RESPONSABLE_ID;
                cmd.Parameters.Add("@responsable_estado_id", SqlDbType.VarChar).Value = "INAC_RESP";
                con.Open();
                int i = cmd.ExecuteNonQuery();
                con.Close();
                string tmp = cmd.CommandText.ToString();
                foreach (SqlParameter p in cmd.Parameters)
                    tmp = tmp.Replace(p.ParameterName.ToString(), "'" + p.Value.ToString() + "'");

                if (i >= 1)
                {
                    auditservice.AddAudit(new AuditModel()
                    {
                        AUDITORIA_MODULO = "RESPONSIBLE",
                        AUDITORIA_ACCION = "DELETE",
                        AUDITORIA_ESTADO = "OK",
                        AUDITORIA_MENSAJE = "El responsable se elimino correctamente",
                        AUDITORIA_NOMBRECONTROLADOR = "ResponsibleService.DeleteResponsable",
                        AUDITORIA_RASTRO = tmp
                    });

                    return true;
                }
                else
                {
                    auditservice.AddAudit(new AuditModel()
                    {
                        AUDITORIA_MODULO = "RESPONSIBLE",
                        AUDITORIA_ACCION = "DELETE",
                        AUDITORIA_ESTADO = "ERROR",
                        AUDITORIA_MENSAJE = "Hubo un error al elimino el responsable",
                        AUDITORIA_NOMBRECONTROLADOR = "ResponsibleService.DeleteResponsable",
                        AUDITORIA_RASTRO = tmp
                    });
                    return false;
                }
            }
            catch (Exception ex)
            {
                auditservice.AddAudit(new AuditModel()
                {
                    AUDITORIA_MODULO = "RESPONSIBLE",
                    AUDITORIA_ACCION = "DELETE",
                    AUDITORIA_ESTADO = "ERROR",
                    AUDITORIA_MENSAJE = "Hubo un error al elimino el responsable",
                    AUDITORIA_NOMBRECONTROLADOR = "ResponsibleService.DeleteResponsable",
                    AUDITORIA_RASTRO = ex.Message
                });
                return false;
            }
        }
    }
}