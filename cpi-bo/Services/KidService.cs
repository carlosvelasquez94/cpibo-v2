﻿using System.Data;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Configuration;
using System.Net;
using cpi_bo.Models;
using System;

namespace cpi_bo.Services
{
    public class KidService
    {
        private SqlConnection con;
        AuditModel Audit = new AuditModel();
        AuditService auditservice = new AuditService();
        private void connection()
        {
            string constring = ConfigurationManager.ConnectionStrings["db_conn"].ToString();
            con = new SqlConnection(constring);
        }

        public List<KidModel> GetKids()
        {
            connection();
            List<KidModel> kidlist = new List<KidModel>();

            SqlCommand cmd = new SqlCommand("SELECT * FROM INFANTES AS A INNER JOIN SALAS AS B ON A.infante_sala_id = B.sala_id ;", con);
            cmd.CommandType = CommandType.Text;
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            sd.Fill(dt);
            con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                kidlist.Add(
                    new KidModel
                    {
                        INFANTE_ID = (int)dr["INFANTE_ID"],
                        INFANTE_NOMBRE = Convert.ToString(dr["INFANTE_NOMBRE"]),
                        INFANTE_APELLIDO = Convert.ToString(dr["INFANTE_APELLIDO"]),
                        INFANTE_DOCUMENTO = Convert.ToString(dr["INFANTE_DOCUMENTO"]),
                        INFANTE_TIPODOCUMENTO_ID = Convert.ToInt32(dr["INFANTE_TIPODOCUMENTO_ID"]),
                        INFANTE_GENERO = Convert.ToChar(dr["INFANTE_GENERO"]),
                        INFANTE_NACIONALIDAD_ID = Convert.ToInt32(dr["INFANTE_NACIONALIDAD_ID"]),
                        INFANTE_FECHANACIMIENTO = Convert.ToDateTime(dr["INFANTE_FECHANACIMIENTO"]),
                        INFANTE_FECHAINGRESO = Convert.ToDateTime(dr["INFANTE_FECHAINGRESO"]),
                        INFANTE_DOMICILIO = Convert.ToString(dr["INFANTE_DOMICILIO"]),
                        INFANTE_LOCALIDAD_CODIGOPOSTAL = Convert.ToString(dr["INFANTE_LOCALIDAD_CODIGOPOSTAL"]),
                        INFANTE_SALA_ID = Convert.ToInt32(dr["INFANTE_SALA_ID"]),
                        INFANTE_DESCRIPCION = Convert.ToString(dr["SALA_DESCRIPCION"]),
                        INFANTE_ESTADO_ID = Convert.ToString(dr["INFANTE_ESTADO_ID"]),
                        INFANTE_FECHAEGRESO = Convert.ToDateTime(dr["INFANTE_FECHAEGRESO"]),
                        INFANTE_PUNTAJETOTAL = Convert.ToInt32(dr["INFANTE_PUNTAJETOTAL"])
                    });
            }
            return kidlist;
        }

        public bool AddKid(KidModel kidView)
        {
            Audit.AUDITORIA_MODULO = "KID";
            Audit.AUDITORIA_ACCION = "CREATE";
            Audit.AUDITORIA_NOMBRECONTROLADOR = "KidService.AddKid";
            try
            {
                string sql = "INSERT INTO INFANTES (infante_nombre,infante_apellido,infante_documento,infante_tipodocumento_id,infante_genero,infante_nacionalidad_id,infante_fechanacimiento,infante_fechaingreso,infante_domicilio,infante_localidad_codigopostal," +
                    "infante_sala_id,infante_estado_id,infante_fechaegreso,infante_puntajetotal) VALUES (@INFANTE_NOMBRE,@INFANTE_APELLIDO,@INFANTE_DOCUMENTO,@INFANTE_TIPODOCUMENTO_ID,@INFANTE_GENERO,@INFANTE_NACIONALIDAD_ID,@INFANTE_FECHANACIMIENTO,@INFANTE_FECHAINGRESO" +
                    ",@INFANTE_DOMICILIO,@INFANTE_LOCALIDAD_CODIGOPOSTAL,@INFANTE_SALA_ID,@INFANTE_ESTADO_ID,@INFANTE_FECHAEGRESO,@INFANTE_PUNTAJETOTAL)";
                connection();
                SqlCommand cmd = new SqlCommand(sql, con);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.Add("@INFANTE_NOMBRE", SqlDbType.VarChar).Value = kidView.INFANTE_NOMBRE.ToUpper();
                cmd.Parameters.Add("@INFANTE_APELLIDO", SqlDbType.VarChar).Value = kidView.INFANTE_APELLIDO.ToUpper();
                cmd.Parameters.Add("@INFANTE_DOCUMENTO", SqlDbType.VarChar).Value = kidView.INFANTE_DOCUMENTO.ToUpper();
                cmd.Parameters.Add("@INFANTE_TIPODOCUMENTO_ID", SqlDbType.Int).Value = kidView.INFANTE_TIPODOCUMENTO_ID;
                cmd.Parameters.Add("@INFANTE_GENERO", SqlDbType.Char).Value = kidView.INFANTE_GENERO;
                cmd.Parameters.Add("@INFANTE_NACIONALIDAD_ID", SqlDbType.Int).Value = kidView.INFANTE_NACIONALIDAD_ID;
                cmd.Parameters.Add("@INFANTE_FECHANACIMIENTO", SqlDbType.Date).Value = kidView.INFANTE_FECHANACIMIENTO;
                cmd.Parameters.Add("@INFANTE_FECHAINGRESO", SqlDbType.Date).Value = kidView.INFANTE_FECHAINGRESO;
                cmd.Parameters.Add("@INFANTE_DOMICILIO", SqlDbType.VarChar).Value = kidView.INFANTE_DOMICILIO;
                cmd.Parameters.Add("@INFANTE_LOCALIDAD_CODIGOPOSTAL", SqlDbType.VarChar).Value = kidView.INFANTE_LOCALIDAD_CODIGOPOSTAL;
                cmd.Parameters.Add("@INFANTE_SALA_ID", SqlDbType.Int).Value = kidView.INFANTE_SALA_ID;
                cmd.Parameters.Add("@INFANTE_ESTADO_ID", SqlDbType.VarChar).Value = "WAIT_INF";
                cmd.Parameters.Add("@INFANTE_FECHAEGRESO", SqlDbType.Date).Value = kidView.INFANTE_FECHAEGRESO;
                cmd.Parameters.Add("@INFANTE_PUNTAJETOTAL", SqlDbType.Int).Value = 0;
                con.Open();
                int i = cmd.ExecuteNonQuery();
                con.Close();// query que se ejecuta
                string tmp = cmd.CommandText.ToString();
                foreach (SqlParameter p in cmd.Parameters)
                {
                    tmp = tmp.Replace(p.ParameterName.ToString(), "'" + p.Value.ToString() + "'");
                }

                Audit.AUDITORIA_RASTRO = tmp;

                if (i >= 1)
                {
                    Audit.AUDITORIA_ESTADO = "OK";
                    Audit.AUDITORIA_MENSAJE = "El infante se inserto OK.";
                    auditservice.AddAudit(Audit);
                    return true;
                }
                else
                {
                    Audit.AUDITORIA_ESTADO = "ERROR";
                    Audit.AUDITORIA_MENSAJE = "El infante no se pudo insertar.";
                    auditservice.AddAudit(Audit);
                    return false;
                }
            }
            catch (Exception ex)
            {
                Audit.AUDITORIA_RASTRO = ex.Message;
                Audit.AUDITORIA_ESTADO = "ERROR";
                Audit.AUDITORIA_MENSAJE = "El infante no se pudo insertar.";
                auditservice.AddAudit(Audit);
                return false;
            }
        }


        public KidModel GetKidXId(int kidId)
        {
            connection();
            con.Open();
            KidModel kidView = new KidModel();
            SqlCommand cmd = new SqlCommand("SELECT * FROM INFANTES WHERE infante_id = @INFANTE_ID", con);
            cmd.Parameters.Add("@INFANTE_ID", SqlDbType.Int).Value = kidId;

            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                kidView.INFANTE_ID = (int)reader["INFANTE_ID"];
                kidView.INFANTE_NOMBRE = Convert.ToString(reader["INFANTE_NOMBRE"]);
                kidView.INFANTE_APELLIDO = Convert.ToString(reader["INFANTE_APELLIDO"]);
                kidView.INFANTE_DOCUMENTO = Convert.ToString(reader["INFANTE_DOCUMENTO"]);
                kidView.INFANTE_TIPODOCUMENTO_ID = Convert.ToInt32(reader["INFANTE_TIPODOCUMENTO_ID"]);
                kidView.INFANTE_GENERO = Convert.ToChar(reader["INFANTE_GENERO"]);
                kidView.INFANTE_NACIONALIDAD_ID = Convert.ToInt32(reader["INFANTE_NACIONALIDAD_ID"]);
                kidView.INFANTE_FECHANACIMIENTO = Convert.ToDateTime(reader["INFANTE_FECHANACIMIENTO"]);
                kidView.INFANTE_FECHAINGRESO = Convert.ToDateTime(reader["INFANTE_FECHAINGRESO"]);
                kidView.INFANTE_DOMICILIO = Convert.ToString(reader["INFANTE_DOMICILIO"]);
                kidView.INFANTE_LOCALIDAD_CODIGOPOSTAL = Convert.ToString(reader["INFANTE_LOCALIDAD_CODIGOPOSTAL"]);
                kidView.INFANTE_SALA_ID = Convert.ToInt32(reader["INFANTE_SALA_ID"]);
                kidView.INFANTE_ESTADO_ID = Convert.ToString(reader["INFANTE_ESTADO_ID"]);
                kidView.INFANTE_FECHAEGRESO = Convert.ToDateTime(reader["INFANTE_FECHAEGRESO"]);
                kidView.INFANTE_PUNTAJETOTAL = Convert.ToInt32(reader["INFANTE_PUNTAJETOTAL"]);
            }
            con.Close();
            return kidView;
        }

        public bool UpdateKid(KidModel kidView)
        {
            string sql = "";
            try
            {
                if (kidView.INFANTE_FECHAEGRESO != null)
                {
                    sql = "UPDATE INFANTES SET INFANTE_NOMBRE = @INFANTE_NOMBRE, INFANTE_APELLIDO = @INFANTE_APELLIDO, INFANTE_DOCUMENTO = @INFANTE_DOCUMENTO," +
                                             " INFANTE_TIPODOCUMENTO_ID = @INFANTE_TIPODOCUMENTO_ID, INFANTE_GENERO = @INFANTE_GENERO, INFANTE_NACIONALIDAD_ID = @INFANTE_NACIONALIDAD_ID," +
                                             " INFANTE_FECHANACIMIENTO = @INFANTE_FECHANACIMIENTO,  INFANTE_FECHAINGRESO = @INFANTE_FECHAINGRESO, INFANTE_DOMICILIO = @INFANTE_DOMICILIO, INFANTE_LOCALIDAD_CODIGOPOSTAL = @INFANTE_LOCALIDAD_CODIGOPOSTAL," +
                                             " INFANTE_SALA_ID = @INFANTE_SALA_ID, INFANTE_FECHAEGRESO = @INFANTE_FECHAEGRESO where INFANTE_ID = @INFANTE_ID";
                } else {
                sql = "UPDATE INFANTES SET INFANTE_NOMBRE = @INFANTE_NOMBRE, INFANTE_APELLIDO = @INFANTE_APELLIDO, INFANTE_DOCUMENTO = @INFANTE_DOCUMENTO," +
                                             " INFANTE_TIPODOCUMENTO_ID = @INFANTE_TIPODOCUMENTO_ID, INFANTE_GENERO = @INFANTE_GENERO, INFANTE_NACIONALIDAD_ID = @INFANTE_NACIONALIDAD_ID," +
                                             " INFANTE_FECHANACIMIENTO = @INFANTE_FECHANACIMIENTO,  INFANTE_FECHAINGRESO = @INFANTE_FECHAINGRESO, INFANTE_DOMICILIO = @INFANTE_DOMICILIO, INFANTE_LOCALIDAD_CODIGOPOSTAL = @INFANTE_LOCALIDAD_CODIGOPOSTAL," +
                                             " INFANTE_SALA_ID = @INFANTE_SALA_ID where INFANTE_ID = @INFANTE_ID";
                }
                connection();
                SqlCommand cmd = new SqlCommand(sql, con);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.Add("@INFANTE_ID", SqlDbType.Int).Value = kidView.INFANTE_ID;
                cmd.Parameters.Add("@INFANTE_NOMBRE", SqlDbType.VarChar).Value = kidView.INFANTE_NOMBRE.ToUpper();
                cmd.Parameters.Add("@INFANTE_APELLIDO", SqlDbType.VarChar).Value = kidView.INFANTE_APELLIDO.ToUpper();
                cmd.Parameters.Add("@INFANTE_DOCUMENTO", SqlDbType.VarChar).Value = kidView.INFANTE_DOCUMENTO.ToUpper();
                cmd.Parameters.Add("@INFANTE_TIPODOCUMENTO_ID", SqlDbType.Int).Value = kidView.INFANTE_TIPODOCUMENTO_ID;
                cmd.Parameters.Add("@INFANTE_GENERO", SqlDbType.Char).Value = kidView.INFANTE_GENERO;
                cmd.Parameters.Add("@INFANTE_NACIONALIDAD_ID", SqlDbType.Int).Value = kidView.INFANTE_NACIONALIDAD_ID;
                cmd.Parameters.Add("@INFANTE_FECHANACIMIENTO", SqlDbType.Date).Value = kidView.INFANTE_FECHANACIMIENTO;
                cmd.Parameters.Add("@INFANTE_FECHAINGRESO", SqlDbType.Date).Value = kidView.INFANTE_FECHAINGRESO;
                cmd.Parameters.Add("@INFANTE_DOMICILIO", SqlDbType.VarChar).Value = kidView.INFANTE_DOMICILIO;
                cmd.Parameters.Add("@INFANTE_LOCALIDAD_CODIGOPOSTAL", SqlDbType.VarChar).Value = kidView.INFANTE_LOCALIDAD_CODIGOPOSTAL;
                cmd.Parameters.Add("@INFANTE_SALA_ID", SqlDbType.Int).Value = kidView.INFANTE_SALA_ID;
                if(kidView.INFANTE_FECHAEGRESO != null)
                {
                    cmd.Parameters.Add("@INFANTE_FECHAEGRESO", SqlDbType.Date).Value = kidView.INFANTE_FECHAEGRESO;
                }
                con.Open();
                int i = cmd.ExecuteNonQuery();
                con.Close();
                string tmp = cmd.CommandText.ToString();
                foreach (SqlParameter p in cmd.Parameters)
                    tmp = tmp.Replace(p.ParameterName.ToString(), "'" + p.Value.ToString() + "'");

                if (i >= 1)
                {
                    auditservice.AddAudit(new AuditModel()
                    {
                        AUDITORIA_MODULO = "KID",
                        AUDITORIA_ACCION = "UPDATE",
                        AUDITORIA_ESTADO = "OK",
                        AUDITORIA_MENSAJE = "El infante se actualizo correctamente",
                        AUDITORIA_NOMBRECONTROLADOR = "KidService.UpdateKid",
                        AUDITORIA_RASTRO = tmp
                    });

                    return true;
                }
                else
                {
                    auditservice.AddAudit(new AuditModel()
                    {
                        AUDITORIA_MODULO = "KID",
                        AUDITORIA_ACCION = "UPDATE",
                        AUDITORIA_ESTADO = "ERROR",
                        AUDITORIA_MENSAJE = "Hubo un error al actualizar el infante",
                        AUDITORIA_NOMBRECONTROLADOR = "KidService.UpdateKid",
                        AUDITORIA_RASTRO = tmp
                    });
                    return false;
                }
            }
            catch (Exception ex)
            {
                auditservice.AddAudit(new AuditModel()
                {
                    AUDITORIA_MODULO = "KID",
                    AUDITORIA_ACCION = "UPDATE",
                    AUDITORIA_ESTADO = "ERROR",
                    AUDITORIA_MENSAJE = "Hubo un error al actualizar el infante",
                    AUDITORIA_NOMBRECONTROLADOR = "KidService.UpdateKid",
                    AUDITORIA_RASTRO = ex.Message
                });
                return false;
            }
        }

        public bool SetStatusInactive(KidModel kidView)
        {
            try
            {
                string sql = "UPDATE INFANTES SET infante_estado_id = @infante_estado_id " +
                                             " where infante_id = @infante_id";

                connection();
                SqlCommand cmd = new SqlCommand(sql, con);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.Add("@infante_id", SqlDbType.Int).Value = kidView.INFANTE_ID;
                cmd.Parameters.Add("@infante_estado_id", SqlDbType.VarChar).Value = "INAC_INF";
                con.Open();
                int i = cmd.ExecuteNonQuery();
                con.Close();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public bool ChangeState(int id)
        {
            try
            {
                string sql = "UPDATE INFANTES SET INFANTE_ESTADO_ID = @INFANTE_ESTADO_ID where INFANTE_ID = @INFANTE_ID";
                connection();
                SqlCommand cmd = new SqlCommand(sql, con);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.Add("@INFANTE_ID", SqlDbType.Int).Value = id;
                cmd.Parameters.Add("@INFANTE_ESTADO_ID", SqlDbType.VarChar).Value = "ACT_INF";
                con.Open();
                int i = cmd.ExecuteNonQuery();
                con.Close();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public List<KidModel> GetKidsNationality()
        {
            connection();
            List<KidModel> kidlist = new List<KidModel>();

            SqlCommand cmd = new SqlCommand("SELECT A.INFANTE_NACIONALIDAD_ID, B.TIPONACIONALIDAD_DESCRIPCION FROM INFANTES AS A INNER JOIN TIPONACIONALIDADES AS B ON A.INFANTE_NACIONALIDAD_ID = B.TIPONACIONALIDAD_ID", con);
            cmd.CommandType = CommandType.Text;
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            sd.Fill(dt);
            con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                kidlist.Add(
                    new KidModel
                    {
                        INFANTE_NACIONALIDAD_ID = Convert.ToInt32(dr["INFANTE_NACIONALIDAD_ID"]),
                        NACIONALIDAD = Convert.ToString(dr["TIPONACIONALIDAD_DESCRIPCION"]),
                    });
            }
            return kidlist;
        }
    }
  }
