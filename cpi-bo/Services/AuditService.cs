﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using cpi_bo.Models;


namespace cpi_bo.Services
{
    public class AuditService
    {
        private SqlConnection con;

        private void connection()
        {
            string constring = ConfigurationManager.ConnectionStrings["db_conn"].ToString();
            con = new SqlConnection(constring);
        }
        private DataTable sqlQuery(string sQuery)
        {
            connection();
            SqlCommand cmd = new SqlCommand(sQuery, con);
            cmd.CommandType = CommandType.Text;
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            sd.Fill(dt);
            con.Close();

            return dt;
        }

        public List<AuditModel> GetAudit()
        {
            List<AuditModel> AuditList = new List<AuditModel>();
            DataTable dtAudit = sqlQuery("SELECT * FROM AUDITORIAS ORDER BY AUDITORIA_ID ASC;");

            foreach (DataRow dr in dtAudit.Rows)
            {
                AuditList.Add(
                    new AuditModel
                    {
                        AUDITORIA_ID = Convert.ToInt32(dr["AUDITORIA_ID"]),
                        AUDITORIA_MODULO = Convert.ToString(dr["AUDITORIA_MODULO"]),
                        AUDITORIA_ACCION = Convert.ToString(dr["AUDITORIA_ACCION"]),
                        AUDITORIA_ESTADO = Convert.ToString(dr["AUDITORIA_ESTADO"]),
                        AUDITORIA_MENSAJE = Convert.ToString(dr["AUDITORIA_MENSAJE"]),
                        AUDITORIA_RASTRO = Convert.ToString(dr["AUDITORIA_RASTRO"]),
                        AUDITORIA_NOMBRECONTROLADOR = Convert.ToString(dr["AUDITORIA_NOMBRECONTROLADOR"]),
                        AUDITORIA_USUARIO_NICK = Convert.ToString(dr["AUDITORIA_USUARIO_NICK"]),
                        AUDITORIA_FECHAHORA = Convert.ToDateTime(dr["AUDITORIA_FECHAHORA"]),
                    });
            }
            return AuditList;
        }

        /**************** ADD NEW Audit *********************/

        public bool AddAudit(AuditModel AuditView)
        {
            try
            {
                connection();
                SqlCommand cmd = new SqlCommand("INSERT INTO AUDITORIAS (auditoria_modulo, auditoria_accion, auditoria_estado, auditoria_mensaje, auditoria_rastro, auditoria_nombrecontrolador, auditoria_usuario_nick, auditoria_fechahora) VALUES (@auditoria_modulo, @auditoria_accion, @auditoria_estado, @auditoria_mensaje, @auditoria_rastro, @auditoria_nombrecontrolador, @auditoria_usuario_nick, @auditoria_fechahora)", con);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.Add("@auditoria_modulo", SqlDbType.VarChar).Value = AuditView.AUDITORIA_MODULO.ToUpper();
                cmd.Parameters.Add("@auditoria_accion", SqlDbType.VarChar).Value = AuditView.AUDITORIA_ACCION.ToUpper();
                cmd.Parameters.Add("@auditoria_estado", SqlDbType.VarChar).Value = AuditView.AUDITORIA_ESTADO.ToUpper();
                cmd.Parameters.Add("@auditoria_mensaje", SqlDbType.VarChar).Value = AuditView.AUDITORIA_MENSAJE.ToUpper();
                cmd.Parameters.Add("@auditoria_rastro", SqlDbType.VarChar).Value = AuditView.AUDITORIA_RASTRO.ToUpper();
                cmd.Parameters.Add("@auditoria_nombrecontrolador", SqlDbType.VarChar).Value = AuditView.AUDITORIA_NOMBRECONTROLADOR.ToUpper();
                cmd.Parameters.Add("@auditoria_usuario_nick", SqlDbType.VarChar).Value = HttpContext.Current.User.Identity.Name == string.Empty ? AuditView.AUDITORIA_USUARIO_NICK : HttpContext.Current.User.Identity.Name;
                cmd.Parameters.Add("@auditoria_fechahora", SqlDbType.DateTime).Value = DateTime.Now;

                con.Open();
                int i = cmd.ExecuteNonQuery();
                con.Close();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }
        }
    }