﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using cpi_bo.Models;

namespace cpi_bo.Services
{
    public class TypeTableService
    {
        private SqlConnection con;

        private void connection()
        {
            string constring = ConfigurationManager.ConnectionStrings["db_conn"].ToString();
            con = new SqlConnection(constring);
        }

        private DataTable sqlQuery(string sQuery)
        {
            connection();
            SqlCommand cmd = new SqlCommand(sQuery, con);
            cmd.CommandType = CommandType.Text;
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            sd.Fill(dt);
            con.Close();

            return dt;
        }


        public List<TypeTableModel> GetTypes(string table, string id)
        {
            List<TypeTableModel> typeTables = new List<TypeTableModel>();

            DataTable dtSocialWork = sqlQuery("SELECT * FROM " + table + " ORDER BY " + id + " ASC;");

            foreach (DataRow dr in dtSocialWork.Rows)
            {
                typeTables.Add(
                    new TypeTableModel
                    {
                        ID = Convert.ToInt16(dr[0]),
                        DESCRIPCION = Convert.ToString(dr[1])
                    });
            }
            return typeTables;
        }
    }
}