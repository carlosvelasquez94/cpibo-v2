﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using cpi_bo.Models;

namespace cpi_bo.Services
{
    public class ProfileService
    {
        private SqlConnection con;
        AuditModel Audit = new AuditModel();
        AuditService auditservice = new AuditService();
        // LogOperationDBHandle DBlog = new LogOperationDBHandle();

        private void connection()
        {
            string constring = ConfigurationManager.ConnectionStrings["db_conn"].ToString();
            con = new SqlConnection(constring);
        }
        private DataTable sqlQuery(string sQuery)
        {
            connection();
            SqlCommand cmd = new SqlCommand(sQuery, con);
            cmd.CommandType = CommandType.Text;
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            sd.Fill(dt);
            con.Close();

            return dt;
        }

        public List<ProfileModel> GetProfile()
        {
            List<ProfileModel> ProfileList = new List<ProfileModel>();
            DataTable dtPerfile = sqlQuery("SELECT * FROM PERFILES ORDER BY perfil_nombre ASC;");

            foreach (DataRow dr in dtPerfile.Rows)
            {
                ProfileList.Add(
                    new ProfileModel
                    {
                        perfil_id = Convert.ToInt16(dr["perfil_id"]),
                        perfil_nombre = Convert.ToString(dr["perfil_nombre"]),
                        perfil_descripcion = Convert.ToString(dr["perfil_descripcion"])
                    });
            }
            return ProfileList;
        }

        /**************** ADD NEW PROFILE *********************/

        public bool AddProfile(ProfileModel ProfileView)
        {
            Audit.AUDITORIA_MODULO = "PROFILE";
            Audit.AUDITORIA_ACCION = "CREATE";
            Audit.AUDITORIA_NOMBRECONTROLADOR = "ProfileService.AddProfile";
            try
            {
                connection();
                SqlCommand cmd = new SqlCommand("INSERT INTO PERFILES (perfil_nombre, perfil_descripcion) VALUES (@perfil_nombre, @perfil_descripcion)", con);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.Add("@perfil_nombre", SqlDbType.VarChar).Value = ProfileView.perfil_nombre.ToUpper();
                cmd.Parameters.Add("@perfil_descripcion", SqlDbType.VarChar).Value = ProfileView.perfil_descripcion.ToUpper();
                con.Open();
                int i = cmd.ExecuteNonQuery();
                con.Close();
                // query que se ejecuta
                string tmp = cmd.CommandText.ToString();
                foreach (SqlParameter p in cmd.Parameters)
                {
                    tmp = tmp.Replace(p.ParameterName.ToString(), "'" + p.Value.ToString() + "'");
                }

                Audit.AUDITORIA_RASTRO = tmp;

                if (i >= 1)
                {
                    Audit.AUDITORIA_ESTADO = "OK";
                    Audit.AUDITORIA_MENSAJE = "El perfil se inserto OK.";
                    auditservice.AddAudit(Audit);
                    return true;
                }
                else
                {
                    Audit.AUDITORIA_ESTADO = "ERROR";
                    Audit.AUDITORIA_MENSAJE = "El perfil no se pudo insertar.";
                    auditservice.AddAudit(Audit);
                    return false;
                }
            }
            catch (Exception ex)
            {
                Audit.AUDITORIA_RASTRO = ex.Message;
                Audit.AUDITORIA_ESTADO = "ERROR";
                Audit.AUDITORIA_MENSAJE = "El perfil no se pudo insertar.";
                auditservice.AddAudit(Audit);
                return false;
            }
        }

        /**************** UPDATE NEW ROL *********************/
        public bool UpdateProfile(ProfileModel ProfileView)
        {
            try
            {
                connection();
                SqlCommand cmd = new SqlCommand("UPDATE PERFILES SET perfil_nombre= @perfil_nombre, perfil_descripcion = @perfil_descripcion WHERE perfil_id = @perfil_id ", con);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.Add("@perfil_nombre", SqlDbType.VarChar).Value = ProfileView.perfil_nombre.ToUpper();
                cmd.Parameters.Add("@perfil_descripcion", SqlDbType.VarChar).Value = ProfileView.perfil_descripcion.ToUpper();
                cmd.Parameters.Add("@perfil_id", SqlDbType.Int).Value = ProfileView.perfil_id;

                con.Open();
                int i = cmd.ExecuteNonQuery();
                con.Close();
                 string tmp = cmd.CommandText.ToString();
                foreach (SqlParameter p in cmd.Parameters)
                    tmp = tmp.Replace(p.ParameterName.ToString(), "'" + p.Value.ToString() + "'");

                if (i >= 1)
                {
                    auditservice.AddAudit(new AuditModel()
                    { 
                        AUDITORIA_MODULO = "PROFILE",
                        AUDITORIA_ACCION = "UPDATE",
                        AUDITORIA_ESTADO = "OK",
                        AUDITORIA_MENSAJE = "El perfil se actualizo correctamente",
                        AUDITORIA_NOMBRECONTROLADOR = "ProfileService.UpdateProfile",
                        AUDITORIA_RASTRO = tmp
                    });

                    return true;
                }
                else
                {
                    auditservice.AddAudit(new AuditModel()
                    {
                        AUDITORIA_MODULO = "PROFILE",
                        AUDITORIA_ACCION = "UPDATE",
                        AUDITORIA_ESTADO = "ERROR",
                        AUDITORIA_MENSAJE = "Hubo un error al actualizar el perfil",
                        AUDITORIA_NOMBRECONTROLADOR = "ProfileService.UpdateProfile",
                        AUDITORIA_RASTRO = tmp
                    });
                    return false;
                }
            }
            catch (Exception ex)
            {
                auditservice.AddAudit(new AuditModel()
                {
                    AUDITORIA_MODULO = "PROFILE",
                    AUDITORIA_ACCION = "UPDATE",
                    AUDITORIA_ESTADO = "ERROR",
                    AUDITORIA_MENSAJE = "Hubo un error al actualizar el perfil",
                    AUDITORIA_NOMBRECONTROLADOR = "ProfileService.UpdateProfile",
                    AUDITORIA_RASTRO = ex.Message
                });
                return false;
            }
        }


        /**************** DELETE NEW ROL *********************/
        public bool DeleteProfile(ProfileModel ProfileView)
        {
            try
            {
                connection();
                SqlCommand cmd = new SqlCommand("DELETE PERFILES WHERE perfil_id=" + ProfileView.perfil_id, con);//pregu
                cmd.CommandType = CommandType.Text;

                con.Open();
                int i = cmd.ExecuteNonQuery();
                con.Close();
                string tmp = cmd.CommandText.ToString();
                foreach (SqlParameter p in cmd.Parameters)
                    tmp = tmp.Replace(p.ParameterName.ToString(), "'" + p.Value.ToString() + "'");

                if (i >= 1)
                {
                    auditservice.AddAudit(new AuditModel()
                    {
                        AUDITORIA_MODULO = "PROFILE",
                        AUDITORIA_ACCION = "DELETE",
                        AUDITORIA_ESTADO = "OK",
                        AUDITORIA_MENSAJE = "El perfil se elimino correctamente",
                        AUDITORIA_NOMBRECONTROLADOR = "ProfileService.DeleteProfile",
                        AUDITORIA_RASTRO = tmp
                    });

                    return true;
                }
                else
                {
                    auditservice.AddAudit(new AuditModel()
                    {
                        AUDITORIA_MODULO = "PROFILE",
                        AUDITORIA_ACCION = "DELETE",
                        AUDITORIA_ESTADO = "ERROR",
                        AUDITORIA_MENSAJE = "Hubo un error al elimino el perfil",
                        AUDITORIA_NOMBRECONTROLADOR = "ProfileService.DeleteProfile",
                        AUDITORIA_RASTRO = tmp
                    });
                    return false;
                }
            }
            catch (Exception ex)
            {
                auditservice.AddAudit(new AuditModel()
                {
                    AUDITORIA_MODULO = "FAMILY",
                    AUDITORIA_ACCION = "DELETE",
                    AUDITORIA_ESTADO = "ERROR",
                    AUDITORIA_MENSAJE = "Hubo un error al elimino el perfil",
                    AUDITORIA_NOMBRECONTROLADOR = "ProfileService.DeleteProfile",
                    AUDITORIA_RASTRO = ex.Message
                });
                return false;
            }
        }
    }
}