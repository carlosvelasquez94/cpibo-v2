﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Linq;
using System.Web;

namespace cpi_bo.Services
{
    public class RoomService
    {
        private SqlConnection con;
        private void connection()
        {
            string constring = ConfigurationManager.ConnectionStrings["db_conn"].ToString();
            con = new SqlConnection(constring);
        }

        public int getRooms (string salaid)
        {
            try
            {
                connection();
                con.Open();
                SqlCommand cmd = new SqlCommand("select count(*)*100/30 as total from infantes where infante_sala_id = " + salaid, con);
                cmd.CommandType = CommandType.Text;
                var result = cmd.ExecuteScalar();
                con.Close();
                return Convert.ToInt16(result);

            }
            catch (Exception e)
            {
                return 0;
            }
        }
    }
}