﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using cpi_bo.Models;

namespace cpi_bo.Services
{
    public class ActivityService 
    {
        private SqlConnection con;
        AuditModel Audit = new AuditModel();
        AuditService auditservice = new AuditService();
        private void connection()
        {
            string constring = ConfigurationManager.ConnectionStrings["db_conn"].ToString();
            con = new SqlConnection(constring);
        }
        private DataTable sqlQuery(string sQuery)
        {
            connection();
            SqlCommand cmd = new SqlCommand(sQuery, con);
            cmd.CommandType = CommandType.Text;
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            sd.Fill(dt);
            con.Close();

            return dt;
        }

        public List<ActivityModel> GetActivity()
        {
            List<ActivityModel> ActivityList = new List<ActivityModel>();
            DataTable dtActivity = sqlQuery("SELECT * FROM EMPLEADOS_ACTIVIDADES AS A INNER JOIN EMPLEADOS AS B ON A.EMPLEADOACTIVIDAD_EMPLEADO_ID = B.EMPLEADO_ID INNER JOIN ACTIVIDADES AS C ON A.EMPLEADOACTIVIDAD_ACTIVIDAD_ID = C.ACTIVIDAD_ID ;");

            foreach (DataRow dr in dtActivity.Rows)
            {
                ActivityList.Add(
                    new ActivityModel
                    {
                        EMPLEADOACTIVIDAD_ID = Convert.ToInt32(dr["EMPLEADOACTIVIDAD_ID"]),
                        EMPLEADOACTIVIDAD_EMPLEADO_DESCRIPCION = Convert.ToString(dr["EMPLEADO_NOMBRE"]),
                        EMPLEADOACTIVIDAD_ACTIVIDAD_ID = Convert.ToInt32(dr["EMPLEADOACTIVIDAD_ACTIVIDAD_ID"]),
                        EMPLEADOACTIVIDAD_EMPLEADO_ID = Convert.ToInt32(dr["EMPLEADOACTIVIDAD_EMPLEADO_ID"]),
                        EMPLEADOACTIVIDAD_ACTIVIDAD_DESCRIPCION = Convert.ToString(dr["actividad_descripcion"]),
                        EMPLEADOACTIVIDAD_FECHA = Convert.ToDateTime(dr["EMPLEADOACTIVIDAD_FECHA"]),
                        EMPLEADOACTIVIDAD_HORA = Convert.ToString(dr["EMPLEADOACTIVIDAD_HORA"]),
                        EMPLEADOACTIVIDAD_ANOTACION = Convert.ToString(dr["EMPLEADOACTIVIDAD_ANOTACION"]),
                    });
            }
            return ActivityList;
        }

        /**************** ADD NEW Activity *********************/

        public bool AddActivity(ActivityModel ActivityView)
        {
            Audit.AUDITORIA_MODULO = "ACTIVITY";
            Audit.AUDITORIA_ACCION = "CREATE";
            Audit.AUDITORIA_NOMBRECONTROLADOR = "ActivityService.AddActivity";
            try
            {
                connection();
                SqlCommand cmd = new SqlCommand("INSERT INTO EMPLEADOS_ACTIVIDADES ( empleadoactividad_empleado_id, empleadoactividad_actividad_id, empleadoactividad_fecha, empleadoactividad_hora, empleadoactividad_anotacion) VALUES ( @empleadoactividad_empleado_id, @empleadoactividad_actividad_id, @empleadoactividad_fecha, @empleadoactividad_hora, @empleadoactividad_anotacion)", con);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.Add("@empleadoactividad_empleado_id", SqlDbType.Int).Value = ActivityView.EMPLEADOACTIVIDAD_EMPLEADO_ID;
                cmd.Parameters.Add("@empleadoactividad_actividad_id", SqlDbType.Int).Value = ActivityView.EMPLEADOACTIVIDAD_ACTIVIDAD_ID;
                cmd.Parameters.Add("@empleadoactividad_fecha", SqlDbType.DateTime).Value = ActivityView.EMPLEADOACTIVIDAD_FECHA;
                cmd.Parameters.Add("@empleadoactividad_hora", SqlDbType.DateTime).Value = ActivityView.EMPLEADOACTIVIDAD_HORA;
                cmd.Parameters.Add("@empleadoactividad_anotacion", SqlDbType.VarChar).Value = ActivityView.EMPLEADOACTIVIDAD_ANOTACION.ToUpper();
                con.Open();
                int i = cmd.ExecuteNonQuery();
                con.Close();

                // query que se ejecuta
                string tmp = cmd.CommandText.ToString();
                foreach (SqlParameter p in cmd.Parameters)
                {
                    tmp = tmp.Replace(p.ParameterName.ToString(), "'" + p.Value.ToString() + "'");
                }

                Audit.AUDITORIA_RASTRO = tmp;

                if (i >= 1)
                {
                    Audit.AUDITORIA_ESTADO = "OK";
                    Audit.AUDITORIA_MENSAJE = "La actividad se inserto OK.";
                    auditservice.AddAudit(Audit);
                    return true;
                }
                else
                {
                    Audit.AUDITORIA_ESTADO = "ERROR";
                    Audit.AUDITORIA_MENSAJE = "La actividad no se pudo insertar.";
                    auditservice.AddAudit(Audit);
                    return false;
                }
            }
            catch (Exception ex)
            {
                Audit.AUDITORIA_RASTRO = ex.Message;
                Audit.AUDITORIA_ESTADO = "ERROR";
                Audit.AUDITORIA_MENSAJE = "La actividad no se pudo insertar.";
                auditservice.AddAudit(Audit);
                return false;
            }
        }

        /**************** UPDATE NEW Activity *********************/
        public bool UpdateActivity(ActivityModel ActivityView)
        {
            try
            {
                connection();
                SqlCommand cmd = new SqlCommand("UPDATE EMPLEADOS_ACTIVIDADES SET empleadoactividad_empleado_id= @empleadoactividad_empleado_id, empleadoactividad_actividad_id = @empleadoactividad_actividad_id,empleadoactividad_fecha = @empleadoactividad_fecha,empleadoactividad_hora= @empleadoactividad_hora,empleadoactividad_anotacion= @empleadoactividad_anotacion WHERE empleadoactividad_id = @empleadoactividad_id ", con);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.Add("@empleadoactividad_empleado_id", SqlDbType.Int).Value = ActivityView.EMPLEADOACTIVIDAD_EMPLEADO_ID;
                cmd.Parameters.Add("@empleadoactividad_actividad_id", SqlDbType.Int).Value = ActivityView.EMPLEADOACTIVIDAD_ACTIVIDAD_ID;
                cmd.Parameters.Add("@empleadoactividad_fecha", SqlDbType.DateTime).Value = ActivityView.EMPLEADOACTIVIDAD_FECHA;
                cmd.Parameters.Add("@empleadoactividad_hora", SqlDbType.VarChar).Value = ActivityView.EMPLEADOACTIVIDAD_HORA.ToUpper();
                cmd.Parameters.Add("@empleadoactividad_anotacion", SqlDbType.VarChar).Value = ActivityView.EMPLEADOACTIVIDAD_ANOTACION.ToUpper();
                cmd.Parameters.Add("@empleadoactividad_id", SqlDbType.Int).Value = ActivityView.EMPLEADOACTIVIDAD_ID;

                con.Open();
                int i = cmd.ExecuteNonQuery();
                con.Close();
                string tmp = cmd.CommandText.ToString();
                foreach (SqlParameter p in cmd.Parameters)
                    tmp = tmp.Replace(p.ParameterName.ToString(), "'" + p.Value.ToString() + "'");

                if (i >= 1)
                {
                    auditservice.AddAudit(new AuditModel()
                    {
                        AUDITORIA_MODULO = "ACTIVITY",
                        AUDITORIA_ACCION = "UPDATE",
                        AUDITORIA_ESTADO = "OK",
                        AUDITORIA_MENSAJE = "La actividad se actualizo correctamente",
                        AUDITORIA_NOMBRECONTROLADOR = "ActivityService.UpdateActivity",
                        AUDITORIA_RASTRO = tmp
                    });

                    return true;
                }
                else
                {
                    auditservice.AddAudit(new AuditModel()
                    {
                        AUDITORIA_MODULO = "ACTIVITY",
                        AUDITORIA_ACCION = "UPDATE",
                        AUDITORIA_ESTADO = "ERROR",
                        AUDITORIA_MENSAJE = "Hubo un error al actualizar la actividad",
                        AUDITORIA_NOMBRECONTROLADOR = "ActivityService.UpdateActivity",
                        AUDITORIA_RASTRO = tmp
                    });
                    return false;
                }
            }
            catch (Exception ex)
            {
                auditservice.AddAudit(new AuditModel()
                {
                    AUDITORIA_MODULO = "ACTIVITY",
                    AUDITORIA_ACCION = "UPDATE",
                    AUDITORIA_ESTADO = "ERROR",
                    AUDITORIA_MENSAJE = "Hubo un error al actualizar la actividad",
                    AUDITORIA_NOMBRECONTROLADOR = "ActivityService.UpdateActivity",
                    AUDITORIA_RASTRO = ex.Message
                });
                return false;
            }
        }
    }
}