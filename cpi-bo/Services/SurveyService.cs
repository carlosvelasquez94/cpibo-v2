﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Configuration;
using System.Linq;
using System.Web;
using cpi_bo.Models;

namespace cpi_bo.Services
{
    public class SurveyService
    {
        private SqlConnection con;

        private void connection()
        {
            string constring = ConfigurationManager.ConnectionStrings["db_conn"].ToString();
            con = new SqlConnection(constring);
        }

        public List<FamilyOrganization> GetFamilyOrganization()
        {
            try { 
            connection();
            List<FamilyOrganization> FamilyOrganizationlist = new List<FamilyOrganization>();

            SqlCommand cmd = new SqlCommand("select * from OrganizacionFamiliar ; ", con);
            cmd.CommandType = CommandType.Text;
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            sd.Fill(dt);
            con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                FamilyOrganizationlist.Add(
                    new FamilyOrganization
                    {
                        ID = Convert.ToInt16(dr["ID"]),
                        FAMILIARACARGO = Convert.ToInt16(dr["FAMILIARACARGO"]),
                        FAMILIARPRIVADOLIBERTAD = Convert.ToInt16(dr["FAMILIARPRIVADOLIBERTAD"]),
                        CANTIDADHERMANOS = Convert.ToInt16(dr["CANTIDADHERMANOS"]),
                        MADREADOLECENTE = Convert.ToInt16(dr["MADREADOLECENTE"]),
                        FAMILIAREMBARAZADA = Convert.ToInt16(dr["FAMILIAREMBARAZADA"]),
                        IDINFANTE = Convert.ToInt16(dr["IDINFANTE"])
                    });
            }
            return FamilyOrganizationlist;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public List<Health> GetHealth()
        {
            try
            {
                connection();
                List<Health> Healthlist = new List<Health>();

                SqlCommand cmd = new SqlCommand("select * from salud ; ", con);
                cmd.CommandType = CommandType.Text;
                SqlDataAdapter sd = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();

                con.Open();
                sd.Fill(dt);
                con.Close();

                foreach (DataRow dr in dt.Rows)
                {
                    Healthlist.Add(
                        new Health
                        {
                            ID = Convert.ToInt16(dr["ID"]),
                            CONDICIONESDESALUDFAMILIAR = Convert.ToInt16(dr["CONDICIONESDESALUDFAMILIAR"]),
                            COBERTURADESALUDFAMILIAR = Convert.ToInt16(dr["COBERTURADESALUDFAMILIAR"]),
                            CONTROLDESALUDDELINFANTE = Convert.ToInt16(dr["CONTROLDESALUDDELNIÑO"]),
                            VACUNACIONDELINFANTE = Convert.ToInt16(dr["VACUNACIONDELNIÑO"]),
                            IDINFANTE = Convert.ToInt16(dr["IDINFANTE"])
                        });
                }
                return Healthlist;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public List<Education> GetEducation()
        {
            try
            {
                connection();
                List<Education> Educationlist = new List<Education>();

                SqlCommand cmd = new SqlCommand("select * from EDUCACION ; ", con);
                cmd.CommandType = CommandType.Text;
                SqlDataAdapter sd = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();

                con.Open();
                sd.Fill(dt);
                con.Close();

                foreach (DataRow dr in dt.Rows)
                {
                    Educationlist.Add(
                        new Education
                        {
                            ID = Convert.ToInt16(dr["ID"]),
                            ESCOLARIZACIONDELOSINFANTES = Convert.ToInt16(dr["ESCOLARIZACIONDELOSINFANTES"]),
                            MADREADOLESCENTE = Convert.ToInt16(dr["MADREADOLESCENTE"]),
                            IDINFANTE = Convert.ToInt16(dr["IDINFANTE"])
                        });
                }
                return Educationlist;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public List<WorkSituation> GetWorkSituation()
        {
            try
            {
                connection();
                List<WorkSituation> WorkSituationlist = new List<WorkSituation>();

                SqlCommand cmd = new SqlCommand("select * from SITUACION_LABORAL ; ", con);
                cmd.CommandType = CommandType.Text;
                SqlDataAdapter sd = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();

                con.Open();
                sd.Fill(dt);
                con.Close();

                foreach (DataRow dr in dt.Rows)
                {
                    WorkSituationlist.Add(
                        new WorkSituation
                        {
                            ID = Convert.ToInt16(dr["ID"]),
                            TRABAJOENLAFAMILIA = Convert.ToInt16(dr["TRABAJOENLAFAMILIA"]),
                            TRABAJOMENORDEEDAD = Convert.ToInt16(dr["TRABAJOMENORDEEDAD"]),
                            IDINFANTE = Convert.ToInt16(dr["IDINFANTE"])
                        });
                }
                return WorkSituationlist;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public List<FamilyEconomy> GetFamilyEconomy()
        {
            try
            {
                connection();
                List<FamilyEconomy> FamilyEconomylist = new List<FamilyEconomy>();

                SqlCommand cmd = new SqlCommand("select * from econimiaFamiliar ; ", con);
                cmd.CommandType = CommandType.Text;
                SqlDataAdapter sd = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();

                con.Open();
                sd.Fill(dt);
                con.Close();

                foreach (DataRow dr in dt.Rows)
                {
                    FamilyEconomylist.Add(
                        new FamilyEconomy
                        {
                            ID = Convert.ToInt16(dr["ID"]),
                            UBICACIONINGRESOFAMILIAR = Convert.ToInt16(dr["UBICACIONINGRESOFAMILIAR"]),
                            BENEFICIOSOCIAL = Convert.ToInt16(dr["BENEFICIOSOCIAL"]),
                            IDINFANTE = Convert.ToInt16(dr["IDINFANTE"])
                        });
                }
                return FamilyEconomylist;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public List<Dwelling> GetDwelling()
        {
            try
            {
                connection();
                List<Dwelling> Dwellinglist = new List<Dwelling>();

                SqlCommand cmd = new SqlCommand("select * from vivienda ; ", con);
                cmd.CommandType = CommandType.Text;
                SqlDataAdapter sd = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();

                con.Open();
                sd.Fill(dt);
                con.Close();

                foreach (DataRow dr in dt.Rows)
                {
                    Dwellinglist.Add(
                        new Dwelling
                        {
                            ID = Convert.ToInt16(dr["ID"]),
                            TIPOVIVIENDA = Convert.ToInt16(dr["TIPOVIVIENDA"]),
                            TIPOPROPIEDAD = Convert.ToInt16(dr["TIPOPROPIEDAD"]),
                            CANTIDADPERSONASPORHABITACION = Convert.ToInt16(dr["CANTIDADPERSONASPORHABITACION"]),
                            SERVICIOSVIVIENDA = Convert.ToInt16(dr["SERVICIOSVIVIENDA"]),
                            IDINFANTE = Convert.ToInt16(dr["IDINFANTE"])
                        });
                }
                return Dwellinglist;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public List<SocialProblem> GetSocialProblem()
        {
            try
            {
                connection();
                List<SocialProblem> SocialProblemlist = new List<SocialProblem>();

                SqlCommand cmd = new SqlCommand("select * from problemasSociales ; ", con);
                cmd.CommandType = CommandType.Text;
                SqlDataAdapter sd = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();

                con.Open();
                sd.Fill(dt);
                con.Close();

                foreach (DataRow dr in dt.Rows)
                {
                    SocialProblemlist.Add(
                        new SocialProblem
                        {
                            ID = Convert.ToInt16(dr["ID"]),
                            REQUIEREINTERVENCION = Convert.ToInt16(dr["REQUIEREINTERVENCION"]),
                            IDINFANTE = Convert.ToInt16(dr["IDINFANTE"])
                        });
                }
                return SocialProblemlist;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public bool addFamiliOrganization(FamilyOrganization familyOrganization)
        {
            try
            {
                string sql = "";
                var result = validateSurvey(familyOrganization.IDINFANTE.ToString(), "idInfante", "organizacionFamiliar");
                if(result)
                {
                    sql = "insert into OrganizacionFamiliar values (@FAMILIARACARGO, @FAMILIARPRIVADOLIBERTAD, @CANTIDADHERMANOS, @MADREADOLECENTE, @FAMILIAREMBARAZADA, @IDINFANTE)";
                } else
                {
                    sql = "update OrganizacionFamiliar set FAMILIARACARGO=@FAMILIARACARGO, FAMILIARPRIVADOLIBERTAD=@FAMILIARPRIVADOLIBERTAD, CANTIDADHERMANOS=@CANTIDADHERMANOS, MADREADOLECENTE=@MADREADOLECENTE, FAMILIAREMBARAZADA=@FAMILIAREMBARAZADA where IDINFANTE=@IDINFANTE";
                }
                connection();
                SqlCommand cmd = new SqlCommand(sql, con);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.Add("@ID", SqlDbType.Int).Value = familyOrganization.ID;
                cmd.Parameters.Add("@FAMILIARACARGO", SqlDbType.Int).Value = familyOrganization.FAMILIARACARGO;
                cmd.Parameters.Add("@FAMILIARPRIVADOLIBERTAD", SqlDbType.Int).Value = familyOrganization.FAMILIARPRIVADOLIBERTAD;
                cmd.Parameters.Add("@CANTIDADHERMANOS", SqlDbType.Int).Value = familyOrganization.CANTIDADHERMANOS;
                cmd.Parameters.Add("@MADREADOLECENTE", SqlDbType.Int).Value = familyOrganization.MADREADOLECENTE;
                cmd.Parameters.Add("@FAMILIAREMBARAZADA", SqlDbType.Int).Value = familyOrganization.FAMILIAREMBARAZADA;
                cmd.Parameters.Add("@IDINFANTE", SqlDbType.Int).Value = familyOrganization.IDINFANTE;
                con.Open();
                int i = cmd.ExecuteNonQuery();
                con.Close();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public bool addHealth(Health Health)
        {
            try
            {
                string sql = "";
                var result = validateSurvey(Health.IDINFANTE.ToString(), "idInfante", "salud");
                if (result)
                {
                    sql = "insert into Salud values (@CONDICIONESDESALUDFAMILIAR, @COBERTURADESALUDFAMILIAR, @CONTROLDESALUDDELNIÑO, @VACUNACIONDELNIÑO, @IDINFANTE)";
                }
                else
                {
                    sql = "update Salud set CONDICIONESDESALUDFAMILIAR=@CONDICIONESDESALUDFAMILIAR, COBERTURADESALUDFAMILIAR=@COBERTURADESALUDFAMILIAR, CONTROLDESALUDDELNIÑO=@CONTROLDESALUDDELNIÑO, VACUNACIONDELNIÑO=@VACUNACIONDELNIÑO where IDINFANTE=@IDINFANTE";
                }
                connection();
                SqlCommand cmd = new SqlCommand(sql, con);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.Add("@ID", SqlDbType.Int).Value = Health.ID;
                cmd.Parameters.Add("@CONDICIONESDESALUDFAMILIAR", SqlDbType.Int).Value = Health.CONDICIONESDESALUDFAMILIAR;
                cmd.Parameters.Add("@COBERTURADESALUDFAMILIAR", SqlDbType.Int).Value = Health.COBERTURADESALUDFAMILIAR;
                cmd.Parameters.Add("@CONTROLDESALUDDELNIÑO", SqlDbType.Int).Value = Health.CONTROLDESALUDDELINFANTE;
                cmd.Parameters.Add("@VACUNACIONDELNIÑO", SqlDbType.Int).Value = Health.VACUNACIONDELINFANTE;
                cmd.Parameters.Add("@IDINFANTE", SqlDbType.Int).Value = Health.IDINFANTE;
                con.Open();
                int i = cmd.ExecuteNonQuery();
                con.Close();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public bool addEducation(Education Education)
        {
            try
            {
                string sql = "";
                var result = validateSurvey(Education.IDINFANTE.ToString(), "idInfante", "EDUCACION");
                if (result)
                {
                    sql = "insert into EDUCACION values (@ESCOLARIZACIONDELOSINFANTES, @MADREADOLESCENTE, @IDINFANTE)";
                }
                else
                {
                    sql = "update EDUCACION set ESCOLARIZACIONDELOSINFANTES=@ESCOLARIZACIONDELOSINFANTES, MADREADOLESCENTE=@MADREADOLESCENTE where IDINFANTE=@IDINFANTE";
                }
                connection();
                SqlCommand cmd = new SqlCommand(sql, con);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.Add("@ID", SqlDbType.Int).Value = Education.ID;
                cmd.Parameters.Add("@ESCOLARIZACIONDELOSINFANTES", SqlDbType.Int).Value = Education.ESCOLARIZACIONDELOSINFANTES;
                cmd.Parameters.Add("@MADREADOLESCENTE", SqlDbType.Int).Value = Education.MADREADOLESCENTE;
                cmd.Parameters.Add("@IDINFANTE", SqlDbType.Int).Value = Education.IDINFANTE;
                con.Open();
                int i = cmd.ExecuteNonQuery();
                con.Close();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public bool addWorkSituation(WorkSituation WorkSituation)
        {
            try
            {
                string sql = "";
                var result = validateSurvey(WorkSituation.IDINFANTE.ToString(), "idInfante", "SITUACION_LABORAL");
                if (result)
                {
                    sql = "insert into SITUACION_LABORAL values (@TRABAJOENLAFAMILIA, @TRABAJOMENORDEEDAD, @IDINFANTE)";
                }
                else
                {
                    sql = "update SITUACION_LABORAL set TRABAJOENLAFAMILIA=@TRABAJOENLAFAMILIA, TRABAJOMENORDEEDAD=@TRABAJOMENORDEEDAD where IDINFANTE=@IDINFANTE";
                }
                connection();
                SqlCommand cmd = new SqlCommand(sql, con);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.Add("@ID", SqlDbType.Int).Value = WorkSituation.ID;
                cmd.Parameters.Add("@TRABAJOENLAFAMILIA", SqlDbType.Int).Value = WorkSituation.TRABAJOENLAFAMILIA;
                cmd.Parameters.Add("@TRABAJOMENORDEEDAD", SqlDbType.Int).Value = WorkSituation.TRABAJOMENORDEEDAD;
                cmd.Parameters.Add("@IDINFANTE", SqlDbType.Int).Value = WorkSituation.IDINFANTE;
                con.Open();
                int i = cmd.ExecuteNonQuery();
                con.Close();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public bool addFamilyEconomy(FamilyEconomy familyEconomy)
        {
            try
            {
                string sql = "";
                var result = validateSurvey(familyEconomy.IDINFANTE.ToString(), "idInfante", "econimiaFamiliar");
                if (result)
                {
                    sql = "insert into econimiaFamiliar values (@UBICACIONINGRESOFAMILIAR, @UBICACIONINGRESOFAMILIAR, @IDINFANTE)";
                }
                else
                {
                    sql = "update econimiaFamiliar set UBICACIONINGRESOFAMILIAR=@UBICACIONINGRESOFAMILIAR, BENEFICIOSOCIAL=@BENEFICIOSOCIAL where IDINFANTE=@IDINFANTE";
                }
                connection();
                SqlCommand cmd = new SqlCommand(sql, con);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.Add("@ID", SqlDbType.Int).Value = familyEconomy.ID;
                cmd.Parameters.Add("@UBICACIONINGRESOFAMILIAR", SqlDbType.Int).Value = familyEconomy.UBICACIONINGRESOFAMILIAR;
                cmd.Parameters.Add("@BENEFICIOSOCIAL", SqlDbType.Int).Value = familyEconomy.BENEFICIOSOCIAL;
                cmd.Parameters.Add("@IDINFANTE", SqlDbType.Int).Value = familyEconomy.IDINFANTE;
                con.Open();
                int i = cmd.ExecuteNonQuery();
                con.Close();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public bool addDwelling(Dwelling dwelling)
        {
            try
            {
                string sql = "";
                var result = validateSurvey(dwelling.IDINFANTE.ToString(), "idInfante", "vivienda");
                if (result)
                {
                    sql = "insert into vivienda values (@TIPOVIVIENDA, @TIPOPROPIEDAD, @CANTIDADPERSONASPORHABITACION, @SERVICIOSVIVIENDA, @IDINFANTE)";
                }
                else
                {
                    sql = "update vivienda set TIPOVIVIENDA=@TIPOVIVIENDA, TIPOPROPIEDAD=@TIPOPROPIEDAD, CANTIDADPERSONASPORHABITACION=@CANTIDADPERSONASPORHABITACION, SERVICIOSVIVIENDA=@SERVICIOSVIVIENDA where IDINFANTE=@IDINFANTE";
                }
                connection();
                SqlCommand cmd = new SqlCommand(sql, con);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.Add("@ID", SqlDbType.Int).Value = dwelling.ID;
                cmd.Parameters.Add("@TIPOVIVIENDA", SqlDbType.Int).Value = dwelling.TIPOVIVIENDA;
                cmd.Parameters.Add("@TIPOPROPIEDAD", SqlDbType.Int).Value = dwelling.TIPOPROPIEDAD;
                cmd.Parameters.Add("@CANTIDADPERSONASPORHABITACION", SqlDbType.Int).Value = dwelling.CANTIDADPERSONASPORHABITACION;
                cmd.Parameters.Add("@SERVICIOSVIVIENDA", SqlDbType.Int).Value = dwelling.SERVICIOSVIVIENDA;
                cmd.Parameters.Add("@IDINFANTE", SqlDbType.Int).Value = dwelling.IDINFANTE;
                con.Open();
                int i = cmd.ExecuteNonQuery();
                con.Close();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public bool addSocialProblem(SocialProblem socialProblem)
        {
            try
            {
                string sql = "";
                var result = validateSurvey(socialProblem.IDINFANTE.ToString(), "idInfante", "problemasSociales");
                if (result)
                {
                    sql = "insert into problemasSociales values (@REQUIEREINTERVENCION, @IDINFANTE)";
                }
                else
                {
                    sql = "update problemasSociales set REQUIEREINTERVENCION=@REQUIEREINTERVENCION where IDINFANTE=@IDINFANTE";
                }
                connection();
                SqlCommand cmd = new SqlCommand(sql, con);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.Add("@ID", SqlDbType.Int).Value = socialProblem.ID;
                cmd.Parameters.Add("@REQUIEREINTERVENCION", SqlDbType.Int).Value = socialProblem.REQUIEREINTERVENCION;
                cmd.Parameters.Add("@IDINFANTE", SqlDbType.Int).Value = socialProblem.IDINFANTE;
                con.Open();
                int i = cmd.ExecuteNonQuery();
                con.Close();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        private bool addSurvey(int idInf, int idOF)
        {
            try {
                string sql = "insert into encuestas values (@idInf, @idOF)";
                
                connection();
                SqlCommand cmd = new SqlCommand(sql, con);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.Add("@ID", SqlDbType.Int).Value = 1;
                cmd.Parameters.Add("@idInf", SqlDbType.Int).Value = idInf;
                cmd.Parameters.Add("@idOF", SqlDbType.Int).Value = idOF;
                con.Open();
                int i = cmd.ExecuteNonQuery();
                con.Close();
                return true;
            } catch(Exception e) { return false; }
        }

        public bool validateSurvey(string id, string campo, string tabla)
        {
            try
            {
                connection();
                SqlCommand cmd = new SqlCommand("SELECT * FROM "+ tabla +" WHERE " + campo + " = @ID;", con);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.Add("@ID", SqlDbType.Int).Value = id;
                SqlDataAdapter sd = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();

                con.Open();
                sd.Fill(dt);
                if (dt.Rows.Count < 1)
                    return true;
                return false;
            }
            catch (Exception e)
            {
                return false;
            }
            finally
            {
                con.Close();
            }
        }
   }
}