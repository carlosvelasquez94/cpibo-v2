﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using cpi_bo.Models;

namespace cpi_bo.Services
{
    public class EmployeesService
    {
        // GET: EmployeesService
        private SqlConnection con;
        AuditModel Audit = new AuditModel();
        AuditService auditservice = new AuditService();

        private void connection()
        {
            string constring = ConfigurationManager.ConnectionStrings["db_conn"].ToString();
            con = new SqlConnection(constring);
        }

        private DataTable sqlQuery(string sQuery)
        {
            connection();
            SqlCommand cmd = new SqlCommand(sQuery, con);
            cmd.CommandType = CommandType.Text;
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            sd.Fill(dt);
            con.Close();

            return dt;
        }

        public List<EmployeesModel> GetEmployees()
        {
            List<EmployeesModel> EmployeesList = new List<EmployeesModel>();
            DataTable dtEmployees = sqlQuery("SELECT * FROM EMPLEADOS AS A INNER JOIN TIPONACIONALIDADES AS B ON A.EMPLEADO_NACIONALIDAD_ID = B.tiponacionalidad_id ;");

            foreach (DataRow dr in dtEmployees.Rows)
            {
                EmployeesList.Add(
                    new EmployeesModel
                    {
                        EMPLEADO_ID = Convert.ToInt16(dr["EMPLEADO_ID"]),
                        EMPLEADO_NOMBRE = Convert.ToString(dr["EMPLEADO_NOMBRE"]),
                        EMPLEADO_APELLIDO = Convert.ToString(dr["EMPLEADO_APELLIDO"]),
                        EMPLEADO_FECHANACIMIENTO = Convert.ToDateTime(dr["EMPLEADO_FECHANACIMIENTO"]),
                        EMPLEADO_ESTADOCIVIL = Convert.ToInt16(dr["EMPLEADO_ESTADOCIVIL"]),
                        EMPLEADO_CANTIDADHIJOS = Convert.ToInt16(dr["EMPLEADO_CANTIDADHIJOS"]),
                        EMPLEADO_DOMICILIO = Convert.ToString(dr["EMPLEADO_DOMICILIO"]),
                        EMPLEADO_EMAIL = Convert.ToString(dr["EMPLEADO_EMAIL"]),
                        EMPLEADO_LOCALIDAD_CODIGOPOSTAL = Convert.ToString(dr["EMPLEADO_LOCALIDAD_CODIGOPOSTAL"]),
                        EMPLEADO_DOCUMENTO = Convert.ToString(dr["EMPLEADO_DOCUMENTO"]),
                        EMPLEADO_CUIL = Convert.ToString(dr["EMPLEADO_CUIL"]),
                        EMPLEADO_DESCRIPCION = Convert.ToString(dr["TIPONACIONALIDAD_DESCRIPCION"]),
                        EMPLEADO_FECHAINGRESO = Convert.ToDateTime(dr["EMPLEADO_FECHAINGRESO"]),
                        EMPLEADO_ESTADO_ID = Convert.ToString(dr["EMPLEADO_ESTADO_ID"]),
                        EMPLEADO_NACIONALIDAD_ID = Convert.ToInt16(dr["EMPLEADO_NACIONALIDAD_ID"]),
                        EMPLEADO_TIPODOCUMENTO_ID = Convert.ToInt16(dr["EMPLEADO_TIPODOCUMENTO_ID"]),
                        EMPLEADO_NUMEROAFILIADO = Convert.ToString(dr["EMPLEADO_NUMEROAFILIADO"]),
                        EMPLEADO_OBRASOCIAL_ID = Convert.ToInt16(dr["EMPLEADO_OBRASOCIAL_ID"]),
                    });
            }
            return EmployeesList;
        }

        /**************** ADD NEW EMPLOYEES *********************/

        public bool AddEmployees(EmployeesModel EmployeesView)
        {
            Audit.AUDITORIA_MODULO = "EMPLOYEES";
            Audit.AUDITORIA_ACCION = "CREATE";
            Audit.AUDITORIA_NOMBRECONTROLADOR = "EmployeesService.AddEmployees";
            try
            {
                connection();
                SqlCommand cmd = new SqlCommand("INSERT INTO EMPLEADOS (empleado_nombre, empleado_apellido, empleado_fechanacimiento, empleado_estadocivil, empleado_cantidadhijos, empleado_domicilio, empleado_email, empleado_localidad_codigopostal, empleado_documento, empleado_cuil, empleado_nacionalidad_id, empleado_fechaingreso, empleado_estado_id, empleado_tipodocumento_id, empleado_numeroafiliado, empleado_obrasocial_id) VALUES (@empleado_nombre, @empleado_apellido, @empleado_fechanacimiento, @empleado_estadocivil, @empleado_cantidadhijos, @empleado_domicilio, @empleado_email, @empleado_localidad_codigopostal, @empleado_documento, @empleado_cuil, @empleado_nacionalidad_id, @empleado_fechaingreso, @empleado_estado_id, @empleado_tipodocumento_id, @empleado_numeroafiliado, @empleado_obrasocial_id)", con);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.Add("@empleado_nombre", SqlDbType.VarChar).Value = EmployeesView.EMPLEADO_NOMBRE.ToUpper();
                cmd.Parameters.Add("@empleado_apellido", SqlDbType.VarChar).Value = EmployeesView.EMPLEADO_APELLIDO.ToUpper();
                cmd.Parameters.Add("@empleado_fechanacimiento", SqlDbType.DateTime).Value = EmployeesView.EMPLEADO_FECHANACIMIENTO;
                cmd.Parameters.Add("@empleado_estadocivil", SqlDbType.Int).Value = EmployeesView.EMPLEADO_ESTADOCIVIL;
                cmd.Parameters.Add("@empleado_cantidadhijos", SqlDbType.Int).Value = EmployeesView.EMPLEADO_CANTIDADHIJOS;
                cmd.Parameters.Add("@empleado_domicilio", SqlDbType.VarChar).Value = EmployeesView.EMPLEADO_DOMICILIO.ToUpper();
                cmd.Parameters.Add("@empleado_email", SqlDbType.VarChar).Value = EmployeesView.EMPLEADO_EMAIL;
                cmd.Parameters.Add("@empleado_localidad_codigopostal", SqlDbType.VarChar).Value = EmployeesView.EMPLEADO_LOCALIDAD_CODIGOPOSTAL.ToUpper();
                cmd.Parameters.Add("@empleado_documento", SqlDbType.VarChar).Value = EmployeesView.EMPLEADO_DOCUMENTO.ToUpper();
                cmd.Parameters.Add("@empleado_cuil", SqlDbType.VarChar).Value = EmployeesView.EMPLEADO_CUIL.ToUpper();
                cmd.Parameters.Add("@empleado_nacionalidad_id", SqlDbType.Int).Value = EmployeesView.EMPLEADO_NACIONALIDAD_ID;
                cmd.Parameters.Add("@empleado_fechaingreso", SqlDbType.DateTime).Value = EmployeesView.EMPLEADO_FECHAINGRESO;
                cmd.Parameters.Add("@empleado_estado_id", SqlDbType.VarChar).Value = "ACT_EMP";
                cmd.Parameters.Add("@empleado_tipodocumento_id", SqlDbType.Int).Value = EmployeesView.EMPLEADO_TIPODOCUMENTO_ID;
                cmd.Parameters.Add("@empleado_numeroafiliado", SqlDbType.VarChar).Value = EmployeesView.EMPLEADO_NUMEROAFILIADO.ToUpper();
                cmd.Parameters.Add("@empleado_obrasocial_id", SqlDbType.Int).Value = EmployeesView.EMPLEADO_OBRASOCIAL_ID;
                con.Open();
                int i = cmd.ExecuteNonQuery();
                con.Close();

                // query que se ejecuta
                string tmp = cmd.CommandText.ToString();
                foreach (SqlParameter p in cmd.Parameters)
                {
                    tmp = tmp.Replace(p.ParameterName.ToString(), "'" + p.Value.ToString() + "'");
                }

                Audit.AUDITORIA_RASTRO = tmp;

                if (i >= 1)
                {
                    Audit.AUDITORIA_ESTADO = "OK";
                    Audit.AUDITORIA_MENSAJE = "El empleado se inserto OK.";
                    auditservice.AddAudit(Audit);
                    return true;
                }
                else
                {
                    Audit.AUDITORIA_ESTADO = "ERROR";
                    Audit.AUDITORIA_MENSAJE = "El empleado no se pudo insertar.";
                    auditservice.AddAudit(Audit);
                    return false;
                }
            }
            catch (Exception ex)
            {
                Audit.AUDITORIA_RASTRO = ex.Message;
                Audit.AUDITORIA_ESTADO = "ERROR";
                Audit.AUDITORIA_MENSAJE = "El empleado no se pudo insertar.";
                auditservice.AddAudit(Audit);
                return false;
            }
        }

        /**************** EMPLOYEES EDIT *********************/

        public bool UpdateEmployees(EmployeesModel EmployeesView)
        {
            try
            {
                connection();
                SqlCommand cmd = new SqlCommand("UPDATE EMPLEADOS SET empleado_nombre = @empleado_nombre , empleado_apellido = @empleado_apellido , empleado_fechanacimiento = @empleado_fechanacimiento , empleado_estadocivil = @empleado_estadocivil, empleado_cantidadhijos = @empleado_cantidadhijos, empleado_domicilio = @empleado_domicilio , empleado_email = @empleado_email, empleado_localidad_codigopostal = @empleado_localidad_codigopostal, empleado_documento = @empleado_documento, empleado_cuil = @empleado_cuil, empleado_nacionalidad_id = @empleado_nacionalidad_id, empleado_fechaingreso = @empleado_fechaingreso, empleado_tipodocumento_id = @empleado_tipodocumento_id, empleado_numeroafiliado = @empleado_numeroafiliado, empleado_obrasocial_id = @empleado_obrasocial_id WHERE empleado_id = @empleado_id ", con);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.Add("@empleado_id", SqlDbType.Int).Value = EmployeesView.EMPLEADO_ID;
                cmd.Parameters.Add("@empleado_nombre", SqlDbType.VarChar).Value = EmployeesView.EMPLEADO_NOMBRE.ToUpper();
                cmd.Parameters.Add("@empleado_apellido", SqlDbType.VarChar).Value = EmployeesView.EMPLEADO_APELLIDO.ToUpper();
                cmd.Parameters.Add("@empleado_fechanacimiento", SqlDbType.DateTime).Value = EmployeesView.EMPLEADO_FECHANACIMIENTO;
                cmd.Parameters.Add("@empleado_estadocivil", SqlDbType.Int).Value = EmployeesView.EMPLEADO_ESTADOCIVIL;
                cmd.Parameters.Add("@empleado_cantidadhijos", SqlDbType.Int).Value = EmployeesView.EMPLEADO_CANTIDADHIJOS;
                cmd.Parameters.Add("@empleado_domicilio", SqlDbType.VarChar).Value = EmployeesView.EMPLEADO_DOMICILIO.ToUpper();
                cmd.Parameters.Add("@empleado_email", SqlDbType.VarChar).Value = EmployeesView.EMPLEADO_EMAIL;
                cmd.Parameters.Add("@empleado_localidad_codigopostal", SqlDbType.VarChar).Value = EmployeesView.EMPLEADO_LOCALIDAD_CODIGOPOSTAL.ToUpper();
                cmd.Parameters.Add("@empleado_documento", SqlDbType.VarChar).Value = EmployeesView.EMPLEADO_DOCUMENTO.ToUpper();
                cmd.Parameters.Add("@empleado_cuil", SqlDbType.VarChar).Value = EmployeesView.EMPLEADO_CUIL.ToUpper();
                cmd.Parameters.Add("@empleado_nacionalidad_id", SqlDbType.Int).Value = EmployeesView.EMPLEADO_NACIONALIDAD_ID;
                cmd.Parameters.Add("@empleado_fechaingreso", SqlDbType.DateTime).Value = EmployeesView.EMPLEADO_FECHAINGRESO;
                cmd.Parameters.Add("@empleado_tipodocumento_id", SqlDbType.Int).Value = EmployeesView.EMPLEADO_TIPODOCUMENTO_ID;
                cmd.Parameters.Add("@empleado_numeroafiliado", SqlDbType.VarChar).Value = EmployeesView.EMPLEADO_NUMEROAFILIADO.ToUpper();
                cmd.Parameters.Add("@empleado_obrasocial_id", SqlDbType.Int).Value = EmployeesView.EMPLEADO_OBRASOCIAL_ID;
                con.Open();
                int i = cmd.ExecuteNonQuery();
                con.Close();
                string tmp = cmd.CommandText.ToString();
                foreach (SqlParameter p in cmd.Parameters)
                    tmp = tmp.Replace(p.ParameterName.ToString(), "'" + p.Value.ToString() + "'");

                if (i >= 1)
                {
                    auditservice.AddAudit(new AuditModel()
                    {
                        AUDITORIA_MODULO = "EMPLOYEES",
                        AUDITORIA_ACCION = "UPDATE",
                        AUDITORIA_ESTADO = "OK",
                        AUDITORIA_MENSAJE = "El empleado se actualizo correctamente",
                        AUDITORIA_NOMBRECONTROLADOR = "EmployeesService.UpdateEmployees",
                        AUDITORIA_RASTRO = tmp
                    });

                    return true;
                }
                else
                {
                    auditservice.AddAudit(new AuditModel()
                    {
                        AUDITORIA_MODULO = "EMPLOYEES",
                        AUDITORIA_ACCION = "UPDATE",
                        AUDITORIA_ESTADO = "ERROR",
                        AUDITORIA_MENSAJE = "Hubo un error al actualizar el empleado",
                        AUDITORIA_NOMBRECONTROLADOR = "EmployeesService.UpdateEmployees",
                        AUDITORIA_RASTRO = tmp
                    });
                    return false;
                }
            }
            catch (Exception ex)
            {
                auditservice.AddAudit(new AuditModel()
                {
                    AUDITORIA_MODULO = "EMPLOYEES",
                    AUDITORIA_ACCION = "UPDATE",
                    AUDITORIA_ESTADO = "ERROR",
                    AUDITORIA_MENSAJE = "Hubo un error al actualizar el empleado",
                    AUDITORIA_NOMBRECONTROLADOR = "EmployeesService.UpdateEmployees",
                    AUDITORIA_RASTRO = ex.Message
                });
                return false;
            }
        }

        public bool ValidateEmployees(EmployeesModel EmployeesView)
        {
            try
            {
                connection();
                SqlCommand cmd = new SqlCommand("select r.EMPLEADO_ID from EMPLEADOS as r inner join FAMILIARES_EMPLEADOS as i on r.EMPLEADO_ID=i.EMPLEADO_ID where i.EMPLEADO_ID = @EMPLEADO_ID", con);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.Add("@EMPLEADO_ID", SqlDbType.Int).Value = EmployeesView.EMPLEADO_ID;
                SqlDataAdapter sd = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();

                con.Open();
                sd.Fill(dt);
                if (dt.Rows.Count < 1)
                {
                    return true;
                }
                return false;
            }
            catch (Exception e)
            {
                return false;
            }
            finally
            {
                con.Close();
            }
        }

        public bool SetStatusInactive(EmployeesModel empView)
        {
            try
            {
                string sql = "UPDATE EMPLEADOS SET empleado_estado_id = @empleado_estado_id " +
                                             " where empleado_id = @empleado_id";

                connection();
                SqlCommand cmd = new SqlCommand(sql, con);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.Add("@empleado_id", SqlDbType.Int).Value = empView.EMPLEADO_ID;
                cmd.Parameters.Add("@empleado_estado_id", SqlDbType.VarChar).Value = "INAC_EMP";
                con.Open();
                int i = cmd.ExecuteNonQuery();
                con.Close();
                string tmp = cmd.CommandText.ToString();
                foreach (SqlParameter p in cmd.Parameters)
                    tmp = tmp.Replace(p.ParameterName.ToString(), "'" + p.Value.ToString() + "'");

                if (i >= 1)
                {
                    auditservice.AddAudit(new AuditModel()
                    {
                        AUDITORIA_MODULO = "EMPLOYEES",
                        AUDITORIA_ACCION = "DELETE",
                        AUDITORIA_ESTADO = "OK",
                        AUDITORIA_MENSAJE = "El empleado se elimino correctamente",
                        AUDITORIA_NOMBRECONTROLADOR = "EmployeesService.DeleteEmployees",
                        AUDITORIA_RASTRO = tmp
                    });

                    return true;
                }
                else
                {
                    auditservice.AddAudit(new AuditModel()
                    {
                        AUDITORIA_MODULO = "EMPLOYEES",
                        AUDITORIA_ACCION = "DELETE",
                        AUDITORIA_ESTADO = "ERROR",
                        AUDITORIA_MENSAJE = "Hubo un error al elimino el empleado",
                        AUDITORIA_NOMBRECONTROLADOR = "EmployeesService.DeleteEmployees",
                        AUDITORIA_RASTRO = tmp
                    });
                    return false;
                }
            }
            catch (Exception ex)
            {
                auditservice.AddAudit(new AuditModel()
                {
                    AUDITORIA_MODULO = "EMPLOYEES",
                    AUDITORIA_ACCION = "DELETE",
                    AUDITORIA_ESTADO = "ERROR",
                    AUDITORIA_MENSAJE = "Hubo un error al elimino el empleado",
                    AUDITORIA_NOMBRECONTROLADOR = "EmployeesService.DeleteEmployees",
                    AUDITORIA_RASTRO = ex.Message
                });
                return false;
            }
        }

        public bool ExistEmployees(string dni)
        {
            try
            {
                connection();
                SqlCommand cmd = new SqlCommand("SELECT * FROM FAMILIARES WHERE EMPLEADO_DOCUMENTO = @empleado_documento;", con);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.Add("@empleado_documento", SqlDbType.VarChar).Value = dni;
                SqlDataAdapter sd = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();

                con.Open();
                sd.Fill(dt);
                if (dt.Rows.Count < 1)
                    return true;
                return false;
            }
            catch (Exception e)
            {
                return false;
            }
            finally
            {
                con.Close();
            }
        }

        public bool ExistEmployeesDNI(string dni)
        {
            try
            {
                connection();
                SqlCommand cmd = new SqlCommand("SELECT * FROM EMPLEADOS WHERE empleado_documento = @empleado_documento;", con);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.Add("@empleado_documento", SqlDbType.VarChar).Value = dni;
                SqlDataAdapter sd = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();

                con.Open();
                sd.Fill(dt);
                if (dt.Rows.Count < 1)
                    return true;
                return false;
            }
            catch (Exception e)
            {
                return false;
            }
            finally
            {
                con.Close();
            }
        }

        public bool ExistEmployeesCUIL(string cuil)
        {
            try
            {
                connection();
                SqlCommand cmd = new SqlCommand("SELECT * FROM EMPLEADOS WHERE empleado_cuil = @empleado_cuil;", con);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.Add("@empleado_cuil", SqlDbType.VarChar).Value = cuil;
                SqlDataAdapter sd = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();

                con.Open();
                sd.Fill(dt);
                if (dt.Rows.Count < 1)
                    return true;
                return false;
            }
            catch (Exception e)
            {
                return false;
            }
            finally
            {
                con.Close();
            }
        }
    }
}
