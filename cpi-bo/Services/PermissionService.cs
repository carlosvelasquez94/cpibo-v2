﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using cpi_bo.Models;

namespace cpi_bo.Services
{
    public class PermissionService
    {
        private SqlConnection con;
        AuditModel Audit = new AuditModel();
        AuditService auditservice = new AuditService();
        private void connection()
        {
            string constring = ConfigurationManager.ConnectionStrings["db_conn"].ToString();
            con = new SqlConnection(constring);
        }
        private DataTable sqlQuery(string sQuery)
        {
            connection();
            SqlCommand cmd = new SqlCommand(sQuery, con);
            cmd.CommandType = CommandType.Text;
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            sd.Fill(dt);
            con.Close();

            return dt;
        }

        /********** VIEW PERM ASSIGNED X ROL ********************/
        public List<PermissionsModel> GetPermAsigXUser(string sUserNick)
        {
            List<PermissionsModel> permList = new List<PermissionsModel>();
            DataTable dtRoles = sqlQuery("select a.* " +
                                        "from PERMISOS a , PERFILES_PERMISOS b, USUARIOS c " +
                                        "where a.permiso_id = b.permiso_id " +
                                        "and c.usuario_perfil_id = b.perfil_id " +
                                        "and c.usuario_nick = '" + sUserNick.ToUpper() + "'");

            foreach (DataRow dr in dtRoles.Rows)
            {
                permList.Add(
                    new PermissionsModel
                    {
                        PERMISO_ID = Convert.ToString(dr["PERMISO_ID"]),
                        PERMISO_DESCRIPCION = Convert.ToString(dr["PERMISO_DESCRIPCION"]),
                        PERMISO_MODULO = Convert.ToString(dr["PERMISO_MODULO"])
                    }); 
            }
            return permList;
        }
        /********** VIEW PERM AVAILABLE X ROL ********************/
        public List<PermissionsModel> GetPermDispXRol(Int16 iRolId)
        {
            List<PermissionsModel> permList = new List<PermissionsModel>();
            DataTable dtRoles = sqlQuery("select * " +
                                        "from PERMISOS " +
                                        "where PERMISO_ID not in (select PERMISO_ID from PERFILES_PERMISOS where perfil_id = " + iRolId + ")");

            foreach (DataRow dr in dtRoles.Rows)
            {
                permList.Add(
                    new PermissionsModel
                    {
                        PERMISO_ID = Convert.ToString(dr["PERMISO_ID"]),
                        PERMISO_DESCRIPCION = Convert.ToString(dr["PERMISO_DESCRIPCION"]),
                        PERMISO_MODULO = Convert.ToString(dr["PERMISO_MODULO"])
                    });
            }
            return permList;
        }
        /********** VIEW PERM ASSIGNED X ROL ********************/
        public List<PermissionsModel> GetPermAsigXRol(Int16 iRolId)
        {
            List<PermissionsModel> permList = new List<PermissionsModel>();
            DataTable dtRoles = sqlQuery("select * " +
                                        "from PERMISOS " +
                                        "where PERMISO_ID in (select PERMISO_ID from PERFILES_PERMISOS where perfil_id = " + iRolId + ")");

            foreach (DataRow dr in dtRoles.Rows)
            {
                permList.Add(
                    new PermissionsModel
                    {
                        PERMISO_ID = Convert.ToString(dr["PERMISO_ID"]),
                        PERMISO_DESCRIPCION = Convert.ToString(dr["PERMISO_DESCRIPCION"]),
                        PERMISO_MODULO = Convert.ToString(dr["PERMISO_MODULO"])
                    });

            }

            return permList;
        }
        /**************** ADD NEW PERM TO ROL *********************/
        public bool AddPermToRol(Int16 iRolId, string sPerm)
        {
            Audit.AUDITORIA_MODULO = "PERMISSION";
            Audit.AUDITORIA_ACCION = "CREATE";
            Audit.AUDITORIA_NOMBRECONTROLADOR = "PermissionService.AddPermission";
            try { 
            connection();
            SqlCommand cmd = new SqlCommand("INSERT INTO PERFILES_PERMISOS (PERFIL_ID, PERMISO_ID) VALUES ( " + iRolId + ",'" + sPerm + "')", con);
            cmd.CommandType = CommandType.Text;

            con.Open();
            int i = cmd.ExecuteNonQuery();
            con.Close();

                // query que se ejecuta
                string tmp = cmd.CommandText.ToString();
                foreach (SqlParameter p in cmd.Parameters)
                {
                    tmp = tmp.Replace(p.ParameterName.ToString(), "'" + p.Value.ToString() + "'");
                }

                Audit.AUDITORIA_RASTRO = tmp;

                if (i >= 1)
                {
                    Audit.AUDITORIA_ESTADO = "OK";
                    Audit.AUDITORIA_MENSAJE = "El permiso se inserto OK.";
                    auditservice.AddAudit(Audit);
                    return true;
                }
                else
                {
                    Audit.AUDITORIA_ESTADO = "ERROR";
                    Audit.AUDITORIA_MENSAJE = "El permiso no se pudo insertar.";
                    auditservice.AddAudit(Audit);
                    return false;
                }
            }
            catch (Exception ex)
            {
                Audit.AUDITORIA_RASTRO = ex.Message;
                Audit.AUDITORIA_ESTADO = "ERROR";
                Audit.AUDITORIA_MENSAJE = "El permiso no se pudo insertar.";
                auditservice.AddAudit(Audit);
                return false;
            }
        }
        /**************** ADD NEW PERM TO ROL *********************/
        public bool DeletePermToRol(Int16 iRolId, string sPerm)
        {
            try
            {
                connection();
                SqlCommand cmd = new SqlCommand("DELETE PERFILES_PERMISOS where PERFIL_ID = " + iRolId + " and PERMISO_ID = '" + sPerm + "'; ", con);
                cmd.CommandType = CommandType.Text;

                con.Open();
                int i = cmd.ExecuteNonQuery();
                con.Close();
                string tmp = cmd.CommandText.ToString();
                foreach (SqlParameter p in cmd.Parameters)
                    tmp = tmp.Replace(p.ParameterName.ToString(), "'" + p.Value.ToString() + "'");

                if (i >= 1)
                {
                    auditservice.AddAudit(new AuditModel()
                    {
                        AUDITORIA_MODULO = "PERMISSION",
                        AUDITORIA_ACCION = "DELETE",
                        AUDITORIA_ESTADO = "OK",
                        AUDITORIA_MENSAJE = "El permiso se elimino correctamente",
                        AUDITORIA_NOMBRECONTROLADOR = "PermissionService.DeletePermission",
                        AUDITORIA_RASTRO = tmp
                    });

                    return true;
                }
                else
                {
                    auditservice.AddAudit(new AuditModel()
                    {
                        AUDITORIA_MODULO = "PERMISSION",
                        AUDITORIA_ACCION = "DELETE",
                        AUDITORIA_ESTADO = "ERROR",
                        AUDITORIA_MENSAJE = "Hubo un error al elimino el permiso",
                        AUDITORIA_NOMBRECONTROLADOR = "PermissionService.DeletePermission",
                        AUDITORIA_RASTRO = tmp
                    });
                    return false;
                }
            }
            catch (Exception ex)
            {
                auditservice.AddAudit(new AuditModel()
                {
                    AUDITORIA_MODULO = "PERMISSION",
                    AUDITORIA_ACCION = "DELETE",
                    AUDITORIA_ESTADO = "ERROR",
                    AUDITORIA_MENSAJE = "Hubo un error al elimino el permiso",
                    AUDITORIA_NOMBRECONTROLADOR = "PermissionService.DeletePermission",
                    AUDITORIA_RASTRO = ex.Message
                });
                return false;
            }
        }
    }
}