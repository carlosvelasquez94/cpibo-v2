﻿using cpi_bo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace cpi_bo.Helpers
{
    public class SeguridadTagHelper
    {
        public static IHtmlString SegHrefMenuConIcono(string sUrl, string sIcono, string sText, String sPermiso)
        {
            string htmlString = String.Format("<a href='{0}'><i class='glyphicon {1} ' aria-hidden='true'></i> {2}</a> ", sUrl, sIcono, sText);

            if (validPermisosUser(sPermiso))
                return new HtmlString(htmlString);
            else
                return new HtmlString(string.Empty);
        }

        public static IHtmlString SegHrefBtnConIcono(string sUrl, string sTypeBtn, string sIcono, string sTooltip, string sText, String sPermiso)
        {
            string htmlString;
            if (sText != string.Empty)
                htmlString = String.Format("<a href='{0}' data-toggle='tooltip' title='{3}' class='{1}'><i class='glyphicon {2} ' aria-hidden='true'></i> {4}</a> ", sUrl, sTypeBtn, sIcono, sTooltip, sText);
            else
                htmlString = String.Format("<a href='{0}' data-toggle='tooltip' title='{3}' class='{1}'><i class='glyphicon {2} ' aria-hidden='true'></i></a> ", sUrl, sTypeBtn, sIcono, sTooltip);

            if (validPermisosUser(sPermiso))
                return new HtmlString(htmlString);
            else
                return new HtmlString(string.Empty);
        }

        public static IHtmlString SegHrefAjaxConModalIcon(string sUrl, string sClass, string sNameModal, string sClassIcon, string sText, string sTooltip, String sPermiso)
        {

            string htmlString;
            if (sText != string.Empty)
                htmlString = String.Format("<a href='{0}' class='{1}' data-toggle='tooltip' title='{5}' data-ajax='true' data-ajax-mode='replace'" +
                                           " data-ajax-update='#{2}' onclick='$(\"#{2}\").modal(\"show\");'> <i class=' glyphicon {3}'></i> {4} </a> ", sUrl, sClass, sNameModal, sClassIcon, sText, sTooltip);
            else
                htmlString = String.Format("<a href='{0}' class='{1}' data-toggle='tooltip' title='{4}' data-ajax='true' data-ajax-mode='replace'" +
                                               " data-ajax-update='#{2}' onclick='$(\"#{2}\").modal(\"show\");'> <i class=' glyphicon {3}'></i> </a> ", sUrl, sClass, sNameModal, sClassIcon, sTooltip);

            if (validPermisosUser(sPermiso))
                return new HtmlString(htmlString);
            else
                return new HtmlString(string.Empty);
        }

        public static bool validPermisosUser(string perm_id)
        {
            if (HttpContext.Current.Session["PermisosUser"] != null)
                foreach (var i in (List<PermissionsModel>)HttpContext.Current.Session["PermisosUser"])
                {
                    if (i.PERMISO_ID.Equals(perm_id))
                        return true;
                }

            return false;
        }
    }
}