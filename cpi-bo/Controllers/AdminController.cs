﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using cpi_bo.Services;

namespace cpi_bo.Controllers
{
    public class AdminController : AbstractSeguridadController
    {
        private UserService _userService = new UserService();

        // GET: Admin
        public ActionResult Index()
        {
            var user = _userService.GetUsersXId(Session["UserNick"].ToString());
            ViewBag.userName = user.USUARIO_NOMBRE;
            ViewBag.surname = user.USUARIO_APELLIDO;
            return View();
        }

    }
}