﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using cpi_bo.Services;

namespace cpi_bo.Controllers
{
    public class PermController : Controller
    {
        private PermissionService permService = new PermissionService();
        // GET: Perm
        public ActionResult ListPermDisp(Int16 iRoleId, string sRoleName)
        {
            ViewBag.iRoleId = iRoleId;
            ViewBag.sRoleName = sRoleName;
            return PartialView("_ListPermDisp", permService.GetPermDispXRol(iRoleId));
        }
        public ActionResult ListPermAsig(Int16 iRoleId, string sRoleName)
        {
            ViewBag.iRoleId = iRoleId;
            ViewBag.sRoleName = sRoleName;

            return PartialView("_ListPermAsig", permService.GetPermAsigXRol(iRoleId));
        }

        public ActionResult AddPermToRol(Int16 iRoleId, string sRoleName, string sPerm)
        {
            permService.AddPermToRol(iRoleId, sPerm);

            return Redirect("/Profile/AddRolPerm?iRoleId=" + iRoleId + "&sRoleName=" + sRoleName);
        }

        public ActionResult DeletePermToRol(Int16 iRoleId, string sRoleName, string sPerm)
        {
            permService.DeletePermToRol(iRoleId, sPerm);

            return Redirect("/Profile/AddRolPerm?iRoleId=" + iRoleId + "&sRoleName=" + sRoleName);
        }
    }
}