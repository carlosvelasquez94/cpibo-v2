﻿using cpi_bo.Helpers;
using cpi_bo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using cpi_bo.Services;

namespace cpi_bo.Controllers
{
    public class GraphicController : Controller
    {
        KidService kidService = new KidService();
        // GET: Graphic
        public ActionResult BoysAndGirls()
        {
            var list = kidService.GetKids();
            List<int> repartitions = new List<int>();
            var gender = list.Select(x => x.INFANTE_GENERO).Distinct();
            foreach (var item in gender)
            {
                repartitions.Add(list.Count(x => x.INFANTE_GENERO == item));
            }
            var rep = repartitions;
            ViewBag.GENDER = gender;
            ViewBag.REP = repartitions.ToList();
            return View();
        }

        public ActionResult Nationality()
        {
            var list = kidService.GetKidsNationality();
            List<int> repartitions = new List<int>();
            var nationality = list.Select(x => x.NACIONALIDAD).Distinct();
            foreach (var item in nationality)
            {
                repartitions.Add(list.Count(x => x.NACIONALIDAD == item));
            }
            var rep = repartitions;
            ViewBag.NATIONALITY = nationality;
            ViewBag.REP = repartitions.ToList();
            return View();
        }
    }
}