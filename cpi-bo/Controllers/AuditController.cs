﻿using cpi_bo.Helpers;
using cpi_bo.Helpers;
using cpi_bo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Web.UI;
using System.Web.Mvc;
using cpi_bo.Services;

namespace cpi_bo.Controllers
{
    public class AuditController : AbstractSeguridadController
    {
        private TypeTableService typeTableService = new TypeTableService();
        private AuditService auditService = new AuditService();
        public ActionResult Index()
        {
            return View(auditService.GetAudit());
        }

        public ActionResult AddAudit()
        {
            return View();
        }
        [HttpPost]
        public ActionResult AddAudit(AuditModel auditView)
        {
            if (ModelState.IsValid)
            {
                if (auditService.AddAudit(auditView))

                    TempData["Msg"] = alertHtml_ok(auditView.AUDITORIA_MENSAJE.ToUpper(), "La auditoria fue guardada con exito");
                else
                    TempData["Msg"] = alertHtml_error(auditView.AUDITORIA_MENSAJE.ToUpper(), "Error al guardar la auditoria");
            }
            return Redirect("/Audit/Index");
        }

        public void ExportListToCSV()
        {

            StringWriter sw = new StringWriter();

            sw.WriteLine("\"ID\",\"Modulo\",\"Accion\",\"Estado\",\"Mensaje\",\"Rastro\",\"Nombre controlador\",\"Usuario\",\"Fecha\"");

            Response.ClearContent();
            Response.AddHeader("content-disposition", "attachment;filename=CSV_Auditoria.csv");
            Response.ContentType = "text/csv";

            var auditList = auditService.GetAudit();

            foreach (var line in auditList)
            {
                sw.WriteLine(string.Format("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\",\"{6}\",\"{7}\",\"{8}\"",
                                           line.AUDITORIA_ID,
                                           line.AUDITORIA_MODULO,
                                           line.AUDITORIA_ACCION,
                                           line.AUDITORIA_ESTADO,
                                           line.AUDITORIA_MENSAJE,
                                           line.AUDITORIA_RASTRO,
                                           line.AUDITORIA_NOMBRECONTROLADOR,
                                           line.AUDITORIA_USUARIO_NICK,
                                           line.AUDITORIA_FECHAHORA));
            }

            Response.Write(sw.ToString());

            Response.End();

        }
    }
}