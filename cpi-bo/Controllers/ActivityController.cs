﻿using cpi_bo.Helpers;
using cpi_bo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Web.UI;
using cpi_bo.Services;

namespace cpi_bo.Controllers
{
    public class ActivityController : AbstractSeguridadController
    {
        private TypeTableService typeTableService = new TypeTableService();
        private ActivityService activityService = new ActivityService();
        public ActionResult Index()
        {
            return View(activityService.GetActivity());
        }

        public ActionResult AddActivity()
        {
            ViewBag.comboActivityType = typeTableService.GetTypes("ACTIVIDADES", "ACTIVIDAD_ID");
            ViewBag.comboEmployees = typeTableService.GetTypes("EMPLEADOS", "EMPLEADO_ID");
            return View();
        }
        [HttpPost]
        public ActionResult AddActivity(ActivityModel activityView)
        {
            ViewBag.comboActivityType = typeTableService.GetTypes("ACTIVIDADES", "ACTIVIDAD_ID");
            ViewBag.comboEmployees = typeTableService.GetTypes("EMPLEADOS", "EMPLEADO_ID");
            if (ModelState.IsValid)
            {
                if (activityService.AddActivity(activityView))
                {
                    TempData["Msg"] = alertHtml_ok(activityView.EMPLEADOACTIVIDAD_ANOTACION.ToUpper(), "La actividad fue guardada con exito");
                    return Redirect("/Activity/Index");
                }
                else
                    TempData["Msg"] = alertHtml_error(activityView.EMPLEADOACTIVIDAD_ANOTACION.ToUpper(), "Error al guardar la actividad");
            }
            return View(activityView);
        }

        public ActionResult EditActivity(int actId)
        {
            ViewBag.comboActivityType = typeTableService.GetTypes("ACTIVIDADES", "ACTIVIDAD_ID");
            ViewBag.comboEmployees = typeTableService.GetTypes("EMPLEADOS", "EMPLEADO_ID");
            return View(activityService.GetActivity().Find(a => a.EMPLEADOACTIVIDAD_ID == actId));
        }
        [HttpPost]
        public ActionResult EditActivity(ActivityModel ActivityView)
        {
            ViewBag.comboActivityType = typeTableService.GetTypes("ACTIVIDADES", "ACTIVIDAD_ID");
            ViewBag.comboEmployees = typeTableService.GetTypes("EMPLEADOS", "EMPLEADO_ID");
            if (ModelState.IsValid)
            {
                if (activityService.UpdateActivity(ActivityView))
                {
                    TempData["Msg"] = alertHtml_ok(ActivityView.EMPLEADOACTIVIDAD_ANOTACION, "La actividad fue guardada con exito");
                    return Redirect("/Activity/Index");
                }
                else
                    TempData["Msg"] = alertHtml_error(ActivityView.EMPLEADOACTIVIDAD_ANOTACION, "Error al guardar la actividad");
            }
            return View(ActivityView);
        }

        public void ExportListToCSV()
        {

            StringWriter sw = new StringWriter();

            sw.WriteLine("\"ID\",\"Actividad ID\",\"Empleado ID\",\"Fecha\",\"Hora\",\"Descripcion\"");

            Response.ClearContent();
            Response.AddHeader("content-disposition", "attachment;filename=CSV_Actividades.csv");
            Response.ContentType = "text/csv";

            var activityList = activityService.GetActivity();

            foreach (var line in activityList)
            {
                sw.WriteLine(string.Format("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\"",
                                           line.EMPLEADOACTIVIDAD_ID,
                                           line.EMPLEADOACTIVIDAD_ACTIVIDAD_ID,
                                           line.EMPLEADOACTIVIDAD_EMPLEADO_ID,
                                           line.EMPLEADOACTIVIDAD_FECHA,
                                           line.EMPLEADOACTIVIDAD_HORA,
                                           line.EMPLEADOACTIVIDAD_ANOTACION));
            }

            Response.Write(sw.ToString());

            Response.End();

        }

    }
}