﻿using cpi_bo.Helpers;
using cpi_bo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using cpi_bo.Services;

namespace cpi_bo.Controllers
{
    public class BackUpController : AbstractSeguridadController
    {
        private BackUpService back = new BackUpService();
        // GET: BackUp
        public ActionResult CreateBackUp()
        {
            return PartialView();
        }
        [HttpPost]
        public void CreateBackUpPost()
        {
            back.CreateBackUp();   
        }
    }
}