﻿using cpi_bo.Helpers;
using cpi_bo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using cpi_bo.Services;
namespace cpi_bo.Controllers
{
    public class RoomController : AbstractSeguridadController
    {
        // GET: Room
        RoomService roomService = new RoomService();
        public ActionResult Index()
        {
            ViewBag.sala1 = roomService.getRooms("1");
            ViewBag.sala2 = roomService.getRooms("2");
            ViewBag.sala3 = roomService.getRooms("3");
            ViewBag.sala4 = roomService.getRooms("4");
            ViewBag.sala5 = roomService.getRooms("5");

            return View();
        }
    }
}