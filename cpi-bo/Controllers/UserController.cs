﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.IO;
using cpi_bo.Services;
using cpi_bo.Models;

namespace cpi_bo.Controllers
{
    public class UserController : AbstractSeguridadController
    {
        // GET: Usuario
        private TypeTableService typeTableService = new TypeTableService();
        private UserService userService = new UserService();

        public ActionResult Index()
        {
            return View(userService.GetUsers());
        }

        public ActionResult AddUser()
        {
            ViewBag.comboProfiles = typeTableService.GetTypes("PERFILES", "PERFIL_ID");
            return View();
        }
        [HttpPost]
        public ActionResult AddUser(UserModel userView)
        {
            ViewBag.comboProfiles = typeTableService.GetTypes("PERFILES", "PERFIL_ID");
            if (ModelState.IsValid)
            {
                ViewBag.comboProfiles = typeTableService.GetTypes("PERFILES", "PERFIL_ID");
                if (userService.AddUser(userView))
                {
                    TempData["Msg"] = alertHtml_ok(userView.USUARIO_NICK.ToUpper(), " user add Successfully");
                    return Redirect("/User/Index");
                }
                else
                    TempData["Msg"] = alertHtml_error(userView.USUARIO_NICK.ToUpper(), "user add error");
            }
            return View(userView);
        }

        public ActionResult EditUser(string sUserNick)
        {
            ViewBag.comboProfiles = typeTableService.GetTypes("PERFILES", "PERFIL_ID");
            return View(userService.GetUsersXId(sUserNick));
        }
        [HttpPost]
        public ActionResult EditUser(UserModel userView)
        {
            ViewBag.comboProfiles = typeTableService.GetTypes("PERFILES", "PERFIL_ID");
            if (ModelState.IsValid)
            {
                if (userService.UpdateUser(userView))
                {
                    TempData["Msg"] = alertHtml_ok(userView.USUARIO_NICK, " user update Successfully");
                    return Redirect("/User/Index");
                }
                else
                    TempData["Msg"] = alertHtml_error(userView.USUARIO_NICK, "user update error");
            }
            return View(userView);
        }

        public ActionResult DeleteUser(string sUserNick)
        {
            var user = userService.GetUsers().Find(smodel => smodel.USUARIO_NICK == sUserNick);
            user.USUARIO_CLAVE = "";
            return PartialView("DeleteUser", user);
        }
        [HttpPost]
        public ActionResult DeleteUser(UserModel userView)
        {
            if (userService.UpdatePassword(userView))
                TempData["Msg"] = alertHtml_ok(userView.USUARIO_NICK, " Password save Successfully");
            else
                TempData["Msg"] = alertHtml_error(userView.USUARIO_NICK, " Password save error");

            return Redirect("/User/Index");
        }

        public JsonResult VerifyUser(string sUserNick)
        {
            bool result;
            string message = "";
            if(userService.ExistUser(sUserNick))
            {
                message = alertHtml_ok(sUserNick, "El usuario es valido");
                result = true;
            }
            else
            {
                message = alertHtml_error(sUserNick, "El usuario ya existe");
                result = false;
            }
            return Json(new { result= result, message= message }, JsonRequestBehavior.AllowGet);
        }

        public void ExportListToCSV()
        {

            StringWriter sw = new StringWriter();

            sw.WriteLine("\"Nick\",\"Nombre\",\"Apellido\",\"Clave\",\"Perfil\",\"Estado\",\"Email\"");

            Response.ClearContent();
            Response.AddHeader("content-disposition", "attachment;filename=CSV_Usuarios.csv");
            Response.ContentType = "text/csv";

            var userList = userService.GetUsers();

            foreach (var line in userList)
            {
                sw.WriteLine(string.Format("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\",\"{6}\"",
                                           line.USUARIO_NICK,
                                           line.USUARIO_NOMBRE,
                                           line.USUARIO_APELLIDO,
                                           line.USUARIO_CLAVE,
                                           line.USUARIO_PERFIL_ID,
                                           line.USUARIO_ESTADO_ID,
                                           line.USUARIO_EMAIL));
            }

            Response.Write(sw.ToString());

            Response.End();

        }
    }
}