﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using cpi_bo.Models;
using cpi_bo.Services;

namespace cpi_bo.Controllers
{
    public class RecoveryPasswordController : Controller
    {
        // GET: RecoverPassword
        private UserService userService = new UserService();

        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Index(UserModel userView)
        {
            userService.RecoveryPassword(userView);
            return Redirect("/Login/Index");
        }
    }
}