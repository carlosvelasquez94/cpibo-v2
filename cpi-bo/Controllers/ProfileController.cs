﻿using cpi_bo.Helpers;
using cpi_bo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using cpi_bo.Services;

namespace cpi_bo.Controllers
{
    public class ProfileController : AbstractSeguridadController
    {
        ProfileService dbProfile = new ProfileService();
        public ActionResult Index()
        {
            ModelState.Clear();
            if (TempData["Msg"] != null)
                ViewBag.Message = TempData["Msg"].ToString();
            return View(dbProfile.GetProfile());
        }

        //[OptionalAuthorize("ABM_ROLES")]
        public ActionResult AddProfile()
        {
            return PartialView("_AddProfile");
        }

       // [OptionalAuthorize("ABM_ROLES")]
        [HttpPost]
        public ActionResult AddProfile(ProfileModel ProfileView)
        {
            if (dbProfile.AddProfile(ProfileView))
                TempData["Msg"] = alertHtml_ok(ProfileView.perfil_nombre.ToUpper(), "Perfil agregado con éxito");
            else
                TempData["Msg"] = alertHtml_error(ProfileView.perfil_nombre.ToUpper(), "Error al agregar perfil");

            return Redirect("/Profile/Index");
        }

        //[OptionalAuthorize("ABM_ROLES")]
        public ActionResult EditProfile(Int16 iProfId)
        {
            return PartialView("_EditProfile", dbProfile.GetProfile().Find(smodel => smodel.perfil_id == iProfId));
        }

        //[OptionalAuthorize("ABM_ROLES")]
        [HttpPost]
        public ActionResult EditProfile(ProfileModel ProfileView)
        {
            if (dbProfile.UpdateProfile(ProfileView))
                TempData["Msg"] = alertHtml_ok(ProfileView.perfil_nombre, "Perfil guardado con exito");
            else
                TempData["Msg"] = alertHtml_error(ProfileView.perfil_nombre, "Error al guardar perfil");

            return Redirect("/Profile/Index");
        }

        //[OptionalAuthorize("ABM_ROLES")]
        public ActionResult DeleteProfile(Int16 iProfId)
        {
            return PartialView("_DeleteProfile", dbProfile.GetProfile().Find(smodel => smodel.perfil_id == iProfId));
        }

        //[OptionalAuthorize("ABM_ROLES")]
        [HttpPost]
        public ActionResult DeleteProfile(ProfileModel ProfileView)
        {
            if (dbProfile.DeleteProfile(ProfileView))
                TempData["Msg"] = alertHtml_ok(ProfileView.perfil_nombre, "Perfil eliminado con exito");
            else
                TempData["Msg"] = alertHtml_error(ProfileView.perfil_nombre, "Error al eliminar perfil");

            return Redirect("/Profile/Index");
        }
        public ActionResult AddRolPerm(Int16 iRoleId, string sRoleName)
        {
            ViewBag.iRoleId = iRoleId;
            ViewBag.sRoleName = sRoleName;
            return View();
        }
    }
}