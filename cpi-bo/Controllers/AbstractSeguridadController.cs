﻿using cpi_bo.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Web.UI;
using System.Web.Mvc;
using cpi_bo.Services;

namespace cpi_bo.Controllers
{
    [ValidLogin]
    public class AbstractSeguridadController : Controller
    {
        ReportsService reportService = new ReportsService();

        protected string alertHtml_ok(string sPrincipal, string sMsg)
        {
            return string.Format(@"<div class='alert alert-success alert-dismissible fade in' role='alert'>" +
                                 " <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span></button> " +
                                 "<i class='glyphicon glyphicon-ok'></i> <strong>{0}</strong>, {1} </div>", sPrincipal, sMsg);

        }

        protected string alertHtml_error(string sPrincipal, string sMsg)
        {
            return string.Format(@"<div class='alert alert-danger alert-dismissible fade in' role='alert'>" +
                                 " <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span></button> " +
                                 "<i class='glyphicon glyphicon-remove'></i> <strong>{0}</strong>, {1} </div>", sPrincipal, sMsg);

        }


        public void DowloadExcel(string nameTable)
        {
            var grid = new System.Web.UI.WebControls.GridView();

            grid.DataSource = reportService.getTable(nameTable);

            grid.DataBind();

            Response.ClearContent();
            Response.AddHeader("content-disposition", "attachment; filename=Exported_"+nameTable+".xls");
            Response.ContentType = "application/excel";
            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);

            grid.RenderControl(htw);

            Response.Write(sw.ToString());

            Response.End();
        }
    }
}