﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using cpi_bo.Models;
using cpi_bo.Services;
using System.Web.Security;

namespace cpi_bo.Controllers
{
    public class LoginController : Controller
    {
        private AuditService _auditService = new AuditService();
        private UserService _userService = new UserService();
        private PermissionService _permService = new PermissionService();
        // GET: Login
        public ActionResult Index()
        {
            return View();
        }
        [AllowAnonymous]
        [HttpPost]
        public ActionResult Index(LoginModel userView)
        {
            if (!this.ModelState.IsValid)
                return View(userView);

            if (_userService.ValidateUser(userView))
            {
                Session["PermisosUser"] = _permService.GetPermAsigXUser(userView.USUARIO_NICK);
                Session["UserNick"] = userView.USUARIO_NICK;
                FormsAuthentication.SetAuthCookie(userView.USUARIO_NICK.ToUpper(), false);

                _auditService.AddAudit(new AuditModel()
                {
                    AUDITORIA_MODULO = "LOGIN",
                    AUDITORIA_ACCION = "START",
                    AUDITORIA_ESTADO = "OK",
                    AUDITORIA_MENSAJE = "Se conecto.",
                    AUDITORIA_NOMBRECONTROLADOR = "Login.LogIn",
                    AUDITORIA_RASTRO = "",
                    AUDITORIA_USUARIO_NICK = userView.USUARIO_NICK.ToUpper()
                });

                return Redirect("/Admin/Index");
            }
            else
            {
                this.ModelState.AddModelError("ErrorLogin", "Error Login - Usuario o Password incorrecto");
                return View(userView);
            }
        }
        public ActionResult LogOut()
        {
            FormsAuthentication.SignOut();

            _auditService.AddAudit(new AuditModel()
            {
                AUDITORIA_MODULO = "LOGIN",
                AUDITORIA_ACCION = "FINISH",
                AUDITORIA_ESTADO = "OK",
                AUDITORIA_MENSAJE = "Se desconecto.",
                AUDITORIA_NOMBRECONTROLADOR = "Login.LogOut",
                AUDITORIA_RASTRO = "",
            });

            return RedirectToAction("Index", "Login");
        }
    }
}