﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Web.UI;
using cpi_bo.Services;
using cpi_bo.Models;

namespace cpi_bo.Controllers
{
    public class ResponsibleController : AbstractSeguridadController
    {
        private TypeTableService typeTableService = new TypeTableService();
        private ResponsibleService responsibleService = new ResponsibleService();

        // GET: Responsable
        public ActionResult Index()
        {
            return View(responsibleService.GetResponsables());
        }

        public ActionResult AddResponsible()
        {
            ViewBag.comboDocumentType = typeTableService.GetTypes("TIPODOCUMENTOS", "TIPODOCUMENTO_ID");
            ViewBag.comboNationalityType = typeTableService.GetTypes("TIPONACIONALIDADES", "TIPONACIONALIDAD_ID");
            return View();
        }
        [HttpPost]
        public ActionResult AddResponsible(ResponsibleModel respView)
        {
            ViewBag.comboDocumentType = typeTableService.GetTypes("TIPODOCUMENTOS", "TIPODOCUMENTO_ID");
            ViewBag.comboNationalityType = typeTableService.GetTypes("TIPONACIONALIDADES", "TIPONACIONALIDAD_ID");

            if (ModelState.IsValid)
            {
                if (responsibleService.AddResponsable(respView))
                {
                    TempData["Msg"] = alertHtml_ok(respView.RESPONSABLE_APELLIDO.ToUpper(), " responsable add Successfully");
                    return Redirect("/Responsible/Index");
                }
                else
                    TempData["Msg"] = alertHtml_error(respView.RESPONSABLE_APELLIDO.ToUpper(), "responsable add error");             
            }
            return View(respView);
        }

        public ActionResult EditResponsible(Int32 iRespId)
        {
            ViewBag.comboDocumentType = typeTableService.GetTypes("TIPODOCUMENTOS", "TIPODOCUMENTO_ID");
            ViewBag.comboNationalityType = typeTableService.GetTypes("TIPONACIONALIDADES", "TIPONACIONALIDAD_ID");
            return View(responsibleService.GetResponsables().Find(smodel => smodel.RESPONSABLE_ID == iRespId));
        }
        [HttpPost]
        public ActionResult EditResponsible(ResponsibleModel respView)
        {
            ViewBag.comboDocumentType = typeTableService.GetTypes("TIPODOCUMENTOS", "TIPODOCUMENTO_ID");
            ViewBag.comboNationalityType = typeTableService.GetTypes("TIPONACIONALIDADES", "TIPONACIONALIDAD_ID");
            if (ModelState.IsValid)
            {
                if (responsibleService.UpdateResponsable(respView))
                {
                    TempData["Msg"] = alertHtml_ok(respView.RESPONSABLE_APELLIDO, " user update Successfully");
                    return Redirect("/Responsible/Index");
                }
                else
                    TempData["Msg"] = alertHtml_error(respView.RESPONSABLE_APELLIDO, "user update error");
            }
            return View(respView);
        }

        public ActionResult DeleteResponsible(Int32 iRespId)
        {
            var responsible = responsibleService.GetResponsables().Find(smodel => smodel.RESPONSABLE_ID == iRespId);
            return PartialView("DeleteResponsible", responsible);
        }
        [HttpPost]
        public ActionResult DeleteResponsible(ResponsibleModel respView)
        {
            if (responsibleService.ValidateResponsible(respView))
            {
                if (responsibleService.SetStatusInactive(respView))
                    TempData["Msg"] = alertHtml_ok(respView.RESPONSABLE_APELLIDO, " eliminado correctamente");
                else
                    TempData["Msg"] = alertHtml_error(respView.RESPONSABLE_APELLIDO, " error");
            }
            else
            {
                TempData["Msg"] = alertHtml_error(respView.RESPONSABLE_APELLIDO, " tiene asignnado un infante y no se puede eliminar");
            }
            return Redirect("/Responsible/Index");
        }

        public void ExportListToCSV()
        {

            StringWriter sw = new StringWriter();

            sw.WriteLine("\"Id\",\"Nombre\",\"Apellido\",\"Documento\",\"Tipo documento\",\"Tipo nacionalidad\"");

            Response.ClearContent();
            Response.AddHeader("content-disposition", "attachment;filename=CSV_Reponsables.csv");
            Response.ContentType = "text/csv";

            var responsibleList = responsibleService.GetResponsables();

            foreach (var line in responsibleList)
            {
                sw.WriteLine(string.Format("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\",\"",
                                           line.RESPONSABLE_ID,
                                           line.RESPONSABLE_NOMBRE,
                                           line.RESPONSABLE_APELLIDO,
                                           line.RESPONSABLE_DOCUMENTO,
                                           line.RESPONSABLE_TIPODOCUMENTO_ID,
                                           line.RESPONSABLE_TIPONACIONALIDAD_ID));
            }

            Response.Write(sw.ToString());

            Response.End();

        }
    }
}