﻿using cpi_bo.Helpers;
using cpi_bo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Web.UI;
using System.Web.Mvc;
using cpi_bo.Services;

namespace cpi_bo.Controllers
{
    public class FamilyController : AbstractSeguridadController
    {
        private TypeTableService typeTableService = new TypeTableService();
        private FamilyService familyService = new FamilyService();
        // GET: Family
        public ActionResult Index()
        {
            ModelState.Clear();
            if (TempData["Msg"] != null)
                ViewBag.Message = TempData["Msg"].ToString();

            return View(familyService.GetFamily());
        }

        public ActionResult AddFamily()
        {
            ViewBag.comboDocumentType = typeTableService.GetTypes("TIPODOCUMENTOS", "TIPODOCUMENTO_ID");
            ViewBag.comboMaritalStatus = typeTableService.GetTypes("ESTADOSCIVILES", "ESTADOCIVIL_ID");
            ViewBag.comboSocialWork = typeTableService.GetTypes("OBRASOCIAL", "OBRASOCIAL_ID");
            return View();
        }

        [HttpPost]
        public ActionResult AddFamily(FamilyModel FamilyView)
        {
            ViewBag.comboDocumentType = typeTableService.GetTypes("TIPODOCUMENTOS", "TIPODOCUMENTO_ID");
            ViewBag.comboMaritalStatus = typeTableService.GetTypes("ESTADOSCIVILES", "ESTADOCIVIL_ID");
            ViewBag.comboSocialWork = typeTableService.GetTypes("OBRASOCIAL", "OBRASOCIAL_ID");
            if (ModelState.IsValid)
            {
                if (familyService.AddFamily(FamilyView))
                {
                    TempData["Msg"] = alertHtml_ok(FamilyView.FAMILIAR_NOMBRE.ToUpper(), "Familiar agregado con éxito");
                    return Redirect("/Family/Index");
                }
                else
                    TempData["Msg"] = alertHtml_error(FamilyView.FAMILIAR_NOMBRE.ToUpper(), "Error al agregar familiar");
            }
            return View(FamilyView);
        }

        public ActionResult EditFamily(string sFamilyId)
        {
            ViewBag.comboDocumentType = typeTableService.GetTypes("TIPODOCUMENTOS", "TIPODOCUMENTO_ID");
            ViewBag.comboMaritalStatus = typeTableService.GetTypes("ESTADOSCIVILES", "ESTADOCIVIL_ID");
            ViewBag.comboSocialWork = typeTableService.GetTypes("OBRASOCIAL", "OBRASOCIAL_ID");
            var a = familyService.GetFamily().Find(x => x.FAMILIAR_ID == Convert.ToInt16(sFamilyId));
            return View(a);
        }

        [HttpPost]
        public ActionResult EditFamily(FamilyModel FamilyView)
        {
            ViewBag.comboDocumentType = typeTableService.GetTypes("TIPODOCUMENTOS", "TIPODOCUMENTO_ID");
            ViewBag.comboMaritalStatus = typeTableService.GetTypes("ESTADOSCIVILES", "ESTADOCIVIL_ID");
            ViewBag.comboSocialWork = typeTableService.GetTypes("OBRASOCIAL", "OBRASOCIAL_ID");
            if (ModelState.IsValid)
            {
                if (familyService.UpdateFamily(FamilyView))
                {
                    TempData["Msg"] = alertHtml_ok(FamilyView.FAMILIAR_NOMBRE.ToUpper(), " actualización de familiar con éxito");
                    return Redirect("/Family/Index");
                }
                else
                    TempData["Msg"] = alertHtml_error(FamilyView.FAMILIAR_NOMBRE.ToUpper(), " error de actualización del familiar");
            }
            return View(FamilyView);
        }

        public ActionResult DeleteFamily(Int32 sFamilyId)
        {
            var family = familyService.GetFamily().Find(smodel => smodel.FAMILIAR_ID == sFamilyId);
            return PartialView("DeleteFamily", family);
        }
        [HttpPost]
        public ActionResult DeleteFamily(FamilyModel famView)
        {
            if (familyService.DeleteBond(famView))
            {
                if (familyService.DeleteFamily(famView))
                {
                    TempData["Msg"] = alertHtml_ok(famView.FAMILIAR_APELLIDO, " eliminado correctamente");
                }
                else
                {
                    TempData["Msg"] = alertHtml_error(famView.FAMILIAR_APELLIDO, " error");
                }
            }
            else
            {
                TempData["Msg"] = alertHtml_error(famView.FAMILIAR_APELLIDO, " error");
            }
            return Redirect("/Family/Index");
        }

        public JsonResult VerifyDoc(string sDocFamily)
        {
            bool result;
            string message = "";
            if (familyService.ExistFamily(sDocFamily))
            {
                message = alertHtml_ok(sDocFamily, "El documento es valido");
                result = true;
            }
            else
            {
                message = alertHtml_error(sDocFamily, "El documento ya existe");
                result = false;
            }
            return Json(new { result = result, message = message }, JsonRequestBehavior.AllowGet);
        }

        public void ExportListToCSV()
        {

            StringWriter sw = new StringWriter();

            sw.WriteLine("\"Id\",\"Estado Civil\",\"Obra Social\",\"Tipo de documento\",\"Nombre\",\"Apellido\",\"Documento\",\"Género\",\"Fecha de Nacimiento\"");

            Response.ClearContent();
            Response.AddHeader("content-disposition", "attachment;filename=CSV_Familiares.csv");
            Response.ContentType = "text/csv";

            var familyList = familyService.GetFamily();

            foreach (var line in familyList)
            {
                sw.WriteLine(string.Format("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\",\"{6}\",\"{7}\",\"{8}\"",
                                           line.FAMILIAR_ID,
                                           line.FAMILIAR_ESTADOCIVIL_ID,
                                           line.FAMILIAR_OBRASOCIAL_ID,
                                           line.FAMILIAR_TIPODOCUMENTO_ID,
                                           line.FAMILIAR_NOMBRE,
                                           line.FAMILIAR_APELLIDO,
                                           line.FAMILIAR_DOCUMENTO,
                                           line.FAMILIAR_GENERO,
                                           line.FAMILIAR_FECHANACIMIENTO));
            }

            Response.Write(sw.ToString());

            Response.End();

        }
    }
}