﻿using cpi_bo.Helpers;
using cpi_bo.Models;
using System;
using System.IO;
using System.Web.UI;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using cpi_bo.Services;

namespace cpi_bo.Controllers
{
    public class EmployeesController : AbstractSeguridadController
    {
        private TypeTableService typeTableService = new TypeTableService();
        private EmployeesService employeesService = new EmployeesService();
        // GET: Employees
        public ActionResult Index()
        {
            ModelState.Clear();
            if (TempData["Msg"] != null)
                ViewBag.Message = TempData["Msg"].ToString();

            return View(employeesService.GetEmployees());
        }

        public ActionResult AddEmployees()
        {
            ViewBag.comboDocumentType = typeTableService.GetTypes("TIPODOCUMENTOS", "TIPODOCUMENTO_ID");
            ViewBag.comboMaritalStatus = typeTableService.GetTypes("ESTADOSCIVILES", "ESTADOCIVIL_ID");
            ViewBag.comboSocialWork = typeTableService.GetTypes("OBRASOCIAL", "OBRASOCIAL_ID");
            ViewBag.comboNationalityType = typeTableService.GetTypes("TIPONACIONALIDADES", "TIPONACIONALIDAD_ID");
            ViewBag.comboLocation = typeTableService.GetTypes("LOCALIDADES", "LOCALIDAD_CODIGOPOSTAL");
            return View();
        }
        [HttpPost]
        public ActionResult AddEmployees(EmployeesModel EmployeesView)
        {
            ViewBag.comboDocumentType = typeTableService.GetTypes("TIPODOCUMENTOS", "TIPODOCUMENTO_ID");
            ViewBag.comboMaritalStatus = typeTableService.GetTypes("ESTADOSCIVILES", "ESTADOCIVIL_ID");
            ViewBag.comboSocialWork = typeTableService.GetTypes("OBRASOCIAL", "OBRASOCIAL_ID");
            ViewBag.comboNationalityType = typeTableService.GetTypes("TIPONACIONALIDADES", "TIPONACIONALIDAD_ID");
            ViewBag.comboLocation = typeTableService.GetTypes("LOCALIDADES", "LOCALIDAD_CODIGOPOSTAL");
            if (ModelState.IsValid)
            {
                if (employeesService.AddEmployees(EmployeesView))
                {
                    TempData["Msg"] = alertHtml_ok(EmployeesView.EMPLEADO_NOMBRE.ToUpper(), "Empleado agregado con éxito");
                    return Redirect("/Employees/Index");
                }
                else
                    TempData["Msg"] = alertHtml_error(EmployeesView.EMPLEADO_NOMBRE.ToUpper(), "Error al agregar empleado");                
            }
            return View(EmployeesView);
        }

        public ActionResult EditEmployees(string sEmployeesId)
        {
            ViewBag.comboDocumentType = typeTableService.GetTypes("TIPODOCUMENTOS", "TIPODOCUMENTO_ID");
            ViewBag.comboMaritalStatus = typeTableService.GetTypes("ESTADOSCIVILES", "ESTADOCIVIL_ID");
            ViewBag.comboSocialWork = typeTableService.GetTypes("OBRASOCIAL", "OBRASOCIAL_ID");
            ViewBag.comboNationalityType = typeTableService.GetTypes("TIPONACIONALIDADES", "TIPONACIONALIDAD_ID");
            ViewBag.comboLocation = typeTableService.GetTypes("LOCALIDADES", "LOCALIDAD_CODIGOPOSTAL");
            var a = employeesService.GetEmployees().Find(x => x.EMPLEADO_ID == Convert.ToInt16(sEmployeesId));
            return View(a);
        }

        [HttpPost]
        public ActionResult EditEmployees(EmployeesModel EmployeesView)
        {
            ViewBag.comboDocumentType = typeTableService.GetTypes("TIPODOCUMENTOS", "TIPODOCUMENTO_ID");
            ViewBag.comboMaritalStatus = typeTableService.GetTypes("ESTADOSCIVILES", "ESTADOCIVIL_ID");
            ViewBag.comboSocialWork = typeTableService.GetTypes("OBRASOCIAL", "OBRASOCIAL_ID");
            ViewBag.comboNationalityType = typeTableService.GetTypes("TIPONACIONALIDADES", "TIPONACIONALIDAD_ID");
            ViewBag.comboLocation = typeTableService.GetTypes("LOCALIDADES", "LOCALIDAD_CODIGOPOSTAL");
            if (ModelState.IsValid)
            {
                if (employeesService.UpdateEmployees(EmployeesView))
                {
                    TempData["Msg"] = alertHtml_ok(EmployeesView.EMPLEADO_NOMBRE.ToUpper(), " actualización de empleado con éxito");
                    return Redirect("/Employees/Index");
                }
                else
                    TempData["Msg"] = alertHtml_error(EmployeesView.EMPLEADO_NOMBRE.ToUpper(), " error de actualización del empleado");
            }
            return View(EmployeesView);
        }

        public ActionResult DeleteEmployees(Int32 iEmpId)
        {
            var employees = employeesService.GetEmployees().Find(smodel => smodel.EMPLEADO_ID == iEmpId);
            return PartialView("DeleteEmployees", employees);
        }
        [HttpPost]
        public ActionResult DeleteEmployees(EmployeesModel EmployeesView)
        {
            if (employeesService.ValidateEmployees(EmployeesView))
            {
                if (employeesService.SetStatusInactive(EmployeesView))
                    TempData["Msg"] = alertHtml_ok(EmployeesView.EMPLEADO_APELLIDO, " eliminado correctamente");
                else
                    TempData["Msg"] = alertHtml_ok(EmployeesView.EMPLEADO_APELLIDO, " eliminado correctamente");
            }
            else
            {
                TempData["Msg"] = alertHtml_error(EmployeesView.EMPLEADO_APELLIDO, " tiene asignado un familiar y no se puede eliminar");
            }
            return Redirect("/Employees/Index");
        }

        public JsonResult VerifyDoc(string sDocEmployees)
        {
            bool result;
            string message = "";
            if (employeesService.ExistEmployeesDNI(sDocEmployees))
            {
                message = alertHtml_ok(sDocEmployees, "El documento es valido");
                result = true;
            }
            else
            {
                message = alertHtml_error(sDocEmployees, "El documento ya existe");
                result = false;
            }
            return Json(new { result = result, message = message }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult VerifyCuil(string sCuilEmployees)
        {
            bool result;
            string message = "";
            if (employeesService.ExistEmployeesCUIL(sCuilEmployees))
            {
                message = alertHtml_ok(sCuilEmployees, "El cuil es valido");
                result = true;
            }
            else
            {
                message = alertHtml_error(sCuilEmployees, "El cuil ya existe");
                result = false;
            }
            return Json(new { result = result, message = message }, JsonRequestBehavior.AllowGet);
        }

        public void ExportListToCSV()
        {

            StringWriter sw = new StringWriter();

            sw.WriteLine("\"Nombre\",\"Apellido\",\"Fecha de Nacimiento\",\"Estado Civil\",\"Cantidad de hijos\",\"Domicilio\",\"Email\",\"Localidad\",\"Documento\",\"CUIL\",\"Nacionalidad\",\"Fecha de Ingreso\",\"Estado\",\"Tipo de documento\",\"Número de afiliado\",\"Obra Social\"");

            Response.ClearContent();
            Response.AddHeader("content-disposition", "attachment;filename=CSV_Empleados.csv");
            Response.ContentType = "text/csv";

            var employeesList = employeesService.GetEmployees();

            foreach (var line in employeesList)
            {
                sw.WriteLine(string.Format("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\",\"{6}\",\"{7}\",\"{8}\",\"{9}\",\"{10}\",\"{11}\",\"{12}\",\"{13}\",\"{14}\",\"{15}\"",
                                           line.EMPLEADO_NOMBRE,
                                           line.EMPLEADO_APELLIDO,
                                           line.EMPLEADO_FECHANACIMIENTO,
                                           line.EMPLEADO_ESTADOCIVIL,
                                           line.EMPLEADO_CANTIDADHIJOS,
                                           line.EMPLEADO_DOMICILIO,
                                           line.EMPLEADO_EMAIL,
                                           line.EMPLEADO_LOCALIDAD_CODIGOPOSTAL,
                                           line.EMPLEADO_DOCUMENTO,
                                           line.EMPLEADO_CUIL,
                                           line.EMPLEADO_NACIONALIDAD_ID,
                                           line.EMPLEADO_FECHAINGRESO,
                                           line.EMPLEADO_ESTADO_ID,
                                           line.EMPLEADO_TIPODOCUMENTO_ID,
                                           line.EMPLEADO_NUMEROAFILIADO,
                                           line.EMPLEADO_OBRASOCIAL_ID));
            }

            Response.Write(sw.ToString());

            Response.End();

        }
    }
}