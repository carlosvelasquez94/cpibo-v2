﻿using cpi_bo.Helpers;
using cpi_bo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Web.UI;
using cpi_bo.Services;

namespace cpi_bo.Controllers
{
    public class KidController : AbstractSeguridadController
    {
        private TypeTableService typeTableService = new TypeTableService();
        KidService kidService = new KidService();
        SurveyService survey = new SurveyService();
        // GET: Kid
        public ActionResult Index()
        {
            return View(kidService.GetKids());
        }

        public ActionResult AddKid()
        {
            ViewBag.comboDocumentType = typeTableService.GetTypes("TIPODOCUMENTOS", "TIPODOCUMENTO_ID");
            ViewBag.comboNationalityType = typeTableService.GetTypes("TIPONACIONALIDADES", "TIPONACIONALIDAD_ID");
            ViewBag.comboLocation = typeTableService.GetTypes("LOCALIDADES", "LOCALIDAD_CODIGOPOSTAL");
            ViewBag.comboRoomType = typeTableService.GetTypes("SALAS", "SALA_ID");
            return View();
        }
        [HttpPost]
        public ActionResult AddKid(KidModel kidView)
        {
            ViewBag.comboDocumentType = typeTableService.GetTypes("TIPODOCUMENTOS", "TIPODOCUMENTO_ID");
            ViewBag.comboNationalityType = typeTableService.GetTypes("TIPONACIONALIDADES", "TIPONACIONALIDAD_ID");
            ViewBag.comboLocation = typeTableService.GetTypes("LOCALIDADES", "LOCALIDAD_CODIGOPOSTAL");
            ViewBag.comboRoomType = typeTableService.GetTypes("SALAS", "SALA_ID");
            if (ModelState.IsValid)
            {
                if (kidService.AddKid(kidView))
                {
                    TempData["Msg"] = alertHtml_ok(kidView.INFANTE_NOMBRE.ToUpper(), " fue agregado exitosamente");
                    return Redirect("/Kid/Index");
                }
                else
                    TempData["Msg"] = alertHtml_error(kidView.INFANTE_NOMBRE.ToUpper(), "no fue agregado - error");
            }
            return View(kidView);
        }

        public ActionResult EditKid(int kidId)
        {
            ViewBag.comboDocumentType = typeTableService.GetTypes("TIPODOCUMENTOS", "TIPODOCUMENTO_ID");
            ViewBag.comboNationalityType = typeTableService.GetTypes("TIPONACIONALIDADES", "TIPONACIONALIDAD_ID");
            ViewBag.comboLocation = typeTableService.GetTypes("LOCALIDADES", "LOCALIDAD_CODIGOPOSTAL");
            ViewBag.comboRoomType = typeTableService.GetTypes("SALAS", "SALA_ID");
            return View(kidService.GetKids().Find(smodel => smodel.INFANTE_ID == kidId));
        }
        [HttpPost]
        public ActionResult EditKid(KidModel kidView)
        {
            ViewBag.comboDocumentType = typeTableService.GetTypes("TIPODOCUMENTOS", "TIPODOCUMENTO_ID");
            ViewBag.comboNationalityType = typeTableService.GetTypes("TIPONACIONALIDADES", "TIPONACIONALIDAD_ID");
            ViewBag.comboLocation = typeTableService.GetTypes("LOCALIDADES", "LOCALIDAD_CODIGOPOSTAL");
            ViewBag.comboRoomType = typeTableService.GetTypes("SALAS", "SALA_ID");
            if (ModelState.IsValid)
            {
                if (kidService.UpdateKid(kidView))
                {
                    TempData["Msg"] = alertHtml_ok(kidView.INFANTE_NOMBRE, " fue guardado exitosamente");
                    return Redirect("/Kid/Index");
                }
                else
                    TempData["Msg"] = alertHtml_error(kidView.INFANTE_NOMBRE, "no fue guardado - error");
            }
            return View(kidView);
        }

        public ActionResult DeleteKid(Int32 kidId)
        {
            var kid = kidService.GetKids().Find(smodel => smodel.INFANTE_ID == kidId);
            return PartialView("DeleteKid", kid);
        }
        [HttpPost]
        public ActionResult DeleteKid(KidModel kidView)
        {
            if (kidService.SetStatusInactive(kidView))
                TempData["Msg"] = alertHtml_ok(kidView.INFANTE_APELLIDO, " eliminado correctamente");
            else
                TempData["Msg"] = alertHtml_error(kidView.INFANTE_APELLIDO, " error");

            return Redirect("/Kid/Index");
        }

        public ActionResult SurveyKid(int id)
        {
            ViewBag.infanteId = id;
            return View();
        }
        
        public JsonResult IndexSurvey(int infanteId)
        {
            try { 
            var surveyKid = survey.GetFamilyOrganization().Find(smodel => smodel.IDINFANTE == infanteId);

            // se elimina los objetos que hacen recursividad 
            if(surveyKid == null)
            {
                var surveyFamilia = new FamilyOrganization();
                surveyFamilia.ID = 0;
                surveyFamilia.FAMILIARACARGO = 0;
                surveyFamilia.FAMILIARPRIVADOLIBERTAD = 0;
                surveyFamilia.CANTIDADHERMANOS = 0;
                surveyFamilia.MADREADOLECENTE = 0;
                surveyFamilia.FAMILIAREMBARAZADA = 0;
                surveyFamilia.IDINFANTE = infanteId;

                surveyKid = surveyFamilia;
            }

            return Json((surveyKid), JsonRequestBehavior.AllowGet);
            }
            catch(Exception e)
            {
                return null;
            }
        }

        public JsonResult IndexHealth(int infanteId)
        {
            try
            {
                var surveyKid = survey.GetHealth().Find(smodel => smodel.IDINFANTE == infanteId);

                // se elimina los objetos que hacen recursividad 
                if (surveyKid == null)
                {
                    var surveyHealth = new Health();
                    surveyHealth.ID = 0;
                    surveyHealth.CONDICIONESDESALUDFAMILIAR = 0;
                    surveyHealth.COBERTURADESALUDFAMILIAR = 0;
                    surveyHealth.CONTROLDESALUDDELINFANTE = 0;
                    surveyHealth.VACUNACIONDELINFANTE = 0;
                    surveyHealth.IDINFANTE = infanteId;

                    surveyKid = surveyHealth;
                }

                return Json((surveyKid), JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public JsonResult IndexEducation(int infanteId)
        {
            try
            {
                var surveyKid = survey.GetEducation().Find(smodel => smodel.IDINFANTE == infanteId);

                // se elimina los objetos que hacen recursividad 
                if (surveyKid == null)
                {
                    var surveyEducation = new Education();
                    surveyEducation.ID = 0;
                    surveyEducation.ESCOLARIZACIONDELOSINFANTES = 0;
                    surveyEducation.MADREADOLESCENTE = 0;
                    surveyEducation.IDINFANTE = infanteId;

                    surveyKid = surveyEducation;
                }

                return Json((surveyKid), JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public JsonResult IndexWorkSituation(int infanteId)
        {
            try
            {
                var surveyKid = survey.GetWorkSituation().Find(smodel => smodel.IDINFANTE == infanteId);

                // se elimina los objetos que hacen recursividad 
                if (surveyKid == null)
                {
                    var surveyWorkSituation = new WorkSituation();
                    surveyWorkSituation.ID = 0;
                    surveyWorkSituation.TRABAJOENLAFAMILIA = 0;
                    surveyWorkSituation.TRABAJOMENORDEEDAD = 0;
                    surveyWorkSituation.IDINFANTE = infanteId;

                    surveyKid = surveyWorkSituation;
                }

                return Json((surveyKid), JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public JsonResult IndexFamilyEconomy(int infanteId)
        {
            try
            {
                var surveyKid = survey.GetFamilyEconomy().Find(smodel => smodel.IDINFANTE == infanteId);

                // se elimina los objetos que hacen recursividad 
                if (surveyKid == null)
                {
                    var surveyFamilyEconomy = new FamilyEconomy();
                    surveyFamilyEconomy.ID = 0;
                    surveyFamilyEconomy.UBICACIONINGRESOFAMILIAR = 0;
                    surveyFamilyEconomy.BENEFICIOSOCIAL = 0;
                    surveyFamilyEconomy.IDINFANTE = infanteId;

                    surveyKid = surveyFamilyEconomy;
                }

                return Json((surveyKid), JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public JsonResult IndexDwelling(int infanteId)
        {
            try
            {
                var surveyKid = survey.GetDwelling().Find(smodel => smodel.IDINFANTE == infanteId);

                // se elimina los objetos que hacen recursividad 
                if (surveyKid == null)
                {
                    var surveyDwelling = new Dwelling();
                    surveyDwelling.ID = 0;
                    surveyDwelling.TIPOVIVIENDA = 0;
                    surveyDwelling.TIPOPROPIEDAD = 0;
                    surveyDwelling.CANTIDADPERSONASPORHABITACION = 0;
                    surveyDwelling.SERVICIOSVIVIENDA = 0;
                    surveyDwelling.IDINFANTE = infanteId;

                    surveyKid = surveyDwelling;
                }

                return Json((surveyKid), JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public JsonResult IndexSocialProblem(int infanteId)
        {
            try
            {
                var surveyKid = survey.GetSocialProblem().Find(smodel => smodel.IDINFANTE == infanteId);

                // se elimina los objetos que hacen recursividad 
                if (surveyKid == null)
                {
                    var surveySocialProblem = new SocialProblem();
                    surveySocialProblem.ID = 0;
                    surveySocialProblem.REQUIEREINTERVENCION = 0;
                    surveySocialProblem.IDINFANTE = infanteId;

                    surveyKid = surveySocialProblem;
                }

                return Json((surveyKid), JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public JsonResult saveFamilyOrganization(FamilyOrganization familyOrganization)
        {
            var resp = false;
            if (survey.addFamiliOrganization(familyOrganization))
            {
                resp = true;
            }
            
            return Json((resp), JsonRequestBehavior.AllowGet);
        }

        public JsonResult saveHealth(Health Health)
        {
            var resp = false;
            if (survey.addHealth(Health))
            {
                resp = true;
            }

            return Json((resp), JsonRequestBehavior.AllowGet);
        }

        public JsonResult saveEducation(Education Education)
        {
            var resp = false;
            if (survey.addEducation(Education))
            {
                resp = true;
            }

            return Json((resp), JsonRequestBehavior.AllowGet);
        }

        public JsonResult saveWorkSituation(WorkSituation WorkSituation)
        {
            var resp = false;
            if (survey.addWorkSituation(WorkSituation))
            {
                resp = true;
            }

            return Json((resp), JsonRequestBehavior.AllowGet);
        }

        public JsonResult saveFamilyEconomy(FamilyEconomy familyEconomy)
        {
            var resp = false;
            if (survey.addFamilyEconomy(familyEconomy))
            {
                resp = true;
            }

            return Json((resp), JsonRequestBehavior.AllowGet);
        }

        public JsonResult saveDwelling(Dwelling dwelling)
        {
            var resp = false;
            if (survey.addDwelling(dwelling))
            {
                resp = true;
            }

            return Json((resp), JsonRequestBehavior.AllowGet);
        }

        public JsonResult saveSocialProblem(SocialProblem socialProblem)
        {
            var resp = false;
            if (survey.addSocialProblem(socialProblem))
            {
                resp = true;
            }

            return Json((resp), JsonRequestBehavior.AllowGet);
        }

        public void ExportListToCSV()
        {

            StringWriter sw = new StringWriter();

            sw.WriteLine("\"Id\",\"Nombre\",\"Apellido\",\"Documento\",\"Tipo de documento\",\"Genero\",\"Nacionalidad\",\"Fecha de nacimiento\",\"Fecha de ingreso\",\"Domicilio\",\"Codigo postal\",\"Sala\",\"Estado\",\"Fecha egreso\",\"Puntaje total\"");

            Response.ClearContent();
            Response.AddHeader("content-disposition", "attachment;filename=CSV_Infantes.csv");
            Response.ContentType = "text/csv";

            var kidList = kidService.GetKids();

            foreach (var line in kidList)
            {
                sw.WriteLine(string.Format("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\",\"{6}\",\"{7}\",\"{8}\",\"{9}\",\"{10}\",\"{11}\",\"{12}\",\"{13}\",\"{14}\"",
                                           line.INFANTE_ID,
                                           line.INFANTE_NOMBRE,
                                           line.INFANTE_APELLIDO,
                                           line.INFANTE_DOCUMENTO,
                                           line.INFANTE_TIPODOCUMENTO_ID,
                                           line.INFANTE_GENERO,
                                           line.INFANTE_NACIONALIDAD_ID,
                                           line.INFANTE_FECHANACIMIENTO,
                                           line.INFANTE_FECHAINGRESO,
                                           line.INFANTE_DOMICILIO,
                                           line.INFANTE_LOCALIDAD_CODIGOPOSTAL,
                                           line.INFANTE_SALA_ID,
                                           line.INFANTE_ESTADO_ID,
                                           line.INFANTE_FECHAEGRESO,
                                           line.INFANTE_PUNTAJETOTAL));
            }

            Response.Write(sw.ToString());

            Response.End();
        }
    }
}