﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using cpi_bo.Services;
using cpi_bo.Helpers;
using cpi_bo.Models;

namespace cpi_bo.Controllers
{
    public class AssignController : Controller
    {
        KidService kidService = new KidService();
        // GET: Assign
        public ActionResult Index()
        {
            var infact = kidService.GetKids();
            var query = from inf in infact
                        where inf.INFANTE_ESTADO_ID == "WAIT_INF"
                        select inf;
            return View(query.ToList());
        }
        [HttpPost]
        public ActionResult Index(List<KidModel> infante)
        {
            foreach (KidModel item in infante)
            {
                if (item.ASIGNACION)
                {
                    kidService.ChangeState(item.INFANTE_ID);
                }
            }
            return Redirect("/Assign/Index");
        }
    }
}