USE [CPI-BO]
GO
INSERT [dbo].[ESTADOS] ([estado_id], [estado_descripcion], [estado_tipo], [estado_clase]) VALUES (N'ACT_USER', N'Usuario activo', N'USERS', N'label label-success')
SET IDENTITY_INSERT [dbo].[PERFILES] ON 

INSERT [dbo].[PERFILES] ([perfil_id], [perfil_nombre], [perfil_descripcion]) VALUES (1, N'ROOT', N'ROOT')
SET IDENTITY_INSERT [dbo].[PERFILES] OFF
INSERT [dbo].[USUARIOS] ([usuario_nick], [usuario_nombre], [usuario_apellido], [usuario_clave], [usuario_perfil_id], [usuario_estado_id], [usuario_email]) VALUES (N'CRIS', N'CRISTIA', N'DIAS', N'CnGLPo9sFdg=', 1, N'ACT_USER', N'dasdasd')
INSERT [dbo].[USUARIOS] ([usuario_nick], [usuario_nombre], [usuario_apellido], [usuario_clave], [usuario_perfil_id], [usuario_estado_id], [usuario_email]) VALUES (N'ROOT', N'ROOT', N'ROOT', N'9bwar/8hCL5l/3FfbqSimA==', 1, N'ACT_USER', N'ROOT@ROOT.COM')
INSERT [dbo].[PERMISOS] ([permiso_id], [permiso_descripcion], [permiso_modulo]) VALUES (N'MOD_USERS', N'Visualizar editar usuarios', N'USERS')
INSERT [dbo].[PERMISOS] ([permiso_id], [permiso_descripcion], [permiso_modulo]) VALUES (N'PSW_USERS', N'Permitir editar contraseña', N'USERS')
INSERT [dbo].[PERMISOS] ([permiso_id], [permiso_descripcion], [permiso_modulo]) VALUES (N'READ_USERS', N'Visualizar modulo usuarios', N'USERS')
INSERT [dbo].[PERFILES_PERMISOS] ([perfil_id], [permiso_id]) VALUES (1, N'READ_USERS')
INSERT [dbo].[PERFILES_PERMISOS] ([perfil_id], [permiso_id]) VALUES (1, N'MOD_USERS')
INSERT [dbo].[PERFILES_PERMISOS] ([perfil_id], [permiso_id]) VALUES (1, N'PSW_USERS')
