USE [CPI-BO]
GO
/****** Object:  Table [dbo].[ESTADOS]    Script Date: 26/8/2017 02:32:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ESTADOS](
	[estado_id] [varchar](10) NOT NULL,
	[estado_descripcion] [varchar](100) NULL,
	[estado_tipo] [varchar](50) NULL,
	[estado_clase] [varchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[estado_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PERFILES]    Script Date: 26/8/2017 02:32:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PERFILES](
	[perfil_id] [int] IDENTITY(1,1) NOT NULL,
	[perfil_nombre] [varchar](50) NULL,
	[perfil_descripcion] [varchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[perfil_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PERFILES_PERMISOS]    Script Date: 26/8/2017 02:32:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PERFILES_PERMISOS](
	[perfil_id] [int] NOT NULL,
	[permiso_id] [varchar](10) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PERMISOS]    Script Date: 26/8/2017 02:32:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PERMISOS](
	[permiso_id] [varchar](50) NOT NULL,
	[permiso_descripcion] [varchar](100) NULL,
	[permiso_modulo] [varchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[permiso_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[USUARIOS]    Script Date: 26/8/2017 02:32:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[USUARIOS](
	[usuario_nick] [varchar](50) NOT NULL,
	[usuario_nombre] [varchar](50) NULL,
	[usuario_apellido] [varchar](50) NULL,
	[usuario_clave] [varchar](max) NULL,
	[usuario_perfil_id] [int] NULL,
	[usuario_estado_id] [varchar](10) NOT NULL,
	[usuario_email] [varchar](200) NULL,
PRIMARY KEY CLUSTERED 
(
	[usuario_nick] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[ESTADOS] ([estado_id], [estado_descripcion], [estado_tipo], [estado_clase]) VALUES (N'ACT_USER', N'Usuario activo', N'USERS', N'label label-success')
SET IDENTITY_INSERT [dbo].[PERFILES] ON 

INSERT [dbo].[PERFILES] ([perfil_id], [perfil_nombre], [perfil_descripcion]) VALUES (1, N'ROOT', N'ROOT')
SET IDENTITY_INSERT [dbo].[PERFILES] OFF
INSERT [dbo].[PERFILES_PERMISOS] ([perfil_id], [permiso_id]) VALUES (1, N'READ_USERS')
INSERT [dbo].[PERMISOS] ([permiso_id], [permiso_descripcion], [permiso_modulo]) VALUES (N'READ_USERS', N'Visualizar modulo usuarios', N'USERS')
INSERT [dbo].[USUARIOS] ([usuario_nick], [usuario_nombre], [usuario_apellido], [usuario_clave], [usuario_perfil_id], [usuario_estado_id], [usuario_email]) VALUES (N'ROOT', N'ROOT', N'ROOT', N'9bwar/8hCL5l/3FfbqSimA==', 1, N'ACT_USER', N'ROOT@ROOT.COM')
