USE [sti_cc_val_bo]
GO
/****** Object:  Table [dbo].[perm]    Script Date: 20/8/2017 23:59:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[perm](
	[perm_id] [varchar](50) NOT NULL,
	[perm_desc] [varchar](100) NULL,
	[perm_module] [varchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[perm_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[roles]    Script Date: 20/8/2017 23:59:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[roles](
	[role_id] [int] IDENTITY(1,1) NOT NULL,
	[role_name] [varchar](50) NULL,
	[role_description] [varchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[role_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[roles_permissions]    Script Date: 20/8/2017 23:59:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[roles_permissions](
	[role_id] [int] NULL,
	[perm_id] [varchar](50) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[states]    Script Date: 20/8/2017 23:59:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[states](
	[state_id] [varchar](10) NOT NULL,
	[state_desc] [varchar](100) NULL,
	[state_type] [varchar](50) NULL,
	[state_class] [varchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[state_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[users]    Script Date: 20/8/2017 23:59:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[users](
	[user_nick] [varchar](100) NOT NULL,
	[user_name] [varchar](50) NULL,
	[user_lastName] [varchar](50) NULL,
	[user_email] [varchar](50) NULL,
	[user_phone] [varchar](15) NULL,
	[user_phoneMobile] [varchar](15) NULL,
	[user_state_id] [varchar](10) NULL,
	[user_country_id] [int] NULL,
	[user_prov_id] [int] NULL,
	[user_local_id] [int] NULL,
	[user_role_id] [int] NULL,
	[user_password] [varchar](200) NULL,
	[user_last_conn_date] [datetime] NULL,
	[user_code_airline] [char](2) NULL,
PRIMARY KEY CLUSTERED 
(
	[user_nick] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[roles_permissions]  WITH CHECK ADD  CONSTRAINT [FK_roles_permissions_perm] FOREIGN KEY([perm_id])
REFERENCES [dbo].[perm] ([perm_id])
GO
ALTER TABLE [dbo].[roles_permissions] CHECK CONSTRAINT [FK_roles_permissions_perm]
GO
ALTER TABLE [dbo].[roles_permissions]  WITH CHECK ADD  CONSTRAINT [FK_roles_permissions_roles] FOREIGN KEY([role_id])
REFERENCES [dbo].[roles] ([role_id])
GO
ALTER TABLE [dbo].[roles_permissions] CHECK CONSTRAINT [FK_roles_permissions_roles]
GO
ALTER TABLE [dbo].[users]  WITH CHECK ADD  CONSTRAINT [FK_users_roles] FOREIGN KEY([user_role_id])
REFERENCES [dbo].[roles] ([role_id])
GO
ALTER TABLE [dbo].[users] CHECK CONSTRAINT [FK_users_roles]
GO
ALTER TABLE [dbo].[users]  WITH CHECK ADD  CONSTRAINT [FK_users_states] FOREIGN KEY([user_state_id])
REFERENCES [dbo].[states] ([state_id])
GO
ALTER TABLE [dbo].[users] CHECK CONSTRAINT [FK_users_states]
GO
