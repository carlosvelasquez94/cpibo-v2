USE [CPI_BO]
GO
INSERT [dbo].[ESTADOS] ([estado_id], [estado_descripcion], [estado_tipo], [estado_clase]) VALUES (N'ACT_USER', N'Usuario activo', N'USERS', N'label label-success')
SET IDENTITY_INSERT [dbo].[ESTADOSCIVILES] ON 

INSERT [dbo].[ESTADOSCIVILES] ([estadocivil_id], [estadocivil_descripcion]) VALUES (1, N'Soltero')
INSERT [dbo].[ESTADOSCIVILES] ([estadocivil_id], [estadocivil_descripcion]) VALUES (2, N'Casado')
SET IDENTITY_INSERT [dbo].[ESTADOSCIVILES] OFF
INSERT [dbo].[LOCALIDADES] ([localidad_codigopostal], [localidad_descripcion]) VALUES (N'1440', N'La Matanza')
SET IDENTITY_INSERT [dbo].[OBRASOCIAL] ON 

INSERT [dbo].[OBRASOCIAL] ([obrasocial_id], [obrasocial_descripcion]) VALUES (1, N'OSDE')
INSERT [dbo].[OBRASOCIAL] ([obrasocial_id], [obrasocial_descripcion]) VALUES (2, N'PAMY')
SET IDENTITY_INSERT [dbo].[OBRASOCIAL] OFF
SET IDENTITY_INSERT [dbo].[TIPODOCUMENTOS] ON 

INSERT [dbo].[TIPODOCUMENTOS] ([tipodocumento_id], [tipodocumento_descripcion]) VALUES (1, N'DNI')
SET IDENTITY_INSERT [dbo].[TIPODOCUMENTOS] OFF
SET IDENTITY_INSERT [dbo].[TIPONACIONALIDADES] ON 

INSERT [dbo].[TIPONACIONALIDADES] ([tiponacionalidad_id], [tiponacionalidad_descripcion]) VALUES (1, N'Argentina')
SET IDENTITY_INSERT [dbo].[TIPONACIONALIDADES] OFF
SET IDENTITY_INSERT [dbo].[FAMILIARES] ON 

INSERT [dbo].[FAMILIARES] ([familiar_id], [familiar_nombre], [familiar_apellido], [familiar_documento], [familiar_estadocivil_id], [familiar_genero], [familiar_fechanacimiento], [familiar_obrasocial_id], [familiar_tipodocumento_id]) VALUES (5, N'NOE', N'VIGNARDI', N'39549786', 1, N'2', CAST(N'1996-04-22' AS Date), 1, 1)
INSERT [dbo].[FAMILIARES] ([familiar_id], [familiar_nombre], [familiar_apellido], [familiar_documento], [familiar_estadocivil_id], [familiar_genero], [familiar_fechanacimiento], [familiar_obrasocial_id], [familiar_tipodocumento_id]) VALUES (6, N'CELESTE', N'VIGNARDI', N'39549448', 1, N'2', CAST(N'1996-04-22' AS Date), 1, 1)
INSERT [dbo].[FAMILIARES] ([familiar_id], [familiar_nombre], [familiar_apellido], [familiar_documento], [familiar_estadocivil_id], [familiar_genero], [familiar_fechanacimiento], [familiar_obrasocial_id], [familiar_tipodocumento_id]) VALUES (7, N'CARLOS', N'VELASQUEZ', N'55565', 1, N'2', CAST(N'1996-04-22' AS Date), 1, 1)
INSERT [dbo].[FAMILIARES] ([familiar_id], [familiar_nombre], [familiar_apellido], [familiar_documento], [familiar_estadocivil_id], [familiar_genero], [familiar_fechanacimiento], [familiar_obrasocial_id], [familiar_tipodocumento_id]) VALUES (8, N'TEST', N'TEST', N'43543545', 1, N'2', CAST(N'1996-04-22' AS Date), 1, 1)
INSERT [dbo].[FAMILIARES] ([familiar_id], [familiar_nombre], [familiar_apellido], [familiar_documento], [familiar_estadocivil_id], [familiar_genero], [familiar_fechanacimiento], [familiar_obrasocial_id], [familiar_tipodocumento_id]) VALUES (9, N'CARLITOOOS', N'VELASQUEZ', N'40878776', 1, N'2', CAST(N'1996-04-22' AS Date), 1, 1)
SET IDENTITY_INSERT [dbo].[FAMILIARES] OFF
SET IDENTITY_INSERT [dbo].[SALAS] ON 

INSERT [dbo].[SALAS] ([sala_id], [sala_descripcion]) VALUES (1, N'Sala 1')
SET IDENTITY_INSERT [dbo].[SALAS] OFF
SET IDENTITY_INSERT [dbo].[INFANTES] ON 

INSERT [dbo].[INFANTES] ([infante_id], [infante_nombre], [infante_apellido], [infante_documento], [infante_tipodocumento_id], [infante_genero], [infante_nacionalidad_id], [infante_fechanacimiento], [infante_fechaingreso], [infante_domicilio], [infante_localidad_codigopostal], [infante_telefono], [infante_sala_id], [infante_estado_id], [infante_fechaegreso], [infante_puntajetotal]) VALUES (1, N'Carlos', N'Velasquez', N'40342123', 1, N'M', 1, CAST(N'1995-11-11' AS Date), CAST(N'2000-11-11' AS Date), N'Sarasa 1233', N'1440', N'1545237645', 1, N'ACT_USER', CAST(N'2002-11-11' AS Date), 20)
INSERT [dbo].[INFANTES] ([infante_id], [infante_nombre], [infante_apellido], [infante_documento], [infante_tipodocumento_id], [infante_genero], [infante_nacionalidad_id], [infante_fechanacimiento], [infante_fechaingreso], [infante_domicilio], [infante_localidad_codigopostal], [infante_telefono], [infante_sala_id], [infante_estado_id], [infante_fechaegreso], [infante_puntajetotal]) VALUES (11, N'Tomas', N'Oyala', N'40256987', 1, N'M', 1, CAST(N'1997-06-25' AS Date), CAST(N'1999-03-25' AS Date), N'Sarasa 1333', N'1440', N'1547823622', 1, N'ACT_USER', CAST(N'2000-09-29' AS Date), 25)
INSERT [dbo].[INFANTES] ([infante_id], [infante_nombre], [infante_apellido], [infante_documento], [infante_tipodocumento_id], [infante_genero], [infante_nacionalidad_id], [infante_fechanacimiento], [infante_fechaingreso], [infante_domicilio], [infante_localidad_codigopostal], [infante_telefono], [infante_sala_id], [infante_estado_id], [infante_fechaegreso], [infante_puntajetotal]) VALUES (12, N'Cristian', N'Dias', N'40398594', 1, N'M', 1, CAST(N'1997-06-28' AS Date), CAST(N'1999-03-20' AS Date), N'Las Heras 3793', N'1440', N'1547823622', 1, N'ACT_USER', CAST(N'2000-11-29' AS Date), 25)
INSERT [dbo].[INFANTES] ([infante_id], [infante_nombre], [infante_apellido], [infante_documento], [infante_tipodocumento_id], [infante_genero], [infante_nacionalidad_id], [infante_fechanacimiento], [infante_fechaingreso], [infante_domicilio], [infante_localidad_codigopostal], [infante_telefono], [infante_sala_id], [infante_estado_id], [infante_fechaegreso], [infante_puntajetotal]) VALUES (13, N'Noelia', N'Vignardi', N'40985632', 1, N'F', 1, CAST(N'1997-11-16' AS Date), CAST(N'1999-03-25' AS Date), N'Las Heras 3793', N'1440', N'1598552147', 1, N'ACT_USER', CAST(N'2001-11-29' AS Date), 20)
INSERT [dbo].[INFANTES] ([infante_id], [infante_nombre], [infante_apellido], [infante_documento], [infante_tipodocumento_id], [infante_genero], [infante_nacionalidad_id], [infante_fechanacimiento], [infante_fechaingreso], [infante_domicilio], [infante_localidad_codigopostal], [infante_telefono], [infante_sala_id], [infante_estado_id], [infante_fechaegreso], [infante_puntajetotal]) VALUES (14, N'Micaela', N'Perez', N'40236985', 1, N'F', 1, CAST(N'1997-05-18' AS Date), CAST(N'1999-03-22' AS Date), N'Hornos 625', N'1440', N'1596325874', 1, N'ACT_USER', CAST(N'2002-11-30' AS Date), 30)
SET IDENTITY_INSERT [dbo].[INFANTES] OFF
SET IDENTITY_INSERT [dbo].[PERFILES] ON 

INSERT [dbo].[PERFILES] ([perfil_id], [perfil_nombre], [perfil_descripcion]) VALUES (1, N'ROOT', N'ROOT')
SET IDENTITY_INSERT [dbo].[PERFILES] OFF
INSERT [dbo].[USUARIOS] ([usuario_nick], [usuario_nombre], [usuario_apellido], [usuario_clave], [usuario_perfil_id], [usuario_estado_id], [usuario_email]) VALUES (N'CRIS', N'CRISTIA', N'DIAS', N'CnGLPo9sFdg=', 1, N'ACT_USER', N'dasdasd')
INSERT [dbo].[USUARIOS] ([usuario_nick], [usuario_nombre], [usuario_apellido], [usuario_clave], [usuario_perfil_id], [usuario_estado_id], [usuario_email]) VALUES (N'NOELIA', N'NOELIA', N'VIGNARDI', N'CnGLPo9sFdg=', 1, N'ACT_USER', N'noelia.vignardi@hotmail.com')
INSERT [dbo].[USUARIOS] ([usuario_nick], [usuario_nombre], [usuario_apellido], [usuario_clave], [usuario_perfil_id], [usuario_estado_id], [usuario_email]) VALUES (N'ROOT', N'ROOT', N'ROOT', N'9bwar/8hCL5l/3FfbqSimA==', 1, N'ACT_USER', N'ROOT@ROOT.COM')
SET IDENTITY_INSERT [dbo].[RESPONSABLES] ON 

INSERT [dbo].[RESPONSABLES] ([responsable_id], [responsable_nombre], [responsable_apellido], [responsable_documento], [responsable_tiponacionalidad_id], [responsable_tipodocumento_id]) VALUES (1, N'Jose', N'Sousa', N'25896321', 1, 1)
INSERT [dbo].[RESPONSABLES] ([responsable_id], [responsable_nombre], [responsable_apellido], [responsable_documento], [responsable_tiponacionalidad_id], [responsable_tipodocumento_id]) VALUES (2, N'Andrea', N'Alberti', N'28596321', 1, 1)
INSERT [dbo].[RESPONSABLES] ([responsable_id], [responsable_nombre], [responsable_apellido], [responsable_documento], [responsable_tiponacionalidad_id], [responsable_tipodocumento_id]) VALUES (3, N'Nahuel', N'Olaya', N'24896741', 1, 1)
SET IDENTITY_INSERT [dbo].[RESPONSABLES] OFF
INSERT [dbo].[PERMISOS] ([permiso_id], [permiso_descripcion], [permiso_modulo]) VALUES (N'ADD_FAM', N'Alta de familiares', N'FAMILIARES')
INSERT [dbo].[PERMISOS] ([permiso_id], [permiso_descripcion], [permiso_modulo]) VALUES (N'DEL_INT', N'Visualizar borrar infantes', N'INFANTES')
INSERT [dbo].[PERMISOS] ([permiso_id], [permiso_descripcion], [permiso_modulo]) VALUES (N'DEL_RESP', N'Visualizar borrar responsables', N'RESPONSABLES')
INSERT [dbo].[PERMISOS] ([permiso_id], [permiso_descripcion], [permiso_modulo]) VALUES (N'EDIT_FAM', N'Edicion de familiares', N'FAMILIARES')
INSERT [dbo].[PERMISOS] ([permiso_id], [permiso_descripcion], [permiso_modulo]) VALUES (N'MOD_INF', N'Visualizar editar infantes', N'INFANTES')
INSERT [dbo].[PERMISOS] ([permiso_id], [permiso_descripcion], [permiso_modulo]) VALUES (N'MOD_KID', N'Visualizar editar infantes', N'INFANTES')
INSERT [dbo].[PERMISOS] ([permiso_id], [permiso_descripcion], [permiso_modulo]) VALUES (N'MOD_RESP', N'Visualizar editar responsables', N'RESPONSABLES')
INSERT [dbo].[PERMISOS] ([permiso_id], [permiso_descripcion], [permiso_modulo]) VALUES (N'MOD_USERS', N'Visualizar editar usuarios', N'USERS')
INSERT [dbo].[PERMISOS] ([permiso_id], [permiso_descripcion], [permiso_modulo]) VALUES (N'PSW_USERS', N'Permitir editar contraseña', N'USERS')
INSERT [dbo].[PERMISOS] ([permiso_id], [permiso_descripcion], [permiso_modulo]) VALUES (N'READ_FAM', N'Visualizar modulo familiares', N'FAMILIARES')
INSERT [dbo].[PERMISOS] ([permiso_id], [permiso_descripcion], [permiso_modulo]) VALUES (N'READ_INF', N'Visualizar modulo infantes', N'INFANTES')
INSERT [dbo].[PERMISOS] ([permiso_id], [permiso_descripcion], [permiso_modulo]) VALUES (N'READ_KID', N'Visualizar modulo infantes', N'INFANTES')
INSERT [dbo].[PERMISOS] ([permiso_id], [permiso_descripcion], [permiso_modulo]) VALUES (N'READ_PROF', N'Visualizar modulo perfiles', N'PERFILES')
INSERT [dbo].[PERMISOS] ([permiso_id], [permiso_descripcion], [permiso_modulo]) VALUES (N'READ_RESP', N'Visualizar modulo responsables', N'RESPONSABLES')
INSERT [dbo].[PERMISOS] ([permiso_id], [permiso_descripcion], [permiso_modulo]) VALUES (N'READ_USERS', N'Visualizar modulo usuarios', N'USERS')
INSERT [dbo].[PERFILES_PERMISOS] ([perfil_id], [permiso_id]) VALUES (1, N'READ_USERS')
INSERT [dbo].[PERFILES_PERMISOS] ([perfil_id], [permiso_id]) VALUES (1, N'MOD_USERS')
INSERT [dbo].[PERFILES_PERMISOS] ([perfil_id], [permiso_id]) VALUES (1, N'PSW_USERS')
INSERT [dbo].[PERFILES_PERMISOS] ([perfil_id], [permiso_id]) VALUES (1, N'READ_PROF')
INSERT [dbo].[PERFILES_PERMISOS] ([perfil_id], [permiso_id]) VALUES (1, N'DEL_RESP')
INSERT [dbo].[PERFILES_PERMISOS] ([perfil_id], [permiso_id]) VALUES (1, N'DEL_INT')
INSERT [dbo].[PERFILES_PERMISOS] ([perfil_id], [permiso_id]) VALUES (1, N'READ_FAM')
INSERT [dbo].[PERFILES_PERMISOS] ([perfil_id], [permiso_id]) VALUES (1, N'ADD_FAM')
INSERT [dbo].[PERFILES_PERMISOS] ([perfil_id], [permiso_id]) VALUES (1, N'READ_RESP')
INSERT [dbo].[PERFILES_PERMISOS] ([perfil_id], [permiso_id]) VALUES (1, N'MOD_RESP')
INSERT [dbo].[PERFILES_PERMISOS] ([perfil_id], [permiso_id]) VALUES (1, N'MOD_KID')
INSERT [dbo].[PERFILES_PERMISOS] ([perfil_id], [permiso_id]) VALUES (1, N'READ_KID')
INSERT [dbo].[PERFILES_PERMISOS] ([perfil_id], [permiso_id]) VALUES (1, N'MOD_INF')
INSERT [dbo].[PERFILES_PERMISOS] ([perfil_id], [permiso_id]) VALUES (1, N'EDIT_FAM')
