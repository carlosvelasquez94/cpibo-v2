CREATE TABLE [dbo].[log_operations](
	[log_id] [int] IDENTITY(1,1) NOT NULL PRIMARY KEY,
	[log_module] [varchar](100) NOT NULL,
	[log_action] [varchar](50) NULL,
	[log_state] [varchar](10) NULL,
	[log_msg] [varchar](1000) NULL,
	[log_StackTrace] [varchar](8000) NULL,
	[log_ControllerName] [varchar](100) NULL,
	[log_user] [varchar](100) NULL,
	[log_date] [datetime] NULL
)