Use [CPI-BO]

/****** Object:  Table [dbo].[Actividades]   ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Actividades](
	[actividad_id] [int] IDENTITY(0,1) NOT NULL,
	[actividad_descripcion] [varchar](200) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[actividad_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Empleados-Actividades]  ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Empleados-Actividades](
	[empleadoactividad_id] [int] NOT NULL,
	[empleadoactividad_empleado_id] [int] NOT NULL,
	[empleadoactividad_actividad_id] [int] NOT NULL,
	[empleadoactividad_fecha] [date] NOT NULL,
	[empleadoactividad_hora] [time] NOT NULL,
	[empleadoactividad_anotacion] [varchar](200) NULL,
 CONSTRAINT [PK_Empleados-Actividades] PRIMARY KEY CLUSTERED 
(
	[empleadoactividad_empleado_id] ASC,
	[empleadoactividad_actividad_id] ASC,
	[empleadoactividad_fecha] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO