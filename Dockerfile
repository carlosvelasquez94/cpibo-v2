FROM microsoft/dotnet:sdk AS build-env
WORKDIR /app

# Copy csproj and restore as distinct layers
COPY *.sln .
COPY cpi-bo/*.csproj ./cpi-bo
RUN dotnet restore

# Copy everything else and build
COPY cpi-bo/. ./cpi-bo
WORKDIR /app/cpi-bo
RUN dotnet publish -c Release -o out

# Build runtime image
FROM microsoft/dotnet:aspnetcore-runtime
WORKDIR /app
COPY --from=build-env /app/cpi-bo/out ./
ENTRYPOINT ["dotnet", "cpi-bo.dll"]